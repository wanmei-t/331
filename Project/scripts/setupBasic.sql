CREATE OR REPLACE PACKAGE CC_Package AS

    PROCEDURE Add_Country (i_Name CC_Countries.Name%TYPE);
    FUNCTION Get_CountryId (i_name CC_Countries.Name%TYPE)
        RETURN CC_Countries.CountryId%TYPE;
    FUNCTION Does_CountryId_Exists (i_CountryId CC_Countries.CountryId%TYPE)
        RETURN NUMBER;
    Country_Exists_Already EXCEPTION;
    CountryId_Not_Exists EXCEPTION;

    PROCEDURE Add_Province (i_Name CC_Provinces.Name%TYPE, i_CountryId CC_Countries.CountryId%TYPE);
    FUNCTION Get_ProvinceId (i_Name CC_Provinces.Name%TYPE, i_CountryId CC_Countries.CountryId%TYPE)
        RETURN CC_Provinces.ProvinceId%TYPE;
    FUNCTION Does_ProvinceId_Exists (i_ProvinceId CC_Provinces.ProvinceId%TYPE)
        RETURN NUMBER;
    Province_Exists_Already EXCEPTION;
    ProvinceId_Not_Exists EXCEPTION;

    PROCEDURE Add_City (i_Name CC_Cities.Name%TYPE, i_ProvinceId CC_Cities.ProvinceId%TYPE);
    FUNCTION Get_CityId (i_Name CC_Cities.Name%TYPE, i_ProvinceId CC_Cities.ProvinceId%TYPE)
        RETURN CC_Cities.CityId%TYPE;
    FUNCTION Does_CityId_Exists (i_CityId CC_Cities.CityId%TYPE)
        RETURN NUMBER;
    City_Exists_Already EXCEPTION; 
    CityId_Not_Exists EXCEPTION;

    PROCEDURE Add_Customer (i_FirstName CC_Customers.FirstName%TYPE, i_LastName CC_Customers.LastName%TYPE, i_Email CC_Customers.Email%TYPE, i_Address CC_Customers.Address%TYPE, i_CityId CC_Customers.CityId%TYPE);
    FUNCTION Get_CustomerId (i_Email CC_Customers.Email%TYPE)
        RETURN CC_Customers.CustomerId%TYPE;
    FUNCTION Does_CustomerId_Exists (i_CustomerId CC_Customers.CustomerId%TYPE)
        RETURN NUMBER;
    Customer_Exists_Already EXCEPTION;
    CustomerId_Not_Exists EXCEPTION;

    PROCEDURE Add_Store(i_Name CC_Stores.Name%TYPE);
    FUNCTION Get_StoreId (i_Name CC_Stores.Name%TYPE)
        RETURN CC_Stores.StoreId%TYPE;
    FUNCTION Does_StoreId_Exists (i_StoreId CC_Stores.StoreId%TYPE)
        RETURN NUMBER;
    Store_Exists_Already EXCEPTION;
    StoreId_Not_Exists EXCEPTION;

    PROCEDURE Add_Product(i_Name CC_Products.Name%TYPE, i_Category CC_Products.Category%TYPE, i_UnitPrice CC_Products.UnitPrice%TYPE);
    FUNCTION Get_ProductId (i_Name CC_Products.Name%TYPE)
        RETURN CC_Products.ProductId%TYPE;
    FUNCTION Does_ProductId_Exists (i_ProductId CC_Products.ProductId%TYPE)
        RETURN NUMBER;
    Product_Exists_Already EXCEPTION;
    ProductId_Not_Exists EXCEPTION;
    Negative_UnitPrice EXCEPTION;

    PROCEDURE Add_Warehouse (i_Name CC_Warehouses.Name%TYPE, i_Address CC_Warehouses.Address%TYPE, i_CityId CC_Warehouses.CityId%TYPE);
    FUNCTION Get_WarehouseId (i_Name CC_Warehouses.Name%TYPE)
        RETURN CC_Warehouses.WarehouseId%TYPE;
    FUNCTION Does_WarehouseId_Exists (i_WarehouseId CC_Warehouses.WarehouseId%TYPE)
        RETURN NUMBER;
    Warehouse_Exists_Already EXCEPTION;
    WarehouseId_Not_Exists EXCEPTION;

    PROCEDURE Add_Order (i_CustomerId CC_Customers.CustomerId%TYPE, i_StoreId CC_Stores.StoreId%TYPE, i_ProductId CC_Products.ProductId%TYPE, i_Quantity CC_Orders.Quantity%TYPE, i_Price CC_Orders.Price%TYPE, i_OrderDate CC_Orders.OrderDate%TYPE);
    FUNCTION Get_OrderId (i_CustomerId CC_Customers.CustomerId%TYPE, i_StoreId CC_Stores.StoreId%TYPE, i_ProductId CC_Products.ProductId%TYPE, i_OrderDate CC_Orders.OrderDate%TYPE)
        RETURN CC_Orders.OrderId%TYPE;
    FUNCTION Does_OrderId_Exists (i_OrderId CC_Orders.OrderId%TYPE)
        RETURN NUMBER;
    Order_Exists_Already EXCEPTION;
    OrderId_Not_Exists EXCEPTION;
    Negative_OrderPrice EXCEPTION;

    PROCEDURE Add_Review (i_ProductId CC_Products.ProductId%TYPE, i_CustomerId CC_Customers.CustomerId%TYPE, i_Score CC_Reviews.Score%TYPE, i_Description CC_Reviews.Description%TYPE, i_NumFlagged CC_Reviews.NumFlagged%TYPE);
    FUNCTION Get_ReviewId (i_ProductId CC_Products.ProductId%TYPE, i_CustomerId CC_Customers.CustomerId%TYPE)
        RETURN CC_Reviews.ReviewId%TYPE;
    FUNCTION Does_ReviewId_Exists (i_ReviewId CC_Reviews.ReviewId%TYPE)
        RETURN NUMBER;
    Review_Exists_Already EXCEPTION;
    ReviewId_Not_Exists EXCEPTION;
    Invalid_ReviewScore EXCEPTION;
    Negative_NumFlagged EXCEPTION;
    
    PROCEDURE Add_Warehouse_Product (i_WarehouseId CC_Warehouses_Products.WarehouseId%TYPE, i_ProductId CC_Warehouses_Products.ProductId%TYPE, i_Quantity CC_Warehouses_Products.Quantity%TYPE);
    FUNCTION Get_ProductQuantity_From_Warehouse (i_WarehouseId CC_Warehouses_Products.WarehouseId%TYPE, i_ProductId CC_Warehouses_Products.ProductId%TYPE)
        RETURN CC_Warehouses_Products.Quantity%TYPE;
    FUNCTION Get_Overall_ProductQuantity (i_ProductId CC_Warehouses_Products.ProductId%TYPE)
        RETURN CC_Warehouses_Products.Quantity%TYPE;
    ProductInWarehouse_Exists_Already EXCEPTION;
    Negative_Quantity EXCEPTION;
    Quantity_Not_Enough EXCEPTION;

END CC_Package;
/

CREATE OR REPLACE PACKAGE BODY CC_Package AS

    PROCEDURE Add_Country (i_Name CC_Countries.Name%TYPE)
    AS
        v_CountryId CC_Countries.CountryId%TYPE;
    BEGIN
        -- Check if the country doesn't exist already
        v_CountryId := Get_CountryId(UPPER(i_Name));
        IF (v_CountryId != -1) THEN
            DBMS_OUTPUT.PUT_LINE('The country ' || i_Name || ' is already associated with country ' || v_CountryId || '.');
            RAISE Country_Exists_Already;
        END IF;

        -- Creates a new country
        INSERT INTO CC_Countries (Name)
        VALUES (UPPER(i_Name));

        DBMS_OUTPUT.PUT_LINE('Successfully added country: ' || i_Name || '.');
    EXCEPTION
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Error adding country: ' || i_Name || '.');
    END;

    -- Returns the CountryId associated with the country's name or -1 if there is none
    FUNCTION Get_CountryId (i_Name CC_Countries.Name%TYPE)
    RETURN CC_Countries.CountryId%TYPE
    AS
        v_CountryId CC_Countries.CountryId%TYPE;
    BEGIN
        SELECT
            CountryId INTO v_CountryId
        FROM
            CC_Countries
        WHERE   
            Name = UPPER(i_Name);
        
        RETURN v_CountryId;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN -1;
    END;
    
    -- Returns 1 if CountryId exists or -1 if it doesn't exist
    FUNCTION Does_CountryId_Exists (i_CountryId CC_Countries.CountryId%TYPE)
    RETURN NUMBER
    AS
        num_CountryId NUMBER;
    BEGIN
        SELECT
            COUNT(CountryId) INTO num_CountryId
        FROM
            CC_Countries
        WHERE
            CountryId = i_CountryId;

        IF (num_CountryId != 1) THEN
            DBMS_OUTPUT.PUT_LINE('Country ID ' || i_CountryId || ' does not exist.');
            RETURN -1;
        END IF;

        RETURN 1;
    END;


    PROCEDURE Add_Province (i_Name CC_Provinces.Name%TYPE, i_CountryId CC_Countries.CountryId%TYPE)
    AS
        v_ProvinceId CC_Provinces.ProvinceId%TYPE;
        exists_CountryId NUMBER;
    BEGIN
        -- Validates CountryId
        exists_CountryId := Does_CountryId_Exists(i_CountryId);
        IF (exists_CountryId = -1) THEN
            RAISE CountryId_Not_Exists;
        END IF;
    
        -- Checks if the province doesn't exist already
        v_ProvinceId := Get_ProvinceId(UPPER(i_Name), i_CountryId);
        IF (v_ProvinceId != -1) THEN
            DBMS_OUTPUT.PUT_LINE('The province ' || i_Name || ' is already associated with province ' || v_ProvinceId || '.');
            RAISE Province_Exists_Already;
        END IF;

        -- Creates a new province
        INSERT INTO CC_Provinces (Name, CountryId)
        VALUES (UPPER(i_Name), i_CountryId);

        DBMS_OUTPUT.PUT_LINE('Successfully added province: ' || i_Name || '.');
    EXCEPTION
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Error adding province: ' || i_Name || '.');
    END;

    -- Returns the ProvinceId of the province or -1 if the province doesn't exist
    FUNCTION Get_ProvinceId (i_Name CC_Provinces.Name%TYPE, i_CountryId CC_Countries.CountryId%TYPE)
    RETURN CC_Provinces.ProvinceId%TYPE
    AS
        v_ProvinceId CC_Provinces.ProvinceId%TYPE;
    BEGIN
        SELECT
            ProvinceId INTO v_ProvinceId
        FROM
            CC_Provinces
        WHERE   
            Name = UPPER(i_Name)
            AND  CountryId = i_CountryId;
        
        RETURN v_ProvinceId;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN -1;
    END;

    -- Returns 1 if the ProductId exists already or -1 if it doesn't exist
    FUNCTION Does_ProvinceId_Exists (i_ProvinceId CC_Provinces.ProvinceId%TYPE)
    RETURN NUMBER
    AS
        num_ProvinceId NUMBER;
    BEGIN
        SELECT
            COUNT(ProvinceId) INTO num_ProvinceId
        FROM
            CC_Provinces
        WHERE
            ProvinceId = i_ProvinceId;

        IF (num_ProvinceId != 1) THEN
            DBMS_OUTPUT.PUT_LINE('Province ID ' || i_ProvinceId || ' does not exist.');
            RETURN -1;
        END IF;

        RETURN 1;
    END;


    PROCEDURE Add_City (i_Name CC_Cities.Name%TYPE, i_ProvinceId CC_Cities.ProvinceId%TYPE)
    AS
        v_CityId CC_Provinces.ProvinceId%TYPE;
        exists_ProvinceId NUMBER;
    BEGIN
        -- Validates ProvinceId exists
        exists_ProvinceId := Does_ProvinceId_Exists(i_ProvinceId);
        IF (exists_ProvinceId = -1) THEN
            RAISE ProvinceId_Not_Exists;
        END IF;

        -- Checks that the city doesn't exist already
        v_CityId := Get_CityId(UPPER(i_Name), i_ProvinceId);
        IF (v_CityId != -1) THEN
            DBMS_OUTPUT.PUT_LINE('The city ' || i_Name || ' is already associated with city ' || v_CityId || '.');
            RAISE City_Exists_Already;
        END IF;

        -- Creates a new city
        INSERT INTO CC_Cities (Name, ProvinceId)
        VALUES (UPPER(i_Name), i_ProvinceId);

        DBMS_OUTPUT.PUT_LINE('Successfully added city: ' || i_Name || '.');
    EXCEPTION
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Error adding city: ' || i_Name || '.');
    END;

    -- Returns the CityId of the city or -1 if the city doesn't exist
    FUNCTION Get_CityId (i_Name CC_Cities.Name%TYPE, i_ProvinceId CC_Cities.ProvinceId%TYPE)
    RETURN CC_Cities.CityId%TYPE
    AS
        v_CityId CC_Cities.CityId%TYPE;
    BEGIN
        SELECT
            CityId INTO v_CityId
        FROM
            CC_Cities
        WHERE   
            Name = UPPER(i_Name)
            AND  ProvinceId = i_ProvinceId;
        
        RETURN v_CityId;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN -1;
    END;

    -- Returns 1 if the CityId exists or -1 if it doesn't
    FUNCTION Does_CityId_Exists (i_CityId CC_Cities.CityId%TYPE)
    RETURN NUMBER
    AS
        num_CityId NUMBER;
    BEGIN
        SELECT
            COUNT(CityId) INTO num_CityId
        FROM
            CC_Cities
        WHERE
            CityId = i_CityId;

        IF (num_CityId != 1) THEN
            DBMS_OUTPUT.PUT_LINE('City ID ' || i_CityId || ' does not exist.');
            RETURN -1;
        END IF;

        RETURN 1;
    END;


    PROCEDURE Add_Customer (i_FirstName CC_Customers.FirstName%TYPE, i_LastName CC_Customers.LastName%TYPE, i_Email CC_Customers.Email%TYPE, i_Address CC_Customers.Address%TYPE, i_CityId CC_Customers.CityId%TYPE) 
    AS
        v_CustomerId CC_Customers.CustomerId%TYPE;
        exists_CityId NUMBER;
    BEGIN
        -- Checks that the customer doesn't exist already
        v_CustomerId := Get_CustomerId(UPPER(i_Email));
        IF (v_CustomerId != -1) THEN
            DBMS_OUTPUT.PUT_LINE('The email ' || i_Email || ' is already associated with customer ' || v_CustomerId || '.');
            RAISE Customer_Exists_Already;
        END IF;

        -- Validates CityId exists
        IF (i_cityId IS NOT NULL) THEN
            exists_CityId := Does_CityId_Exists(i_CityId);
            IF (exists_CityId = -1) THEN
                RAISE CityId_Not_Exists;
            END IF;
        END IF;
    
        -- Creates a new customer
        INSERT INTO CC_Customers (FirstName, LastName, Email, Address, CityId) 
        VALUES (UPPER(i_FirstName), UPPER(i_LastName), UPPER(i_Email), UPPER(i_Address), i_CityId);
        
        DBMS_OUTPUT.PUT_LINE('Successfully added customer: ' || i_FirstName || ' ' || i_LastName || '.');
    EXCEPTION
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Error adding customer: ' || i_FirstName || ' ' || i_LastName || '.');
            RAISE;
    END;

    -- Gets the CustomerId of the customer or -1 if it doesn't exist
    FUNCTION Get_CustomerId (i_Email CC_Customers.Email%TYPE)
    RETURN CC_Customers.CustomerId%TYPE
    AS
        v_CustomerId CC_Customers.CustomerId%TYPE;
    BEGIN
        SELECT
            CustomerId INTO v_CustomerId
        FROM
            CC_Customers
        WHERE
            Email = UPPER(i_Email);
        
        RETURN v_CustomerId;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN -1;
    END;

    -- Returns 1 if the CustomerId exists or -1 if it doesn't
    FUNCTION Does_CustomerId_Exists (i_CustomerId CC_Customers.CustomerId%TYPE)
    RETURN NUMBER
    AS
        num_CustomerId NUMBER;
    BEGIN
        SELECT
            COUNT(CustomerId) INTO num_CustomerId
        FROM
            CC_Customers
        WHERE
            CustomerId = i_CustomerId;

        IF (num_CustomerId != 1) THEN
            DBMS_OUTPUT.PUT_LINE('Customer ID ' || i_CustomerId || ' does not exist.');
            RETURN -1;
        END IF;

        RETURN 1;
    END;

    
    PROCEDURE Add_Store (i_Name CC_Stores.Name%TYPE) 
    AS
        v_StoreId CC_Stores.StoreId%TYPE;
    BEGIN
        -- Checks that the store doesn't exist already
        v_StoreId := Get_StoreId(UPPER(i_Name));
        IF (v_StoreId != -1) THEN
            DBMS_OUTPUT.PUT_LINE('A store with the name of ' || i_Name || ' is already associated with store ' || v_StoreId || '.');
            RAISE Store_Exists_Already;
        END IF;
        
        -- Creates a new store
        INSERT INTO CC_Stores (Name) 
        VALUES (UPPER(i_Name));

        DBMS_OUTPUT.PUT_LINE('Successfully added store: ' || i_Name || '.');
    EXCEPTION
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Error adding store: ' || i_Name || '.');
            RAISE;
    END;

    -- Returns the StoreId of the store or -1 if it doesn't exist
    FUNCTION Get_StoreId (i_Name CC_Stores.Name%TYPE)
    RETURN CC_Stores.StoreId%TYPE
    AS
        v_StoreId CC_Stores.StoreId%TYPE;
    BEGIN
       SELECT
            StoreId INTO v_StoreId
        FROM
            CC_Stores
        WHERE
            Name = UPPER(i_Name);
        
        RETURN v_StoreId;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN -1;
    END;

    -- Returns 1 if the StoreId exists or -1 if it doesn't
    FUNCTION Does_StoreId_Exists (i_StoreId CC_Stores.StoreId%TYPE)
    RETURN NUMBER
    AS
        num_StoreId NUMBER;
    BEGIN
        SELECT
            COUNT(StoreId) INTO num_StoreId
        FROM
            CC_Stores
        WHERE
            StoreId = i_StoreId;

        IF (num_StoreId != 1) THEN
            DBMS_OUTPUT.PUT_LINE('Store ID ' || i_StoreId || ' does not exist.');
            RETURN -1;
        END IF;

        RETURN 1;
    END;

    
    PROCEDURE Add_Product(i_Name CC_Products.Name%TYPE, i_Category CC_Products.Category%TYPE, i_UnitPrice CC_Products.UnitPrice%TYPE)
    AS
        v_ProductId CC_Products.ProductId%TYPE;
    BEGIN
        -- Checks that the product doesn't exist already
        v_ProductId := Get_ProductId(UPPER(i_Name));
        IF (v_ProductId != -1) THEN
            DBMS_OUTPUT.PUT_LINE('The product ' || i_Name || ' is already associated with product ' || v_ProductId || '.');
            RAISE Product_Exists_Already;
        END IF;

        -- Validates the value of unit price to be more than 0
        IF (i_UnitPrice IS NOT NULL AND i_UnitPrice <= 0) THEN
            DBMS_OUTPUT.PUT_LINE('A product cannot have a negative unit price.');
            Raise Negative_UnitPrice;
        END IF;

        -- Creates a new product
        INSERT INTO CC_Products (Name, Category)
        VALUES (UPPER(i_Name), UPPER(i_Category));
        
        DBMS_OUTPUT.PUT_LINE('Successfully added product: ' || i_Name || '.');
    EXCEPTION
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Error adding product: ' || i_Name || '.');
            RAISE;
    END;

    -- Returns the ProductId of the product or -1 if it doesn't exist
    FUNCTION Get_ProductId (i_Name CC_Products.Name%TYPE)
    RETURN CC_Products.ProductId%TYPE
    AS
        v_ProductId CC_Products.ProductId%TYPE;
    BEGIN
        SELECT
            ProductId INTO v_ProductId
        FROM
            CC_Products
        WHERE
            Name = UPPER(i_Name);
        
        RETURN v_ProductId;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN -1;
    END;

    -- Returns 1 if the ProductId exists or -1 if it doesn't
    FUNCTION Does_ProductId_Exists (i_ProductId CC_Products.ProductId%TYPE)
    RETURN NUMBER
    AS
        num_ProductId NUMBER;
    BEGIN
        SELECT
            COUNT(ProductId) INTO num_ProductId
        FROM
            CC_Products
        WHERE
            ProductId = i_ProductId;

        IF (num_ProductId != 1) THEN
            DBMS_OUTPUT.PUT_LINE('Product ID ' || i_ProductId || ' does not exist.');
            RETURN -1;
        END IF;

        RETURN 1;
    END;


    PROCEDURE Add_Warehouse (i_Name CC_Warehouses.Name%TYPE, i_Address CC_Warehouses.Address%TYPE, i_CityId CC_Warehouses.CityId%TYPE)
    AS
        v_WarehouseId CC_Warehouses.WarehouseId%TYPE;
        exists_CityId NUMBER;
    BEGIN
        -- Checks that the warehouse doesn't exist already
        v_WarehouseId := Get_WarehouseId(UPPER(i_Name));
        IF (v_WarehouseId != -1) THEN
            DBMS_OUTPUT.PUT_LINE('The warehouse ' || i_Name || ' is already associated with warehouse ' || v_WarehouseId || '.');
            RAISE Warehouse_Exists_Already;
        END IF;

        -- Validate that the CityId exists
        exists_CityId := Does_CityId_Exists(i_CityId);
        IF (exists_CityId = -1) THEN
            RAISE CityId_Not_Exists;
        END IF;

        -- Creates a new warehouse
        INSERT INTO CC_Warehouses (Name, Address, CityId) 
        VALUES (UPPER(i_Name), UPPER(i_Address), i_CityId);

        DBMS_OUTPUT.PUT_LINE('Successfully added warehouse: ' || i_Name || '.');
    EXCEPTION
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Error adding warehouse: ' || i_Name || '.');
            RAISE;
    END;

    -- Returns the WarehouseId of the warehouse or -1 if it doesn't exist
    FUNCTION Get_WarehouseId (i_Name CC_Warehouses.Name%TYPE)
    RETURN CC_Warehouses.WarehouseId%TYPE
    AS
        v_WarehouseId CC_Warehouses.WarehouseId%TYPE;
    BEGIN
        SELECT
            WarehouseId INTO v_WarehouseId
        FROM
            CC_Warehouses
        WHERE
            Name = UPPER(i_Name);
        
        RETURN v_WarehouseId;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN -1;
    END;

    -- Returns 1 if the WarehouseId exists or -1 if it doesn't
    FUNCTION Does_WarehouseId_Exists (i_WarehouseId CC_Warehouses.WarehouseId%TYPE)
    RETURN NUMBER
    AS
        num_WarehouseId NUMBER;
    BEGIN
        SELECT
            COUNT(WarehouseId) INTO num_WarehouseId
        FROM
            CC_Warehouses
        WHERE
            WarehouseId = i_WarehouseId;

        IF (num_WarehouseId != 1) THEN
            DBMS_OUTPUT.PUT_LINE('Warehouse ID ' || i_WarehouseId || ' does not exist.');
            RETURN -1;
        END IF;

        RETURN 1;
    END;


    -- Helper procedure for adding an order by removing the correct amount of quantity for the relevant product
    PROCEDURE Remove_Quantity_From_Warehouses_Products(i_ProductId CC_Warehouses_Products.ProductId%TYPE, i_Quantity CC_Warehouses_Products.Quantity%TYPE)
    AS
        over_Quantity CC_Warehouses_Products.Quantity%TYPE;
        new_Quantity CC_Warehouses_Products.Quantity%TYPE;
        num_Quantity CC_Warehouses_Products.Quantity%TYPE;
    BEGIN
        -- Checks if there is enough quantity
        num_Quantity := Get_Overall_ProductQuantity(i_ProductId);
        IF (num_Quantity < i_Quantity) THEN
            DBMS_OUTPUT.PUT_LINE('The quantity available for product ID ' || i_ProductId || ' is not enough.');
            RAISE Quantity_Not_Enough;
        END IF;

        -- Looping through each warehouse with the product until the necessary quantity is removed
        new_Quantity := i_Quantity;
        FOR row IN (SELECT * FROM CC_Warehouses_Products WHERE ProductId = i_ProductId)
        LOOP
            over_Quantity := row.Quantity - new_Quantity;

            -- All the quantity at this warehouse should be removed, because it matches the quantity needed or doesn't have enough quantity
            IF (over_Quantity < 0) THEN 
                UPDATE 
                    CC_Warehouses_Products 
                SET 
                    Quantity = 0
                WHERE 
                    WarehouseId = row.WarehouseId
                    AND ProductId = row.ProductId;
                new_Quantity := (over_Quantity * -1);
            -- This warehouse has more quantity than the amount of quantity needed to be removed
            ELSE
                UPDATE 
                    CC_Warehouses_Products 
                SET 
                    Quantity = over_Quantity
                WHERE 
                    WarehouseId = row.WarehouseId
                    AND ProductId = row.ProductId;

                EXIT;
            END IF;
        END LOOP;
    END;

    PROCEDURE Add_Order (i_CustomerId CC_Customers.CustomerId%TYPE, i_StoreId CC_Stores.StoreId%TYPE, i_ProductId CC_Products.ProductId%TYPE, i_Quantity CC_Orders.Quantity%TYPE, i_Price CC_Orders.Price%TYPE, i_OrderDate CC_Orders.OrderDate%TYPE)
    AS
        v_OrderId CC_Reviews.ReviewId%TYPE;
        exists_CustomerId NUMBER;
        exists_StoreId NUMBER;
        exists_ProductId NUMBER;
    BEGIN
        -- Validates the CustomerId exists
        exists_CustomerId := Does_CustomerId_Exists(i_CustomerId);
        IF (exists_CustomerId = -1) THEN
            RAISE CustomerId_Not_Exists;
        END IF;

        -- Validates the StoreId exists
        exists_StoreId := Does_StoreId_Exists(i_StoreId);
        IF (exists_StoreId = -1) THEN
            RAISE StoreId_Not_Exists;
        END IF;

        -- Validates the ProductId exists
        exists_ProductId := Does_ProductId_Exists(i_ProductId);
        IF (exists_ProductId != 1) THEN
            RAISE ProductId_Not_Exists;
        END IF;

        -- Checks the order doesn't exist already
        v_OrderId := Get_OrderId(i_CustomerId, i_StoreId, i_ProductId, i_OrderDate);
        IF (v_OrderId != -1) THEN
            DBMS_OUTPUT.PUT_LINE('The order of product ' || i_ProductId || ' from store ' || i_StoreId || 'with customer ' || i_CustomerId || ' on ' || i_OrderDate  || ' is already associated with order ' || v_OrderId || '.');
            RAISE Order_Exists_Already;
        END IF;

        -- Validates the value of the order price to be more than 0
        IF (i_Price <= 0) THEN
            DBMS_OUTPUT.PUT_LINE('The price of the order should be more than 0.');
            RAISE Negative_OrderPrice;
        END IF;

        -- Validates the value of quantity to be more than 0
        IF (i_Quantity <= 0) THEN
            DBMS_OUTPUT.PUT_LINE('The quantity for placing an order should be at least 1.');
            RAISE Negative_Quantity;
        END IF;

        -- Checks the quantity can be removed from the warehouses and if so, updates the quantity of the product at the warehouses
        Remove_Quantity_From_Warehouses_Products(i_ProductID, i_Quantity);

        -- Creates a new order
        INSERT INTO CC_Orders (CustomerId, StoreId, ProductId, Quantity, Price, OrderDate) 
        VALUES (i_CustomerId, i_StoreId, i_ProductId, i_Quantity, i_Price, i_OrderDate);

        DBMS_OUTPUT.PUT_LINE('Successfully added order of product ' || i_ProductId || ' from store ' || i_StoreId || ' for customer ' || i_CustomerId || '.');
    EXCEPTION
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Error adding order of product ' || i_ProductId || ' from customer ' || i_CustomerId || '.');
            RAISE;
    END;
    
    --Returns the OrderId of the order or -1 if it doesn't exist
    FUNCTION Get_OrderId (i_CustomerId CC_Customers.CustomerId%TYPE, i_StoreId CC_Stores.StoreId%TYPE, i_ProductId CC_Products.ProductId%TYPE, i_OrderDate CC_Orders.OrderDate%TYPE)
    RETURN CC_Orders.OrderId%TYPE
        AS
            v_OrderId CC_Orders.OrderId%TYPE;
        BEGIN
            SELECT
                OrderId INTO v_OrderId
            FROM
                CC_Orders
            WHERE
                CustomerId = i_CustomerId AND 
                StoreId = i_StoreId AND
                ProductId = i_ProductId AND
                (OrderDate IS NULL OR OrderDate = i_OrderDate);
            RETURN v_OrderId;
            EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN -1;
    END;

    -- Returns 1 if the OrderId exists or -1 if it doesn't
    FUNCTION Does_OrderId_Exists (i_OrderId CC_Orders.OrderId%TYPE)
    RETURN NUMBER
    AS
        num_OrderId NUMBER;
    BEGIN
        SELECT
            COUNT(OrderId) INTO num_OrderId
        FROM
            CC_Orders
        WHERE
            OrderId = i_OrderId;

        IF (num_OrderId != 1) THEN
            DBMS_OUTPUT.PUT_LINE('Order ID ' || i_OrderId || ' does not exist.');
            RETURN -1;
        END IF;

        RETURN 1;
    END;


    PROCEDURE Add_Review (i_ProductId CC_Products.ProductId%TYPE, i_CustomerId CC_Customers.CustomerId%TYPE, i_Score CC_Reviews.Score%TYPE, i_Description CC_Reviews.Description%TYPE, i_NumFlagged CC_Reviews.NumFlagged%TYPE)
    AS
        v_ReviewId CC_Reviews.ReviewId%TYPE;
        exists_ProductId NUMBER;
        exists_CustomerId NUMBER;
    BEGIN
        -- Validates that ProductId exists
        exists_ProductId := Does_ProductId_Exists(i_ProductId);
        IF (exists_ProductId = -1) THEN
            RAISE ProductId_Not_Exists;
        END IF;

        -- Validates that CustomerId exists
        exists_CustomerId := Does_CustomerId_Exists(i_CustomerId);
        IF (exists_CustomerId = -1) THEN
            RAISE CustomerId_Not_Exists;
        END IF;

        -- Checks the review doesn't exist already
        v_ReviewId := Get_ReviewId(i_ProductId, i_CustomerId);
        IF (v_ReviewId != -1) THEN
            DBMS_OUTPUT.PUT_LINE('Customer ' || i_CustomerId || ' already made a review for product ' || i_ProductId || '.');
            RAISE Review_Exists_Already;
        END IF;

        -- Validates the value of score to be within 1 to 5
        IF (i_Score <= 0 OR i_Score > 5) THEN
            DBMS_OUTPUT.PUT_LINE(i_Score || ' is not a valid score.');
            RAISE Invalid_ReviewScore;
        END IF;

        -- Validates the value of NumFlagged to not be negative
        IF (i_NumFlagged IS NOT NULL AND i_NumFlagged < 0) THEN
            DBMS_OUTPUT.PUT_LINE('A review cannot have a negative number of flags.');
            RAISE Negative_NumFlagged;
        END IF;

        -- Creates a review
        INSERT INTO CC_Reviews (ProductId, CustomerId, Score, Description, NumFlagged) 
        VALUES (i_ProductId, i_CustomerId, i_Score, i_Description, i_NumFlagged);
        DBMS_OUTPUT.PUT_LINE('Successfully added review for product ' || i_ProductId || ' made by customer ' || i_CustomerId || '.');
    EXCEPTION
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Error adding review for product ' || i_ProductId || ' made by customer ' || i_CustomerId || '.');
            RAISE;
    END;

    -- Returns the ReviewId of the review or -1 if it doesn't exist
    FUNCTION Get_ReviewId (i_ProductId CC_Products.ProductId%TYPE, i_CustomerId CC_Customers.CustomerId%TYPE)
    RETURN CC_Reviews.ReviewId%TYPE
    AS
        v_ReviewId CC_Reviews.ReviewId%TYPE;
    BEGIN
        SELECT
            ReviewId INTO v_ReviewId
        FROM
            CC_Reviews
        WHERE
            ProductId = i_ProductId
            AND CustomerId = i_CustomerId;
        
        RETURN v_ReviewId;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN -1;
    END;

    -- Returns 1 if the ReviewId exists or -1 if it doesn't
    FUNCTION Does_ReviewId_Exists (i_ReviewId CC_Reviews.ReviewId%TYPE)
    RETURN NUMBER
    AS
        num_ReviewId NUMBER;
    BEGIN
        SELECT
            COUNT(ReviewId) INTO num_ReviewId
        FROM
            CC_Reviews
        WHERE
            ReviewId = i_ReviewId;

        IF (num_ReviewId != 1) THEN
            DBMS_OUTPUT.PUT_LINE('Review ID ' || i_ReviewId || ' does not exist.');
            RETURN -1;
        END IF;

        RETURN 1;
    END;


    PROCEDURE Add_Warehouse_Product (i_WarehouseId CC_Warehouses_Products.WarehouseId%TYPE, i_ProductId CC_Warehouses_Products.ProductId%TYPE, i_Quantity CC_Warehouses_Products.Quantity%TYPE)
    AS
        v_Quantity CC_Warehouses_Products.Quantity%TYPE;
        exists_WarehouseId NUMBER;
        exists_ProductId NUMBER;
    BEGIN
        -- Validates the WarehouseId exists
        exists_WarehouseId := Does_WarehouseId_Exists(i_WarehouseId);
        IF (exists_WarehouseId = -1) THEN
            RAISE WarehouseId_Not_Exists;
        END IF;

        -- Validates the ProductId exists
        exists_ProductId := Does_ProductId_Exists(i_ProductID);
        IF (exists_ProductId = -1) THEN
            RAISE ProductId_Not_Exists;
        END IF;

        -- Checks this product is not already at this warehouse
        v_Quantity := Get_ProductQuantity_From_Warehouse(i_WarehouseId, i_ProductId);
        IF (v_Quantity != -1) THEN
            DBMS_OUTPUT.PUT_LINE('The product ' || i_ProductId || ' is already associated with warehouse ' || i_WarehouseId || '.');
            RAISE ProductInWarehouse_Exists_Already;
        END IF;

        -- Validates the value of quantity to be more than 0
        IF (i_Quantity < 0) THEN
            DBMS_OUTPUT.PUT_LINE('The quantity ' || i_Quantity || ' is negative. Make sure the value of quantity is at least 0.');
            RAISE Negative_Quantity;
        END IF;

        -- Adds a new product at this warehouse
        INSERT INTO CC_Warehouses_Products (WarehouseId, ProductId, Quantity) 
        VALUES (i_WarehouseId, i_ProductId, i_Quantity);

        DBMS_OUTPUT.PUT_LINE('Successfully added quantity: ' || i_Quantity || ' for product ' || i_ProductId || ' in warehouse ' || i_WarehouseId || '.');
    EXCEPTION
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Error adding quantity: ' || i_Quantity || ' for product ' || i_ProductId || ' in warehouse ' || i_WarehouseId || '.');
            RAISE;
    END;

    -- Returns the quantity of a product at a specific warehouse or -1 if the product or warehouse doens't exist together
    FUNCTION Get_ProductQuantity_From_Warehouse (i_WarehouseId CC_Warehouses_Products.WarehouseId%TYPE, i_ProductId CC_Warehouses_Products.ProductId%TYPE)
    RETURN CC_Warehouses_Products.Quantity%TYPE
    AS
        v_Quantity CC_Warehouses_Products.Quantity%TYPE;
    BEGIN
        SELECT
            Quantity INTO v_Quantity
        FROM
            CC_Warehouses_Products
        WHERE
            WarehouseId = i_WarehouseId
            AND ProductId = i_ProductId;
        
        RETURN v_Quantity;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN -1;
    END;

    -- Returns the quantity of a product obtained by combining all warhouses together
    FUNCTION Get_Overall_ProductQuantity (i_ProductId CC_Warehouses_Products.ProductId%TYPE)
    RETURN CC_Warehouses_Products.Quantity%TYPE
    AS
        v_Quantity CC_Warehouses_Products.Quantity%TYPE;
        exists_ProductId NUMBER;
    BEGIN
        -- Validates the ProductId exists
        exists_ProductId := Does_ProductId_Exists(i_ProductID);
        IF (exists_ProductId = -1) THEN
            RAISE ProductId_Not_Exists;
        END IF;
    
        -- Calculates the total quantity
        SELECT
            SUM(Quantity) INTO v_Quantity
        FROM
            CC_Warehouses_Products
        WHERE
            ProductId = i_ProductId;
        
        RETURN v_Quantity;
    EXCEPTION
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('Error searching the total quantity for product ID ' || i_ProductId || '.');
            RAISE;
    END;

END CC_Package;
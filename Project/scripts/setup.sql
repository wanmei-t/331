CREATE TABLE CC_Countries(
    CountryId           NUMBER(4)           GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    Name                VARCHAR2(30)        NOT NULL UNIQUE
);

CREATE TABLE CC_Provinces(
    ProvinceId          NUMBER(4)           GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    Name                VARCHAR2(30)        NOT NULL,
    CountryId           NUMBER(4)           NOT NULL REFERENCES CC_Countries(CountryId),

    CONSTRAINT CC_Provinces_Name_CountryId_U
    UNIQUE (Name, CountryId)
);

CREATE TABLE CC_Cities(
    CityId              NUMBER(4)           GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    Name                VARCHAR2(30)        NOT NULL,
    ProvinceId          NUMBER(4)           NOT NULL REFERENCES CC_Provinces(ProvinceId),

    CONSTRAINT CC_Cities_Name_ProvinceId_U
    UNIQUE (Name, ProvinceId)
);

CREATE TABLE CC_Customers(
    CustomerId          NUMBER(4)           GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    FirstName           VARCHAR2(30)        NOT NULL,
    LastName            VARCHAR2(30)        NOT NULL,
    Email               VARCHAR2(50)        NOT NULL UNIQUE,
    Address             VARCHAR2(50),
    CityId              NUMBER(4)           REFERENCES CC_Cities(CityId)
);

CREATE TABLE CC_Stores(
    StoreId             NUMBER(4)           GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    Name                VARCHAR2(30)        NOT NULL UNIQUE
);

CREATE TABLE CC_Products(
    ProductId           NUMBER(4)           GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    Name                VARCHAR2(30)        NOT NULL UNIQUE,
    Category            VARCHAR2(30)        NOT NULL,
    UnitPrice           NUMBER(8,2)         
);

CREATE TABLE CC_Warehouses(
    WarehouseId         NUMBER(4)           GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    Name                VARCHAR2(30)        NOT NULL UNIQUE,
    Address             VARCHAR2(50)        NOT NULL,
    CityId              NUMBER(4)           NOT NULL REFERENCES CC_Cities(CityId)
);

CREATE TABLE CC_Orders(
    OrderId             NUMBER(4)           GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    CustomerId          NUMBER(4)           NOT NULL REFERENCES CC_Customers(CustomerId),
    StoreId             NUMBER(4)           NOT NULL REFERENCES CC_Stores(StoreId),
    ProductId           NUMBER(4)           NOT NULL REFERENCES CC_Products(ProductId),
    Quantity            NUMBER(8)           NOT NULL,
    Price               NUMBER(8,2)         NOT NULL,
    OrderDate           DATE
);

CREATE TABLE CC_Reviews(
    ReviewId            NUMBER(4)           GENERATED ALWAYS AS IDENTITY,
    ProductId           NUMBER(4)           NOT NULL REFERENCES CC_Products(ProductId),
    CustomerId          NUMBER(4)           NOT NULL REFERENCES CC_Customers(CustomerId),
    Score               NUMBER(1)           NOT NULL CHECK (Score <= 5 AND Score >= 1),
    Description         VARCHAR2(50),
    -- A review can have 1000 flags. It's up to the admin to delete that review or not.
    NumFlagged          NUMBER(8)           DEFAULT(0),

    -- A customer can only place one review per product otherwise it's ambiguous which ReviewId to return when a
    -- user ask for the ReviewId of a CustomerId and ProductId
    CONSTRAINT CC_Reviews_ProductId_CustomerId_U
    UNIQUE (ProductId, CustomerId)
);

CREATE TABLE CC_Warehouses_Products(
    WarehouseId         NUMBER(4)           NOT NULL REFERENCES CC_Warehouses(WarehouseId),
    ProductId           NUMBER(4)           NOT NULL REFERENCES CC_Products(ProductId),
    Quantity            NUMBER(8)           NOT NULL,

    CONSTRAINT CC_Warehouses_Products_WarehouseId_ProductId_PK
    PRIMARY KEY (WarehouseId, ProductId)
);

/

CREATE TYPE CC_Customer_Type AS OBJECT(
    CustomerId          NUMBER(4),
    FirstName           VARCHAR2(30),
    LastName            VARCHAR2(30),
    Email               VARCHAR2(50),
    Address             VARCHAR2(50),
    CityId              NUMBER(4)  
);
/
CREATE TYPE CC_Store_Type AS OBJECT(
    StoreId             NUMBER(4),
    Name                VARCHAR2(30)   
);
/
CREATE TYPE CC_Review_Type AS OBJECT(
    ReviewId            NUMBER(4),
    ProductId           NUMBER(4),
    CustomerId          NUMBER(4),
    Score               NUMBER(1),
    Description         VARCHAR2(50),
    NumFlagged          NUMBER(8)
);
/
CREATE TYPE CC_Product_Type AS OBJECT (
    ProductId           NUMBER(4),
    Name                VARCHAR2(30),
    Category            VARCHAR2(30),
    UnitPrice           NUMBER(8,2)
);
/
CREATE TYPE CC_Warehouse_Type AS OBJECT (
    WarehouseId         NUMBER(4),
    Name                VARCHAR2(30),
    Address             VARCHAR2(50),
    CityId              NUMBER(4)
);
/
CREATE TYPE CC_Order_Type AS OBJECT (
    OrderId             NUMBER(4),
    CustomerId          NUMBER(4),
    StoreId             NUMBER(4),
    ProductId           NUMBER(4),
    Quantity            NUMBER(8),
    Price               NUMBER(8,2),
    OrderDate           DATE
);

/

CREATE TABLE CC_Warehouses_Products_Audit (
    AuditId             NUMBER(8)               GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    WarehouseId         NUMBER(4)               NOT NULL,
    ProductId           NUMBER(4)               NOT NULL,
    OldQuantity         NUMBER(8)               NOT NULL,
    NewQuantity         NUMBER(8)               NOT NULL,
    Action_Date         DATE                    DEFAULT SYSDATE
);

CREATE TABLE CC_Customers_Audit (
    AuditId             NUMBER(8)               GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    CustomerId          NUMBER(4)               NOT NULL,
    OldEmail            VARCHAR2(50)            NOT NULL,
    NewEmail            VARCHAR2(50)            NOT NULL,
    OldAddress          VARCHAR2(50),
    NewAddress          VARCHAR2(50),
    OldCityId           NUMBER(4),
    NewCityId           NUMBER(4),
    Action_Date         DATE                    DEFAULT SYSDATE
);

CREATE TABLE CC_Warehouses_Audit (
    AuditId             NUMBER(8)               GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    WarehouseId         NUMBER(4)               NOT NULL,
    OldAddress          VARCHAR2(50)            NOT NULL,
    NewAddress          VARCHAR2(50)            NOT NULL,
    OldCityId           NUMBER(4)               NOT NULL,
    NewCityId           NUMBER(4)               NOT NULL,
    Action_Date         DATE                    DEFAULT SYSDATE
);

CREATE TABLE CC_Reviews_Audit (
    AuditId            NUMBER(8)                GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    ReviewId           NUMBER(4)                NOT NULL,
    OldScore           NUMBER(1)                NOT NULL,
    NewScore           NUMBER(1)                NOT NULL,
    OldDescription     VARCHAR2(50),
    NewDescription     VARCHAR2(50),
    OldNumFlagged      NUMBER(8)                NOT NULL,
    NewNumFlagged      NUMBER(8)                NOT NULL,
    Action_Date        DATE                     DEFAULT SYSDATE 
);

CREATE TABLE CC_Products_Audit (
    AuditId            NUMBER(8)                GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    ProductId          NUMBER(4)                NOT NULL,
    OldUnitPrice       NUMBER(8,2),
    NewUnitPrice       NUMBER(8,2),
    Action_Date        DATE                     DEFAULT SYSDATE 
);

/

CREATE OR REPLACE TRIGGER CC_Warehouses_Products_Update_Trigger
AFTER UPDATE ON CC_Warehouses_Products
FOR EACH ROW
BEGIN
    INSERT INTO CC_Warehouses_Products_Audit (WarehouseId, ProductId, OldQuantity, NewQuantity) 
    VALUES (:OLD.WarehouseId, :OLD.ProductId, :OLD.Quantity, :NEW.Quantity);
END;
/
CREATE OR REPLACE TRIGGER CC_Customers_Update_Trigger
BEFORE UPDATE ON CC_Customers
FOR EACH ROW
BEGIN
    INSERT INTO CC_Customers_Audit (CustomerId, OldEmail, newEmail,  OldAddress, NewAddress, OldCityId, NewCityId)
        VALUES (:OLD.CustomerId, :OLD.Email, :NEW.Email, :OLD.Address, :NEW.Address, :OLD.CityId, :NEW.CityId);
END;
/
CREATE OR REPLACE TRIGGER CC_Warehouses_Update_Trigger
AFTER UPDATE ON CC_Warehouses
FOR EACH ROW
BEGIN
    INSERT INTO CC_Warehouses_Audit (WarehouseId, OldAddress, NewAddress, OldCityId, NewCityId) 
        VALUES (:OLD.WarehouseId, :OLD.Address, :NEW.Address, :OLD.CityId, :NEW.CityId);
END;
/
CREATE OR REPLACE TRIGGER CC_Reviews_Update_Trigger
AFTER UPDATE ON CC_Reviews
FOR EACH ROW
BEGIN
    INSERT INTO CC_Reviews_Audit (ReviewId, OldScore, NewScore, OldDescription,  NewDescription, OldNumFlagged, NewNumFlagged)
        VALUES (:OLD.ReviewId, :OLD.Score, :NEW.Score, :OLD.Description, :NEW.Description, :OLD.NumFlagged, :NEW.NumFlagged);
END;
/
CREATE OR REPLACE TRIGGER CC_Products_Update_Trigger
AFTER UPDATE ON CC_Products
FOR EACH ROW
BEGIN
    INSERT INTO CC_Products_Audit (ProductId, OldUnitPrice, NewUnitPrice)
        VALUES (:OLD.ProductId, :OLD.UnitPrice, :NEW.UnitPrice);
END;

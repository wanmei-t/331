DROP TRIGGER CC_Warehouses_Products_Update_Trigger;
DROP TRIGGER CC_Customers_Update_Trigger;
DROP TRIGGER CC_Warehouses_Update_Trigger;
DROP TRIGGER CC_Reviews_Update_Trigger;
DROP TRIGGER CC_Products_Update_Trigger;

DROP PACKAGE CC_Package;
DROP PACKAGE CC_Package_Adv;

DROP TYPE CC_Store_Type;
DROP TYPE CC_Customer_Type;
DROP TYPE CC_Review_Type;
DROP TYPE CC_Product_Type;
DROP TYPE CC_Warehouse_Type;
DROP TYPE CC_Order_Type;

DROP TABLE CC_Warehouses_Products;
DROP TABLE CC_Reviews;
DROP TABLE CC_Orders;
DROP TABLE CC_Warehouses;
DROP TABLE CC_Products;
DROP TABLE CC_Stores;
DROP TABLE CC_Customers;
DROP TABLE CC_Cities;
DROP TABLE CC_Provinces;
DROP TABLE CC_Countries;


DROP TABLE CC_Warehouses_Products_Audit;
DROP TABLE CC_Customers_Audit;
DROP TABLE CC_Warehouses_Audit;
DROP TABLE CC_Reviews_Audit;
DROP TABLE CC_Products_Audit;
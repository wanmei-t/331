CREATE OR REPLACE PACKAGE CC_Package_Adv AS

    PROCEDURE Add_Store_Type (i_StoreId OUT CC_Stores.StoreId%TYPE, i_Store CC_Store_Type);
    PROCEDURE Add_Customer_Type (i_CustomerId OUT CC_Customers.CustomerId%TYPE, i_Customer CC_Customer_Type);
    PROCEDURE Add_Review_Type(i_ReviewId OUT CC_Reviews.ReviewId%TYPE, i_Review CC_Review_Type);
    PROCEDURE Add_Product_Type (i_ProductId OUT CC_Products.ProductId%TYPE, i_Product CC_Product_Type);
    PROCEDURE Add_Warehouse_Type (i_WarehouseId OUT CC_Warehouses.WarehouseId%TYPE, i_Warehouse CC_Warehouse_Type);
    PROCEDURE Add_Order_type (i_OrderId OUT CC_Orders.OrderId%TYPE, i_Order CC_Order_Type);

    FUNCTION Display_Products_Audit
        RETURN VARCHAR2;
    FUNCTION Display_Warehouses_Products_Audit
        RETURN VARCHAR2;
    FUNCTION Display_Customers_Audit
        RETURN VARCHAR2;
    FUNCTION Display_Warehouses_Audit
        RETURN VARCHAR2;
    FUNCTION Display_Reviews_Audit
        RETURN VARCHAR2;
        
    PROCEDURE Modify_NumFlags (i_ReviewId CC_Reviews.ReviewId%TYPE, i_NumFlagged CC_Reviews.NumFlagged%TYPE);
    FUNCTION Get_QuantityOrder_Of_ProductId (i_ProductID CC_Products.ProductId%TYPE)
        RETURN NUMBER;
    PROCEDURE Modify_Review (i_ReviewId CC_Reviews.ReviewId%TYPE, i_Score CC_Reviews.Score%TYPE, i_Description CC_Reviews.Description%TYPE);
    FUNCTION Get_All_Reviews_For_ProductID (i_ProductId CC_Products.ProductId%TYPE)
        RETURN VARCHAR2;
    PROCEDURE Change_Product_UnitPrice (i_ProductId CC_Products.ProductId%TYPE, i_UnitPrice CC_Products.UnitPrice%TYPE);
    FUNCTION Get_All_Dangerous_Reviews
        RETURN VARCHAR2;
    PROCEDURE Delete_Review(i_ReviewId IN CC_Reviews.ReviewId%TYPE);
    PROCEDURE Flag_Review(i_ReviewId IN CC_Reviews.ReviewId%TYPE);
    FUNCTION Get_Customers_With_Dangerous_Reviews
        RETURN VARCHAR2; 
    FUNCTION Get_All_Countries
        RETURN VARCHAR2;
    FUNCTION Get_All_Provinces
        RETURN VARCHAR2;
    FUNCTION Get_All_Cities
        RETURN VARCHAR2;
    FUNCTION Get_All_Products
        RETURN VARCHAR2;
    FUNCTION Get_All_Warehouses
        RETURN VARCHAR2;
    FUNCTION Get_All_Stores
        RETURN VARCHAR2;
    FUNCTION Get_All_Customers
        RETURN VARCHAR2;
    FUNCTION Get_AverageScore_For_ProductId (i_ProductId CC_Products.ProductId%TYPE)
        RETURN VARCHAR2;
    FUNCTION Get_All_Products_Of_Category (i_Category CC_Products.Category%TYPE)
        RETURN VARCHAR2;
    FUNCTION Get_All_Orders_For_StoreId (i_StoreId CC_Stores.StoreId%TYPE)
        RETURN VARCHAR2;
    FUNCTION Get_All_Orders_For_CustomerId (i_CustomerId CC_Customers.CustomerId%TYPE)
        RETURN VARCHAR2;
    PROCEDURE Add_Quantity_At_Warehouse(i_WarehouseId CC_Warehouses.WarehouseId%TYPE, i_productId CC_Products.ProductId%TYPE, i_Quantity CC_Warehouses_Products.Quantity%TYPE);
    PROCEDURE Delete_Product(i_ProductId CC_Products.ProductId%TYPE);
    PROCEDURE Delete_Warehouse(i_WarehouseId CC_Warehouses.WarehouseId%TYPE);
    PROCEDURE Delete_Order(i_OrderId CC_Orders.OrderId%TYPE);
    PROCEDURE Update_Email(i_CustomerId CC_Customers.CustomerId%TYPE, i_NewEmail CC_Customers.Email%TYPE);
    PROCEDURE Update_Customer_Address(i_CustomerId CC_Customers.CustomerId%TYPE, i_NewAddress CC_Customers.Address%TYPE);
    PROCEDURE Update_Customer_CityId(i_CustomerId CC_Customers.CustomerId%TYPE, i_NewCityId CC_Customers.CityId%TYPE);
    PROCEDURE Update_Warehouse_Address(i_WarehouseId CC_Warehouses.WarehouseId%TYPE, i_NewAddress CC_Customers.Address%TYPE);
    PROCEDURE Update_Warehouse_CityId(i_WarehouseId CC_Warehouses.WarehouseId%TYPE, i_NewCityId CC_Customers.CityId%TYPE);

END CC_Package_Adv;
/
CREATE OR REPLACE PACKAGE BODY CC_Package_Adv AS

    -- Adds a Store to the database using a Java Store object and returns the ID of the newly created Store
    PROCEDURE Add_Store_Type (i_StoreId OUT CC_Stores.StoreId%TYPE, i_Store CC_Store_Type) AS
    BEGIN
        CC_Package.Add_Store(i_Store.Name);
        i_StoreId := CC_Package.Get_StoreId(i_Store.Name);
    END;

    -- Adds a Product to the database using a Java Product object and returns the ID of the newly created Product
    PROCEDURE Add_Product_Type (i_ProductId OUT CC_Products.ProductId%TYPE, i_Product CC_Product_Type) AS
    BEGIN
        CC_Package.Add_Product(i_Product.Name, i_Product.Category, i_Product.UnitPrice);
        i_ProductId := CC_Package.Get_ProductId(i_Product.Name);
    END;

    -- Adds a Customer to the database using a Java Customer object and returns the ID of the newly created Customer
    PROCEDURE Add_Customer_Type (i_CustomerId OUT CC_Customers.CustomerId%TYPE, i_Customer CC_Customer_Type) AS
    BEGIN
        CC_Package.Add_Customer(i_Customer.FirstName, i_Customer.LastName, i_Customer.Email, i_Customer.Address, i_Customer.CityId);
        i_CustomerId := CC_Package.Get_CustomerId(i_Customer.Email);
    END;

    -- Adds a Review to the database using a Java Review object and returns the ID of the newly created Review
    PROCEDURE Add_Review_Type(i_ReviewId OUT CC_Reviews.ReviewId%TYPE, i_Review CC_Review_Type) AS
    BEGIN
        CC_Package.Add_Review(i_Review.ProductId, i_Review.CustomerId, i_Review.Score, i_Review.Description, i_Review.NumFlagged);
        i_reviewId := CC_Package.Get_ReviewId(i_Review.ProductId, i_Review.CustomerId);
    END;

    -- Adds a Warehouse to the database using a Java Warehouse object and returns the ID of the newly created Warehouse
    PROCEDURE Add_Warehouse_Type (i_WarehouseId OUT CC_Warehouses.WarehouseId%TYPE, i_Warehouse CC_Warehouse_Type) AS
    BEGIN
        CC_Package.Add_Warehouse(i_Warehouse.Name, i_Warehouse.Address, i_Warehouse.CityId);
        i_WarehouseId := CC_Package.Get_WarehouseId(i_Warehouse.Name);
    END;

    -- Adds a Order to the database using a Java Order object and returns the ID of the newly created Order
    PROCEDURE Add_Order_Type (i_OrderId OUT CC_Orders.OrderId%TYPE, i_Order CC_Order_Type) AS
    BEGIN
        CC_Package.Add_Order (i_Order.CustomerId, i_Order.StoreId, i_Order.ProductId, i_Order.Quantity, i_Order.Price, i_Order.OrderDate);
        i_OrderId := CC_Package.Get_OrderId(i_Order.CustomerId, i_Order.StoreId, i_Order.ProductId, i_Order.OrderDate);
    END;


    -- Returns a string containing all the information of the Product's audit
    FUNCTION Display_Products_Audit
    RETURN VARCHAR2
    AS
        v_string VARCHAR2(32767);
    BEGIN
    FOR row IN (SELECT * FROM CC_Products_Audit)
        LOOP
            v_string :=  v_string ||
            'Audit ID: ' || TO_CHAR(row.AuditId) ||
            ' | Product ID: ' || TO_CHAR(row.ProductId) ||
            ' | Old Unit Price: ' || TO_CHAR(row.OldUnitPrice) ||
            ' | New Unit Price: ' || TO_CHAR(row.NewUnitPrice) ||
            ' | Action Date: ' || TO_CHAR(row.Action_Date, 'YYYY-MM-DD HH24:MI:SS') ||
            ';';
        END LOOP;
        
        IF v_string IS NULL THEN
            v_string := 'No data available for this audit table.';
        END IF;

        RETURN v_string;
    END;


    -- Returns a string containing all the information of the Warehouse_Product's audit
    FUNCTION Display_Warehouses_Products_Audit
    RETURN VARCHAR2
    AS
        v_string VARCHAR2(32767);
    BEGIN
        FOR row IN (SELECT * FROM CC_Warehouses_Products_Audit)
        LOOP
            v_string := v_string ||
            'Audit ID: ' || TO_CHAR(row.AuditId) ||
            ' | Warehouse ID: ' || TO_CHAR(row.WarehouseId) ||
            ' | Product ID: ' || TO_CHAR(row.ProductId) ||
            ' | Old Quantity: ' || TO_CHAR(row.OldQuantity) ||
            ' | New Quantity: ' || TO_CHAR(row.NewQuantity) ||
            ' | Action Date: ' || TO_CHAR(row.Action_Date, 'YYYY-MM-DD HH24:MI:SS') ||
            ';';
        END LOOP;
        
        IF v_string IS NULL THEN
            v_string := 'No data available for this audit table.';
        END IF;

        RETURN v_string;
    END;
    
    -- Returns a string containing all the information of the Customer's audit
    FUNCTION Display_Customers_Audit
    RETURN VARCHAR2
    AS
        v_string VARCHAR2(32767);
    BEGIN
        FOR row IN (SELECT * FROM CC_Customers_Audit)
        LOOP
            v_string := v_string ||
            'Audit ID: ' || TO_CHAR(row.AuditId) ||
            ' | Customer ID: ' || TO_CHAR(row.CustomerId) ||
            ' | Old Email: ' || row.OldEmail ||
            ' | New Email: ' || row.NewEmail ||
            ' | Old Address: ' || row.OldAddress ||
            ' | New Address: ' || row.NewAddress ||
            ' | Old City ID: ' || TO_CHAR(row.OldCityId) ||
            ' | New City ID: ' || TO_CHAR(row.NewCityId) ||
            ' | Action Date: ' || TO_CHAR(row.Action_Date, 'YYYY-MM-DD HH24:MI:SS') ||
            ';';
        END LOOP;

        IF v_string IS NULL THEN
            v_string := 'No data available for this audit table.';
        END IF;

        RETURN v_string;
    END;

    -- Returns a string containing all the information of the Warehouse's audit
    FUNCTION Display_Warehouses_Audit
    RETURN VARCHAR2
    AS
        v_string VARCHAR2(32767);
        v_dataCount NUMBER;
    BEGIN
        FOR row IN (SELECT * FROM CC_Warehouses_Audit)
        LOOP
            v_string :=  v_string ||
            'Audit ID: ' || TO_CHAR(row.AuditId) ||
            ' | Warehouse ID: ' || TO_CHAR(row.WarehouseId) ||
            ' | Old Address: ' || row.OldAddress ||
            ' | New Address: ' || row.NewAddress ||
            ' | Old City ID: ' || TO_CHAR(row.OldCityId) ||
            ' | New City ID: ' || TO_CHAR(row.NewCityId) ||
            ' | Action Date: ' || TO_CHAR(row.Action_Date, 'YYYY-MM-DD HH24:MI:SS') ||
            ';';
        END LOOP;

        IF v_string IS NULL THEN
            v_string := 'No data available for this audit table.';
        END IF;

        RETURN v_string;
    END;

    -- Returns a string containing all the information of the Reviewa's audit
    FUNCTION Display_Reviews_Audit
    RETURN VARCHAR2
    AS
        v_string VARCHAR2(32767);
    BEGIN
        FOR row IN (SELECT * FROM CC_Reviews_Audit)
        LOOP
            v_string :=  v_string ||
            'Audit ID: ' || TO_CHAR(row.AuditId) ||
            ' | Review ID: ' || TO_CHAR(row.ReviewId) ||
            ' | Old Score: ' || TO_CHAR(row.OldScore) ||
            ' | New Score: ' || TO_CHAR(row.NewScore) ||
            ' | Old Description: ' || row.OldDescription ||
            ' | New Description: ' || row.NewDescription ||
            ' | Old Number Of Flags: ' || TO_CHAR(row.OldNumFlagged) ||
            ' | New Number OfFlags: ' || TO_CHAR(row.NewNumFlagged) ||
            ' | Action Date: ' || TO_CHAR(row.Action_Date, 'YYYY-MM-DD HH24:MI:SS') ||
            ';';
        END LOOP;

        IF v_string IS NULL THEN
            v_string := 'No data available for this audit table.';
        END IF;

        RETURN v_string;
    END;


    PROCEDURE Modify_NumFlags (i_ReviewId CC_Reviews.ReviewId%TYPE, i_NumFlagged CC_Reviews.NumFlagged%TYPE)
    AS
        exists_ReviewId NUMBER;
    BEGIN
        -- Validates ReviewId exists
        exists_ReviewId := CC_Package.Does_ReviewId_Exists(i_ReviewId);
        IF (exists_ReviewId = -1) THEN
            RAISE CC_Package.ReviewId_Not_Exists;
        END IF;

        -- Validates the value of NumFlagged to not be negative
        IF (i_NumFlagged < 0) THEN
            DBMS_OUTPUT.PUT_LINE('A review cannot have a negative number of flags.');
            RAISE CC_Package.Negative_NumFlagged;
        END IF;

        -- Changes the number of flags this review has
        UPDATE CC_Reviews
        SET NumFlagged = i_NumFlagged
        WHERE ReviewId = i_ReviewId;

        DBMS_OUTPUT.PUT_LINE('Successfully modified the number of flags of review ' || i_ReviewId || '.');
    END;

    -- Returns the number of orders that purchased this product
    FUNCTION Get_QuantityOrder_Of_ProductId (i_ProductID CC_Products.ProductId%TYPE)
    RETURN NUMBER
    AS
        exists_ProductID NUMBER;
        num_Order NUMBER;
    BEGIN
        -- Validates ProductId exists
        exists_ProductID := CC_Package.Does_ProductId_Exists(i_ProductID);
        IF (exists_ProductID = -1) THEN
            RAISE CC_Package.ProductId_Not_Exists;
        END IF;

        SELECT
            COUNT(OrderId) INTO num_Order
        FROM
            CC_Orders
        WHERE
            ProductId = i_ProductID;

        RETURN num_Order;
    END;


    PROCEDURE Modify_Review (i_ReviewId CC_Reviews.ReviewId%TYPE, i_Score CC_Reviews.Score%TYPE, i_Description CC_Reviews.Description%TYPE)
    AS
        exists_ReviewId NUMBER;
    BEGIN
        -- Validates ReviewId exists
        exists_ReviewId := CC_Package.Does_ReviewId_Exists(i_ReviewId);
        IF (exists_ReviewId = -1) THEN
            RAISE CC_Package.ReviewId_Not_Exists;
        END IF;

        -- Validates the value of the unit price is more than 0
        IF (i_Score <= 0 OR i_Score > 5) THEN
            DBMS_OUTPUT.PUT_LINE('The score of a review has to be between 1 and 5.');
            Raise CC_Package.Invalid_ReviewScore;
        END IF;

        -- Changes the score and description of the review
        UPDATE CC_Reviews
        SET Score = i_Score, Description = i_Description
        WHERE ReviewId = i_ReviewId;

        DBMS_OUTPUT.PUT_LINE('Successfully modified the score and desciption of review ' || i_ReviewId || '.');
    END;

    -- Returns a string containing all the reviews of the product
    FUNCTION Get_All_Reviews_For_ProductID (i_ProductId CC_Products.ProductId%TYPE) 
    RETURN VARCHAR2
    AS
        v_string VARCHAR2(32767);
        exists_ProductId NUMBER;
    BEGIN
        -- Validates ProductId exists
        exists_ProductId := CC_Package.Does_ProductId_Exists(i_ProductId);
        IF (exists_ProductId = -1) THEN
            RAISE CC_Package.ProductId_Not_Exists;
        END IF;
        
        FOR row IN (SELECT
                        *
                    FROM
                        CC_Reviews
                    WHERE
                        ProductId = i_ProductId)
        LOOP
            v_string :=  v_string ||
            'Review ID: ' || TO_CHAR(row.ReviewId) ||
            ' | Product ID: ' || TO_CHAR(row.ProductId) ||
            ' | Customer ID: ' || TO_CHAR(row.CustomerId) ||
            ' | Score: ' || TO_CHAR(row.Score) ||
            ' | Description: ' || row.Description ||
            ' | Number Of Flags: ' || TO_CHAR(row.NumFlagged) ||
            ';';
        END LOOP;
        
        IF v_string IS NULL THEN
            v_string := 'There are no reviews for this product.';
        END IF;
        
        RETURN v_string;
    END;

    PROCEDURE Change_Product_UnitPrice (i_ProductId CC_Products.ProductId%TYPE, i_UnitPrice CC_Products.UnitPrice%TYPE)
    AS
        exists_ProductID NUMBER;
    BEGIN
        -- Validates ProductId exists
        exists_ProductID := CC_Package.Does_ProductId_Exists(i_ProductID);
        IF (exists_ProductID = -1) THEN
            RAISE CC_Package.ProductId_Not_Exists;
        END IF;

        -- Validates the value of the unit price is more than 0
        IF (i_UnitPrice <= 0) THEN
            DBMS_OUTPUT.PUT_LINE('A product cannot have a negative unit price.');
            Raise CC_Package.Negative_UnitPrice;
        END IF;

        -- Changes the unit price of the product
        UPDATE CC_Products
        SET UnitPrice = i_UnitPrice
        WHERE ProductId = i_ProductID;

        DBMS_OUTPUT.PUT_LINE('Successfully modified the price of product ' || i_ProductID || '.');
    END;

    -- Returns a string containing all the reviews that are flagged at least twice
    FUNCTION Get_All_Dangerous_Reviews
    RETURN VARCHAR2
    AS
        review_string VARCHAR2(32767);
    BEGIN
        FOR review IN (SELECT * FROM CC_Reviews WHERE NumFlagged >= 2)
        LOOP
            review_string :=  review_string ||
            'ReviewId: ' || TO_CHAR(review.ReviewId) ||
            ' | Product ID: ' || TO_CHAR(review.ProductId) ||
            ' | Customer ID: ' || TO_CHAR(review.CustomerId) ||
            ' | Score: ' || TO_CHAR(review.Score) ||
            ' | Description: ' || review.Description ||
            ' | Number Of Flags: ' || TO_CHAR(review.NumFlagged) ||
            ';';
        END LOOP;
        
        IF review_string IS NULL THEN
            review_string := 'There are currently no dangerous reviews.';
        END IF;
        
        RETURN review_string;
    END;

    PROCEDURE Delete_Review(i_ReviewId IN CC_Reviews.ReviewId%TYPE)
    AS
        exists_ReviewId NUMBER;
    BEGIN
        -- Validates ReviewId exists
        exists_ReviewId := CC_Package.Does_ReviewId_Exists(i_ReviewId);
        IF (exists_ReviewId = -1) THEN 
            RAISE CC_Package.ReviewId_Not_Exists;
        END IF;

        -- Deletes a review
        DELETE FROM CC_Reviews WHERE ReviewId = i_ReviewId;
    END;

    PROCEDURE Flag_Review(i_ReviewId IN CC_Reviews.ReviewId%TYPE)
    AS
        exists_ReviewId NUMBER;
        current_numFlagged NUMBER;
    BEGIN
        -- Validates ReviewId exists
        exists_ReviewId := CC_Package.Does_ReviewId_Exists(i_ReviewId);
        IF (exists_ReviewId = -1) THEN
            RAISE CC_Package.ReviewId_Not_Exists;
        END IF;

        -- Selects the current number of flags of the review
        SELECT
            NumFlagged
        INTO
            current_numFlagged
        FROM
            CC_Reviews
        WHERE
            ReviewId = i_ReviewId;

        -- Increments the number of flags of the review by 1
        UPDATE CC_Reviews 
        SET NumFlagged = current_numFlagged + 1
        WHERE ReviewId = i_ReviewId;
    END;

    -- Returns a string containing all the customers with a review that are flagged at least twice
    FUNCTION Get_Customers_With_Dangerous_Reviews
    RETURN VARCHAR2
    AS
        v_string VARCHAR2(32767);
    BEGIN
        FOR row IN (SELECT DISTINCT
                        CustomerId,
                        FirstName,
                        LastName
                    FROM
                        CC_Reviews R INNER JOIN CC_Customers C
                        USING (CustomerId)
                    WHERE
                        NumFlagged >= 2)
        LOOP
            v_string := v_string ||
                        'Customer ID: ' || TO_CHAR(row.CustomerId) ||
                        ' | First Name: ' || row.FirstName ||
                        ' | Last Name: ' || row.LastName ||
                        ';';
        END LOOP;

        IF v_string IS NULL THEN
            v_string := 'There are no customers with dangerous reviews.';
        END IF;
        
        RETURN v_string;
    END;

    -- Returns a string containing all the countries
    FUNCTION Get_All_Countries
    RETURN VARCHAR2
    AS 
        v_string VARCHAR2(32767);
    BEGIN
        FOR row IN (SELECT
                        *
                    FROM
                        CC_Countries)
        LOOP
            v_string := v_string ||
                        'Country ID: ' || TO_CHAR(row.CountryId) ||
                        ' | Country Name: ' || row.Name ||
                        ';';
        END LOOP;

        IF v_string IS NULL THEN
            v_string := 'There are no countries yet.';
        END IF;

        RETURN v_string;
    END;

    -- Returns a string containing all the provinces
    FUNCTION Get_All_Provinces
    RETURN VARCHAR2
    AS 
        v_string VARCHAR2(32767);
    BEGIN
        FOR row IN (SELECT
                        *
                    FROM
                        CC_Provinces)
        LOOP
            v_string := v_string ||
                        'Province ID: ' || TO_CHAR(row.ProvinceId) ||
                        ' | Province Name: ' || row.Name ||
                        ' | Country ID: ' || TO_CHAR(row.CountryId) ||
                        ';';
        END LOOP;

        IF v_string IS NULL THEN
            v_string := 'There are no provinces yet.';
        END IF;

        RETURN v_string;
    END;

    -- Returns a string containing all the cities
    FUNCTION Get_All_Cities
    RETURN VARCHAR2
    AS 
        v_string VARCHAR2(32767);
    BEGIN
        FOR row IN (SELECT
                        *
                    FROM
                        CC_Cities)
        LOOP
            v_string := v_string ||
                        'City ID: ' || TO_CHAR(row.CityId) ||
                        ' | City Name: ' || row.Name ||
                        ' | Province ID: ' || TO_CHAR(row.ProvinceId) ||
                        ';';
        END LOOP;

        IF v_string IS NULL THEN
            v_string := 'There are no cities yet.';
        END IF;

        RETURN v_string;
    END;

    -- Returns a string containing all the products
    FUNCTION Get_All_Products
    RETURN VARCHAR2
    AS 
        v_string VARCHAR2(32767);
    BEGIN
        FOR row IN (SELECT
                        *
                    FROM
                        CC_Products)
        LOOP
            v_string := v_string ||
                        'Product ID: ' || TO_CHAR(row.ProductId) ||
                        ' | Name: ' || row.Name ||
                        ' | Category: ' || row.Category ||
                        ' | Unit Price: ' || TO_CHAR(row.UnitPrice) ||
                        ';';
        END LOOP;

        IF v_string IS NULL THEN
            v_string := 'There are no products yet.';
        END IF;

        RETURN v_string;
    END;

    -- Returns a string containing all the warehouses
    FUNCTION Get_All_Warehouses
    RETURN VARCHAR2
    AS 
        v_string VARCHAR2(32767);
    BEGIN
        FOR row IN (SELECT
                        *
                    FROM
                        CC_Warehouses)
        LOOP
            v_string := v_string ||
                        'Warehouse ID: ' || TO_CHAR(row.WarehouseId) ||
                        ' | Name: ' || row.Name ||
                        ' | Address: ' || row.Address ||
                        ' | City ID: ' || TO_CHAR(row.CityId) ||
                        ';';
        END LOOP;

        IF v_string IS NULL THEN
            v_string := 'There are no warehouses yet.';
        END IF;

        RETURN v_string;
    END;

    -- Returns a string containing all the stores
    FUNCTION Get_All_Stores
    RETURN VARCHAR2
    AS 
        v_string VARCHAR2(32767);
    BEGIN
        FOR row IN (SELECT
                        *
                    FROM
                        CC_Stores)
        LOOP
            v_string := v_string ||
                        'Store ID: ' || TO_CHAR(row.StoreId) ||
                        ' | Name: ' || row.Name ||
                        ';';
        END LOOP;

        IF v_string IS NULL THEN
            v_string := 'There are no stores yet.';
        END IF;

        RETURN v_string;
    END;

    -- Returns a string containing all the customers
    FUNCTION Get_All_Customers
    RETURN VARCHAR2
    AS 
        v_string VARCHAR2(32767);
    BEGIN
        FOR row IN (SELECT
                        *
                    FROM
                        CC_Customers)
        LOOP
            v_string := v_string ||
                        'Customer ID: ' || TO_CHAR(row.CustomerId) ||
                        ' | First Name: ' || row.FirstName ||
                        ' | Last Name: ' || row.LastName ||
                        ' | Email: ' || row.Email ||
                        ' | Address: ' || row.Address ||
                        ' | City ID: ' || TO_CHAR(row.CityId) ||
                        ';';
        END LOOP;

        IF v_string IS NULL THEN
            v_string := 'There are no customers yet.';
        END IF;

        RETURN v_string;
    END;

    -- Returns the average score of a product
    FUNCTION Get_AverageScore_For_ProductId (i_ProductId CC_Products.ProductId%TYPE)
    RETURN VARCHAR2
    AS
        v_Score NUMBER;
        exists_ProductID NUMBER;
    BEGIN
        -- Validates ProductId exists
        exists_ProductID := CC_Package.Does_ProductId_Exists(i_ProductId);
        IF (exists_ProductID = -1) THEN
            RAISE CC_Package.ProductId_Not_Exists; 
        END IF;

        -- Calculates the average score for this product
        SELECT
            AVG(Score) INTO v_Score
        FROM
            CC_Reviews
        WHERE
            ProductId = i_ProductId
            AND Score IS NOT NULL;
        
        IF v_Score IS NULL THEN
            RETURN 'There is no score available yet for this product.';
        END IF;

        RETURN 'The average score for product ID ' || i_ProductId || ' is ' || v_Score || '/5.';
    END;

    -- Returns a string containing all the products of this specific category
    FUNCTION Get_All_Products_Of_Category (i_Category CC_Products.Category%TYPE)
    RETURN VARCHAR2
    AS
        v_string VARCHAR2(32767);
    BEGIN
        FOR row IN (SELECT
                        *
                    FROM
                        CC_Products
                    WHERE
                        Category = UPPER(i_Category))
        LOOP
            v_string := v_string ||
                        'Product ID: ' || TO_CHAR(row.ProductId) ||
                        ' | Name: ' || row.Name ||
                        ' | Category: ' || row.Category ||
                        ' | Unit Price: ' || TO_CHAR(row.UnitPrice) ||
                        ';';
        END LOOP;

        IF v_string IS NULL THEN
            v_string := 'There are no products of this category.';
        END IF;

        RETURN v_string;
    END;

    -- Returns a string containing all the orders placed at this store
    FUNCTION Get_All_Orders_For_StoreId (i_StoreId CC_Stores.StoreId%TYPE)
    RETURN VARCHAR2
    AS 
        v_string VARCHAR2(32767);
        exists_StoreId NUMBER;
    BEGIN
        -- Validates StoreId exists
        exists_StoreId := CC_Package.Does_StoreId_Exists(i_StoreId);
        IF (exists_StoreId = -1) THEN 
            RAISE CC_Package.StoreId_Not_Exists;
        END IF;

        FOR row IN (SELECT
                        *
                    FROM
                        CC_Orders
                    WHERE
                        StoreId = i_StoreId)
        LOOP
            v_string := v_string ||
                        ' | Order ID: ' || TO_CHAR(row.OrderId) ||
                        ' | Customer ID: ' || TO_CHAR(row.CustomerId) ||
                        ' | Store ID: ' || TO_CHAR(row.StoreId) ||
                        ' | Product ID: ' || TO_CHAR(row.ProductId) ||
                        ' | Quantity: ' || TO_CHAR(row.Quantity) ||
                        ' | Price: ' || TO_CHAR(row.Price) ||
                        ' | Order Date: ' || TO_CHAR(row.OrderDate) ||
                        ';';
        END LOOP;

        IF v_string IS NULL THEN
            v_string := 'There are no orders at this store yet.';
        END IF;

        RETURN v_string;
    END;

    -- Returns a string containing all the orders placed by this customer
    FUNCTION Get_All_Orders_For_CustomerId (i_CustomerId CC_Customers.CustomerId%TYPE)
    RETURN VARCHAR2
    AS 
        v_string VARCHAR2(32767);
        exists_CustomerId NUMBER;
    BEGIN
        -- Validates CustomerId exists
        exists_CustomerId := CC_Package.Does_CustomerId_Exists(i_CustomerId);
        IF (exists_CustomerId = -1) THEN 
            RAISE CC_Package.CustomerId_Not_Exists;
        END IF;

        FOR row IN (SELECT
                        *
                    FROM
                        CC_Orders
                    WHERE
                        CustomerId = i_CustomerId)
        LOOP
            v_string := v_string ||
                        ' | Order ID: ' || TO_CHAR(row.OrderId) ||
                        ' | Customer ID: ' || TO_CHAR(row.CustomerId) ||
                        ' | Store ID: ' || TO_CHAR(row.StoreId) ||
                        ' | Product ID: ' || TO_CHAR(row.ProductId) ||
                        ' | Quantity: ' || TO_CHAR(row.Quantity) ||
                        ' | Price: ' || TO_CHAR(row.Price) ||
                        ' | Order Date: ' || TO_CHAR(row.OrderDate) ||
                        ';';
        END LOOP;

        IF v_string IS NULL THEN
            v_string := 'This customer did not place any orders yet.';
        END IF;

        RETURN v_string;
    END;

    PROCEDURE Add_Quantity_At_Warehouse(i_WarehouseId CC_Warehouses.WarehouseId%TYPE, i_ProductId CC_Products.ProductId%TYPE, i_Quantity CC_Warehouses_Products.Quantity%TYPE)
    AS
        currentQuantity NUMBER;
        doesWarehouseExist NUMBER;
        doesProductExist NUMBER;
    BEGIN
        -- Validates WarehouseId exists
        doesWarehouseExist := CC_Package.Does_WarehouseId_Exists(i_WarehouseId);
        IF (doesWarehouseExist != 1) THEN
            RAISE CC_Package.WarehouseId_Not_Exists;
        END IF;

        -- Validates ProductId exists
        doesProductExist := CC_Package.Does_ProductId_Exists(i_ProductId);
        IF (doesProductExist != 1) THEN
            RAISE CC_Package.ProductId_Not_Exists;
        END IF;

        -- Selects the current quantity of this product at this warehouse
        SELECT 
            Quantity
        INTO
            currentQuantity
        FROM
            CC_Warehouses_Products
        WHERE
            WarehouseId = i_WarehouseId AND ProductId = i_ProductId;

        -- Incremets the quantity of this product at this warehouse
        UPDATE CC_Warehouses_Products
        SET Quantity = currentQuantity + i_Quantity
        WHERE WarehouseId = i_WarehouseId AND ProductId = i_ProductId;
    END;

    PROCEDURE Delete_Product(i_ProductId CC_Products.ProductId%TYPE)
    AS
        doesProductExist NUMBER;
    BEGIN
        -- Validates ProductId exists
        doesProductExist := CC_Package.Does_ProductId_Exists(i_ProductId);
        IF (doesProductExist != 1) THEN 
            RAISE CC_Package.ProductId_Not_Exists;
        END IF;

        -- Deletes the product by removing all of its quantity
        UPDATE CC_Warehouses_Products
        SET Quantity = 0
        WHERE ProductId = i_ProductId;
    END;


    PROCEDURE Delete_Warehouse(i_WarehouseId CC_Warehouses.WarehouseId%TYPE) 
    AS
        doesWarehouseExist NUMBER;
    BEGIN
        -- Validates that WarehouseId exists
        doesWarehouseExist := CC_Package.Does_WarehouseId_Exists(i_WarehouseId);
        IF (doesWarehouseExist != 1) THEN
            RAISE CC_Package.WarehouseId_Not_Exists;
        END IF;
        
        -- Deletes all information related to this warehouse
        DELETE FROM CC_Warehouses_Products
        WHERE WarehouseId = i_WarehouseId;
        DELETE FROM CC_Warehouses
        WHERE WarehouseId = i_WarehouseId;
    END;

    PROCEDURE Delete_Order(i_OrderId CC_Orders.OrderId%TYPE)
    AS
        exists_OrderId NUMBER;
    BEGIN
        -- Validates OrderId exists
        exists_OrderId := CC_Package.Does_OrderId_Exists(i_OrderId);
        IF (exists_OrderId = -1) THEN
            RAISE CC_Package.OrderId_Not_Exists;
        END IF;

        -- Deletes this order
        DELETE FROM CC_Orders
        WHERE OrderId = i_OrderId;
    END;

    PROCEDURE Update_Email(i_CustomerId CC_Customers.CustomerId%TYPE, i_NewEmail CC_Customers.Email%TYPE)
    AS
        doesCustomerExist NUMBER;
    BEGIN
        doesCustomerExist := CC_Package.Does_CustomerId_Exists(i_CustomerId);
        IF (doesCustomerExist != 1) THEN
            RAISE CC_Package.CustomerId_Not_Exists;
        END IF;

        UPDATE CC_Customers 
        SET Email = i_newEmail
        WHERE CustomerId = i_CustomerId;
    END;

    PROCEDURE Update_Customer_Address(i_CustomerId CC_Customers.CustomerId%TYPE, i_NewAddress CC_Customers.Address%TYPE)
    AS
        doesCustomerExist NUMBER;
    BEGIN
        doesCustomerExist := CC_Package.Does_CustomerId_Exists(i_CustomerId);
        IF (doesCustomerExist != 1) THEN
            RAISE CC_Package.CustomerId_Not_Exists;
        END IF;

        UPDATE CC_Customers 
        SET Address = i_NewAddress
        WHERE CustomerId = i_CustomerId;
    END;


    PROCEDURE Update_Customer_CityId(i_CustomerId CC_Customers.CustomerId%TYPE, i_NewCityId CC_Customers.CityId%TYPE)
    AS
        doesCustomerExist NUMBER;
        doesCityExist NUMBER;
    BEGIN
        doesCustomerExist := CC_Package.Does_CustomerId_Exists(i_CustomerId);
        IF (doesCustomerExist != 1) THEN
            RAISE CC_Package.CustomerId_Not_Exists;
        END IF;

        doesCityExist := CC_Package.Does_CityId_Exists(i_NewCityId);
        IF (doesCityExist != 1) THEN
            RAISE CC_Package.CityId_Not_Exists;
        END IF;

        UPDATE CC_Customers 
        SET CityId = i_NewCityId
        WHERE CustomerId = i_CustomerId;
    END;

    PROCEDURE Update_Warehouse_Address(i_WarehouseId CC_Warehouses.WarehouseId%TYPE, i_NewAddress CC_Customers.Address%TYPE)
    AS
        doesWarehouseExist NUMBER;
    BEGIN
        doesWarehouseExist := CC_Package.Does_WarehouseId_Exists(i_WarehouseId);
        IF (doesWarehouseExist != 1) THEN
            RAISE CC_Package.WarehouseId_Not_Exists;
        END IF;

        UPDATE CC_Warehouses 
        SET Address = i_NewAddress
        WHERE WarehouseId = i_WarehouseId;
    END;


    PROCEDURE Update_Warehouse_CityId(i_WarehouseId CC_Warehouses.WarehouseId%TYPE, i_NewCityId CC_Customers.CityId%TYPE)
    AS
        doesWarehouseExist NUMBER;
        doesCityExist NUMBER;
    BEGIN
        doesWarehouseExist := CC_Package.Does_WarehouseId_Exists(i_WarehouseId);
        IF (doesWarehouseExist != 1) THEN
            RAISE CC_Package.WarehouseId_Not_Exists;
        END IF;

        doesCityExist := CC_Package.Does_CityId_Exists(i_NewCityId);
        IF (doesCityExist != 1) THEN
            RAISE CC_Package.CityId_Not_Exists;
        END IF;
       
        UPDATE CC_Warehouses 
        SET CityId = i_NewCityId
        WHERE WarehouseId = i_WarehouseId;
    END;

END CC_Package_Adv;
/
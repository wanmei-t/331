package dawson;
import java.sql.*;
import java.util.Map;

/**
 * @author Felicia Luu
 */
public class Customer implements SQLData {
    public final String TYPENAME = "CC_CUSTOMER_TYPE";
    private int customerId;
    private String firstName;
    private String lastName;
    private String email;
    private String address;
    private int cityId;

    /**
     * Default constructor
     */
    public Customer(){
        
    }

     /**
     * Constructor when creating a Customer to add to the database where customerId is nonexistent
     *
     * @param firstName The first name of the customer.
     * @param lastName  The last name of the customer.
     * @param email     The email of the customer.
     * @param address   The address of the customer.
     * @param cityId    The identifier of the city associated with the customer.
     */
    public Customer(String firstName, String lastName, String email, String address, int cityId){
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.cityId = cityId;
    }

    /**
     * Normal constructor
     *
     * @param customerId The unique identifier for the customer.
     * @param firstName  The first name of the customer.
     * @param lastName   The last name of the customer.
     * @param email      The email of the customer.
     * @param address    The address of the customer.
     * @param cityId     The identifier of the city associated with the customer.
     */
    public Customer(int customerId, String firstName, String lastName, String email, String address, int cityId){
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.cityId = cityId;
    }

    
    /**
     * Adds the customer to the database using a stored procedure.
     *
     * @param conn The database connection.
     * @throws SQLException If a database access error occurs.
     * @throws ClassNotFoundException If the class definition for the customer type is not found.
     */
    public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException{
        Map<String, Class<?>> map = conn.getTypeMap();
        map.put(this.TYPENAME, Class.forName("dawson.Customer"));
        conn.setTypeMap(map);

        Customer newCustomer = new Customer(this.firstName, this.lastName, this.email, this.address, this.cityId);
        String sql = "{call CC_Package_Adv.Add_Customer_Type(?, ?)}";
        CallableStatement stmt = conn.prepareCall(sql);
        stmt.registerOutParameter(1, Types.INTEGER);
        stmt.setObject(2, newCustomer);        
        stmt.execute();
        int newCustomerId = stmt.getInt(1);
        this.setCustomerId(newCustomerId);
    }


    /**
     * @return The unique identifier for the customer.
     */
    public int getCustomerId(){
        return this.customerId;
    }
    
    /**
     * @return The first name of the customer.
     */
    public String getFirstName(){
        return this.firstName;
    }

    /**
     * @return The last name of the customer.
     */
    public String getLastName(){
        return this.lastName;
    }

    /**
     * @return The email of the customer.
     */
    public String getEmail(){
        return this.email;
    }

    /**
     * @return The address of the customer.
     */
    public String getAddress(){
        return this.address;
    }

    /**
     * @return The identifier of the city associated with the customer.
     */
    public int getCityId(){
        return this.cityId;
    }


    /**
     * @param customerId The unique identifier for the customer.
     */
    public void setCustomerId(int customerId){
        this.customerId = customerId;
    }
    
     /**
     * @param firstName The first name of the customer.
     */
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }

    /**
     * @param lastName The last name of the customer.
     */
    public void setLastName(String lastName){
        this.lastName = lastName;
    }

    /**
     * @param email The email of the customer.
     */
    public void setEmail(String email){
        this.email = email;
    }

    /**
     * @param address The address of the customer.
     */
    public void setAddress(String address){
        this.address = address;
    }

    /**
     * @param cityId The identifier of the city associated with the customer.
     */
    public void setCityId(int cityId){
        this.cityId = cityId;
    }

    /**
     * @return The SQL type name.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public String getSQLTypeName() throws SQLException{
        return this.TYPENAME;
    }

    /**
     * @param stream The SQL input stream.
     * @param typeName The SQL type name.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setCustomerId(stream.readInt());
        setFirstName(stream.readString());
        setLastName(stream.readString());
        setEmail(stream.readString());
        setAddress(stream.readString());
        setCityId(stream.readInt());
    }

    /**
     * @param stream The SQL output stream.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        //Note order here matters, it must match the type
        stream.writeInt(getCustomerId());
        stream.writeString(getFirstName());
        stream.writeString(getLastName());
        stream.writeString(getEmail());
        stream.writeString(getAddress());
        stream.writeInt(getCityId());
    }

    /**
     * @return A string representation of the customer.
     */
    @Override
    public String toString(){
        return "[CUSTOMER] Customer ID: " + this.customerId + " | First name: " + this.firstName + " | Last name: " + this.lastName + " | Email: " + this.email + " | Address: " + this.address + " | City ID: " + this.cityId;
    }
}

package dawson;
import java.sql.*;
import java.util.Map;

/**
 * @author Felicia Luu
 */
public class Review implements SQLData{
    public final String TYPENAME = "CC_REVIEW_TYPE";
    private int reviewId;
    private int productId;
    private int customerId;
    private int score;
    private String description;
    private int numFlagged; 

    /**
     * Default constructor
     */
    public Review(){
        
    }

    /**
     * Constructor when adding a review to the database where reviewId is inexistant
     * 
     * @param productId    The identifier of the product associated with the review.
     * @param customerId   The identifier of the customer who submitted the review.
     * @param score        The score given in the review.
     * @param description  The description or content of the review.
     * @param numFlagged   The number of times the review has been flagged.
     */
    public Review(int productId, int customerId, int score, String description, int numFlagged){
        this.productId = productId;
        this.customerId = customerId;
        this.score = score;
        this.description = description;
        this.numFlagged = numFlagged;
    }

    /**
     * Normal constructor
     *
     * @param reviewId     The unique identifier for the review.
     * @param productId    The identifier of the product associated with the review.
     * @param customerId   The identifier of the customer who submitted the review.
     * @param score        The score given in the review.
     * @param description  The description or content of the review.
     * @param numFlagged   The number of times the review has been flagged.
     */
    public Review(int reviewId, int productId, int customerId, int score, String description, int numFlagged){
        this.reviewId = reviewId;
        this.productId = productId;
        this.customerId = customerId;
        this.score = score;
        this.description = description;
        this.numFlagged = numFlagged;
    }


    /**
     * Adds the review to the database using a stored procedure.
     *
     * @param conn The database connection.
     * @throws SQLException If a database access error occurs.
     * @throws ClassNotFoundException If the class definition for the review type is not found.
     */
    public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException{
        Map<String, Class<?>> map = conn.getTypeMap();
        map.put(this.TYPENAME, Class.forName("dawson.Review"));
        conn.setTypeMap(map);

        Review newReview = new Review(this.reviewId, this.productId, this.customerId, this.score, this.description, this.numFlagged);
        String sql = "{call CC_Package_Adv.Add_Review_Type(?, ?)}";
        CallableStatement stmt = conn.prepareCall(sql);
        stmt.registerOutParameter(1, Types.INTEGER);
        stmt.setObject(2, newReview);        
        stmt.execute();
        int newReviewId = stmt.getInt(1);
        this.setReviewId(newReviewId);
    }


    /**
     * @return The unique identifier for the review.
     */
    public int getReviewId(){
        return this.reviewId;
    }
    
    /**
     * @return The identifier of the product associated with the review.
     */

    public int getProductId(){
        return this.productId;
    }

    /**
     * @return The identifier of the customer who submitted the review.
     */
    public int getCustomerId(){
        return this.customerId;
    }

    /**
     * @return The score given in the review.
     */
    public int getScore(){
        return this.score;
    }

    /**
     * @return The description or content of the review.
     */
    public String getDescription(){
        return this.description;
    }

    /**
     * @return The number of times the review has been flagged.
     */
    public int getNumFlagged(){
        return this.numFlagged;
    }


    /**
     * @param reviewId The unique identifier for the review.
     */
    public void setReviewId(int reviewId){
        this.reviewId = reviewId;
    }
    
    /**
     * @param productId The identifier of the product associated with the review.
     */
    public void setProductId(int productId){
        this.productId = productId;
    }

    /**
     * @param customerId The identifier of the customer who submitted the review.
     */
    public void setCustomerId(int customerId){
        this.customerId = customerId;
    }

    /**
     * @param score The score given in the review.
     */
    public void setScore(int score){
        this.score = score;
    }

    /**
     * @param description The description or content of the review.
     */
    public void setDescription(String description){
        this.description = description;
    }

    /**
     * @param numFlagged The number of times the review has been flagged.
     */
    public void setNumFlagged(int numFlagged){
        this.numFlagged = numFlagged;
    }


    /**
     * @param stream The SQL output stream.
     * @throws SQLException If a database access error occurs.
     */
    /**
     * @return The SQL type name.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public String getSQLTypeName() throws SQLException{
        return this.TYPENAME;
    }

    /**
     * @param stream The SQL input stream.
     * @param typeName The SQL type name.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setReviewId(stream.readInt());
        setProductId(stream.readInt());
        setCustomerId(stream.readInt());
        setScore(stream.readInt());
        setDescription(stream.readString());
        setNumFlagged(stream.readInt());
    }

    /**
     * @param stream The SQL output stream.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        //Note order here matters, it must match the type
        stream.writeInt(getReviewId());
        stream.writeInt(getProductId());
        stream.writeInt(getCustomerId());
        stream.writeInt(getScore());
        stream.writeString(getDescription());
        stream.writeInt(getNumFlagged());
    }

    /**
     * @return A string representation of the review.
     */
    @Override
    public String toString(){
        return "[REVIEW] Review ID: " + this.reviewId + " | Product ID: " + this.productId + " | Customer ID: " + this.customerId + " | Score: " +  this.score + " | Description: " + this.description + " | Number of flags: " + this.numFlagged;
    }
}

 

package dawson;
import java.sql.*;

/**
 * @author Wan Mei Tao
 * @author Felicia Luu
 */

public class CommerceCenter{
    public static void main(String[] args)
    {
        try
        {
            try
            {
                System.out.println("Welcome to Commerce Center!");
                Connection conn = getConnection();

                while (true)
                {
                    displayOptions(conn);
                    int answer = readInt("\nDo you want to make another action? Answer 1 for yes or -1 for no: ");
                    if (answer == -1) break;
                }

                closeConnection(conn);
                System.out.println("\nGoodbye!");
            }
            catch (SQLException e)
            {
                System.out.println("[ERROR] Failed to close connection. Please contact admin about this bug.");
            }
            catch (IllegalArgumentException e)
            {
                System.out.println("[ERROR] You do not have access to this program. Contact admin for more details.");
            }
        }
        catch (Exception e)
        {
            System.out.println("[ERROR] There seems to be an unknow issue. Please contact admin about this bug.");
        }
    }

/**
 * Displays the main options related to different categories and calls corresponding category-specific methods.
 * @param conn The Connection object to the database.
 */
    public static void displayOptions(Connection conn)
    {
        System.out.println("\nWhat is the action you wish to make related to?");
        System.out.println("1. Customer");
        System.out.println("2. Order");
        System.out.println("3. Product");
        System.out.println("4. Review");
        System.out.println("5. Store");
        System.out.println("6. Warehouse");
        int answer = readInt("Answer with an integer representing the category that fits best: ");

        switch(answer) 
        {    
            case 1:    
                displayCustomerOptions(conn);   
                break;   
            case 2:
                displayOrderOptions(conn);   
                break;
            case 3:
                displayProductOptions(conn);   
                break;
            case 4:
                displayReviewOptions(conn);   
                break;
            case 5:
                displayStoreOptions(conn);   
                break;
            case 6:
                displayWarehouseOptions(conn);   
                break;  
            default:
                System.out.println("That is not an option");
        }
    }

/**
 * Displays options related to customers and calls corresponding customer-specific methods.
 * @param conn The Connection object to the database.
 */ 
    public static void displayCustomerOptions(Connection conn)
    {
        System.out.println("\nWhat is the action you wish to make?");
        System.out.println("1. Add a customer");
        System.out.println("2. View Customer audit table");
        System.out.println("3. Get all customers with dangerous reviews");
        System.out.println("4. View all customers");
        System.out.println("5. View all the orders placed by a specific customer");
        System.out.println("6. Change email");
        System.out.println("7. Change customer address");
        int answer = readInt("Answer with an integer representing the action you wish to make: ");

        switch(answer) 
        {    
            case 1:    
                addCustomer(conn);  
                break; 
            case 2: 
                displayAudit(conn, "Customers");
                break; 
            case 3: 
                viewCustomersWithDangerousReviews(conn);
                break; 
            case 4: 
                viewAllCustomers(conn);
                break; 
            case 5: 
                viewAllOrdersOfCustomer(conn);
                break;
            case 6:
                changeEmail(conn);
                break;
            case 7: 
                changeAddress(conn, "customer");
                break;
            default:
                System.out.println("That is not an option"); 
        }
    }


/**
 * Displays options related to customers and calls corresponding customer-specific methods.
 * @param conn The Connection object to the database.
 */ 
    public static void displayOrderOptions(Connection conn)
    {
        System.out.println("\nWhat is the action you wish to make?");
        System.out.println("1. Add an order");
        System.out.println("2. Delete an order");
        System.out.println("3. View all the orders placed at a specific store");
        System.out.println("4. View all the orders placed by a specific customer");
        System.out.println("5. View the amount of times a product has been purchased");
        int answer = readInt("Answer with an integer representing the action you wish to make: ");

        switch(answer) 
        {    
            case 1:    
                addOrder(conn);
                break;
            case 2:    
                deleteOrder(conn);
                break;
            case 3:
                viewAllOrdersOfStore(conn);
                break;
            case 4:
                viewAllOrdersOfCustomer(conn);
                break;
            case 5:
                viewPurchaseQuantityOfProduct(conn);
                break;
            default:
                System.out.println("That is not an option");    
        }
    }

/**
 * Displays options related to products and calls corresponding product-specific methods.
 * @param conn The Connection object to the database.
 */
    public static void displayProductOptions(Connection conn)
    {
        System.out.println("\nWhat is the action you wish to make?");
        System.out.println("1. Add a product");
        System.out.println("2. Delete a product");
        System.out.println("3. Get the quantity of a product");   
        System.out.println("4. Get all the reviews of a product");  
        System.out.println("5. Modify the unit price of a product"); 
        System.out.println("6. View Product audit table");
        System.out.println("7. View all the products");
        System.out.println("8. View the average score of a product");
        System.out.println("9. View all the products of a specific category");
        System.out.println("10. View the amount of times a product has been purchased");
        int answer = readInt("Answer with an integer representing the action you wish to make: ");

        switch(answer) 
        {    
            case 1:    
                addProduct(conn);
                break;  
            case 2:
                deleteProduct(conn);
                break;
            case 3:    
                viewProductQuantity(conn);
                break;
            case 4:    
                viewReviewsOfAProduct(conn);
                break;
            case 5:    
                changeProductUnitPrice(conn);
                break;   
            case 6:    
                displayAudit(conn, "Products");
                break;  
            case 7:
                viewAllProducts(conn);
                break;
            case 8:
                viewAverageScoreOfProduct(conn);
                break;
            case 9:
                viewAllProductsOfCategory(conn);
                break;
            case 10:
                viewPurchaseQuantityOfProduct(conn);
                break;
            default:
                System.out.println("That is not an option");   
        }
    }


/**
 * Displays options related to reviews and calls corresponding review-specific methods.
 * @param conn The Connection object to the database.
 */
    public static void displayReviewOptions(Connection conn)
    {
        System.out.println("\nWhat is the action you wish to make?");
        System.out.println("1. Add a review");
        System.out.println("2. Flag a review");
        System.out.println("3. Delete a review");
        System.out.println("4. Get all dangerous reviews");
        System.out.println("5. Get all customers with dangerous reviews");
        System.out.println("6. Get all the reviews of a product");
        System.out.println("7. View Review audit table");
        System.out.println("8. View the average score of a product");
        System.out.println("9. Modify a review (score and description only)");
        System.out.println("10. Modify a review (number of flags only)");
        int answer = readInt("Answer with an integer representing the action you wish to make: ");

        switch(answer) 
        {    
            case 1:    
                addReview(conn);
                break;
            case 2:
                flagReview(conn);
                break;
            case 3:
                deleteReview(conn);
                break;
            case 4:
                getAllDangerousReviews(conn);
                break;
            case 5:
                viewCustomersWithDangerousReviews(conn);
                break;
            case 6:    
                viewReviewsOfAProduct(conn);
                break;  
            case 7:
                displayAudit(conn, "Reviews");
                break;
            case 8:
                viewAverageScoreOfProduct(conn);
                break;
            case 9:
                modifyReview(conn);
                break;
            case 10:
                modifyNumFlags(conn);
                break;
            default:
                System.out.println("That is not an option");  
        }
    }

/**
 * Displays options related to stores and calls corresponding store-specific methods.
 * @param conn The Connection object to the database.
 */
    public static void displayStoreOptions(Connection conn)
    {
        System.out.println("\nWhat is the action you wish to make?");
        System.out.println("1. Add a store");
        System.out.println("2. View all the stores");
        int answer = readInt("Answer with an integer representing the action you wish to make: ");

        switch(answer) 
        {    
            case 1:    
                addStore(conn);
                break;
            case 2:    
                viewAllStores(conn);
                break;
            default:
                System.out.println("That is not an option");     
        }
    }

/**
 * Displays options related to warehouses and calls corresponding warehouse-specific methods.
 * @param conn The Connection object to the database.
 */
    public static void displayWarehouseOptions(Connection conn)
    {
        System.out.println("\nWhat is the action you wish to make?");
        System.out.println("1. Add a warehouse");
        System.out.println("2. Delete a warehouse");
        System.out.println("3. Add product quantity to a warehouse");
        System.out.println("4. View Warehouse audit table");
        System.out.println("5. View Warehouse and Product audit table");
        System.out.println("6. View all the warehouses");
        System.out.println("7. Change warehouse address");
        int answer = readInt("Answer with an integer representing the action you wish to make: ");

        switch(answer) 
        {    
            case 1:    
                addWarehouse(conn);
                break;
            case 2:
                deleteWarehouse(conn);
                break;
            case 3:
                addQuantityToWarehouse(conn);
                break;
            case 4:
                displayAudit(conn, "Warehouses");
                break;
            case 5:
                displayAudit(conn, "Warehouses_Products");
                break;
            case 6:
                viewAllWarehouses(conn);
                break;
            case 7:
                changeAddress(conn, "warehouse");
                break;
            default:
                System.out.println("That is not an option");   
        }
    }

/**
 * Adds a new customer to the database.
 * @param conn The Connection object to the database.
 */
    public static void addCustomer(Connection conn)
    {
        try
        {
            String firstName = readString("\nFirst name of the customer: ");
            String lastName = readString("Last name of the customer: ");
            String email = readString("Email of the customer: ");
            String address = readString("Address of the customer: ");
            int cityId = askCityId(conn);
            Customer c = new Customer(firstName, lastName, email, address, cityId);
            c.addToDatabase(conn);
            System.out.println("[STATUS] Successfully added a customer, here are the details:");
            System.out.println(c);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the customer doesn't exist already and the city ID is valid.");
        }
        catch (ClassNotFoundException e)
        {
            System.out.println("[ERROR] Please report to admin about this bug.");
        }
    }

/**
 * Adds a new order to the database.
 * @param conn The Connection object to the database.
 */
    public static void addOrder(Connection conn)
    {
        try
        {
            int customerId = readInt("\nCustomer ID of the order (integer only): ");
            int storeId = readInt("Store ID of the order (integer only): ");
            int productId = readInt("Product ID of the order (integer only): ");
            int quantity = readInt("Quantity of the order (integer only): ");
            double price = readDouble("Price of the order (double only): ");
            Date orderDate = Date.valueOf(readString("Date of the order (YYYY-MM-DD format only): "));
            Order o = new Order(customerId, storeId, productId, quantity, price, orderDate);
            o.addToDatabase(conn);
            System.out.println("[STATUS] Successfully added an order, here are the details:");
            System.out.println(o);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the order doesn't exist already, the customer ID, product ID, store ID are valid, the quantity and price are more than 0, and the date is in the correct format.");
        }
        catch (ClassNotFoundException e)
        {
            System.out.println("[ERROR] Please report to admin about this bug.");
        }
    }

/**
 * Adds a new product to the database.
 * @param conn The Connection object to the database.
 */
    public static void addProduct(Connection conn)
    {
        try
        {
            String name = readString("\nName of the product: ");
            String category = readString("Category of the product: ");
            double unitPrice = readDouble("Unit price of the product (double only): ");
            Product p = new Product(name, category, unitPrice);
            p.addToDatabase(conn);
            System.out.println("[STATUS] Successfully added a product, here are the details:");
            System.out.println(p);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the product doesn't exist already and the unit price is more than 0.");
        }
        catch (ClassNotFoundException e)
        {
            System.out.println("[ERROR] Please report to admin about this bug.");
        }
    }

/**
 * Adds a new review to the database.
 * @param conn The Connection object to the database.
 */
    public static void addReview(Connection conn)
    {
        try
        {
            int productId = readInt("\nProduct ID of the review (integer only): ");
            int customerId = readInt("Customer ID of the review (integer only): ");
            int score = readInt("Score from 1 to 5 of the review (integer only): ");
            String description = readLine("Description of the review: ");
            int numFlags = readInt("Number of flags of the review (integer only): ");
            Review r = new Review(productId, customerId, score, description, numFlags);
            r.addToDatabase(conn);
            System.out.println("[STATUS] Successfully added a review, here are the details:");
            System.out.println(r);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the review doesn't exist already, the product ID and customer Id are valid, the score is between 1 to 5, and the number of flags is at least 0.");
        }
        catch (ClassNotFoundException e)
        {
            System.out.println("[ERROR] Please report to admin about this bug.");
        }
    }

/**
 * Adds a new store to the database.
 * @param conn The Connection object to the database.
 */
    public static void addStore(Connection conn)
    {
        try
        {
            String name = readString("\nName of the store: ");
            Store s = new Store(name);
            s.addToDatabase(conn);
            System.out.println("[STATUS] Successfully added a store, here are the details:");
            System.out.println(s);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the store doesn't exist already.");
        }
        catch (ClassNotFoundException e)
        {
            System.out.println("[ERROR] Please report to admin about this bug.");
        }
    }

/**
 * Adds a new warehouse to the database.
 * @param conn The Connection object to the database.
 */
    public static void addWarehouse(Connection conn)
    {
        try
        {
            String name = readString("\nName of the warehouse: ");
            String address = readString("Address of the warehouse: ");
            int cityId = askCityId(conn);
            Warehouse w = new Warehouse(name, address, cityId);
            w.addToDatabase(conn);
            System.out.println("[STATUS] Successfully added a warehouse, here are the details:");
            System.out.println(w);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the warehouse doesn't exist already and the city ID is valid.");
        }
        catch (ClassNotFoundException e)
        {
            System.out.println("[ERROR] Please report to admin about this bug.");
        }
    }


//----------------- SECTION FOR CUSTOMER-RELATED METHODS -----------------------


/**
 * Displays customers with dangerous reviews from the database.
 * @param conn The Connection object to the database.
 */
    public static void viewCustomersWithDangerousReviews(Connection conn) {
        System.out.println();
        fetchAndDisplayDataFromDatabase(conn, "CC_Package_Adv", "Get_Customers_With_Dangerous_Reviews", "Couldn't load information from the relevant tables");
    }

/**
 * Views all customers from the database.
 * @param conn The Connection object to the database.
 */
    public static void viewAllCustomers(Connection conn)
    {
        System.out.println();
        fetchAndDisplayDataFromDatabase(conn, "CC_Package_Adv", "Get_All_Customers", "Couldn't load information from the Customers table");
    }

/**
 * Views all orders placed by a specific customer from the database.
 * @param conn The Connection object to the database.
 */
    public static void viewAllOrdersOfCustomer(Connection conn) 
    {
        try
        {
            int customerId = readInt("\nOrders of which customer ID (integer only): ");
            String sql = "{ ? = call CC_Package_Adv.Get_All_Orders_For_CustomerId(?) }";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1, Types.VARCHAR);
            stmt.setInt(2, customerId);
            stmt.execute();
            String products = stmt.getString(1);
            printDataFromDatabase(products);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the customer ID is valid.");
        }
    }

/**
 * Updates the email of a customer in the database.
 * @param conn The Connection object to the database.
 */
    public static void changeEmail(Connection conn){
        try{
            int customerId = readInt("Customer ID: ");
            String newEmail = readString("Customer's new email: ");
            String sql = "{call CC_Package_Adv.Update_Email(?, ?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, customerId);
            stmt.setString(2, newEmail);
            stmt.execute();
            System.out.println("[STATUS] Successfully updated the email to: " + newEmail);
        }
        catch (SQLException e){
            System.out.println("[ERROR] An error occured while attempting to change the customer email.");
        }
    }


//------------------- SECTION FOR ORDER-RELATED METHODS -----------------------

/**
 * Deletes an order from the database.
 * @param conn The Connection object to the database.
 */
    public static void deleteOrder(Connection conn)
    {
        try
        {
            int orderId = readInt("\nOrder ID to be deleted (integer only): ");
            String sql = "{call CC_Package_Adv.Delete_Order(?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, orderId);
            stmt.execute();
            System.out.println("[STATUS] Successfully deleted order " + orderId);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the order ID is valid");
        }
    }

/**
 * Views all orders placed at a specific store from the database.
 * @param conn The Connection object to the database.
 */
    public static void viewAllOrdersOfStore(Connection conn) 
    {
        try
        {
            int storeId = readInt("\nOrders of which store ID (integer only): ");
            String sql = "{ ? = call CC_Package_Adv.Get_All_Orders_For_StoreId(?) }";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1, Types.VARCHAR);
            stmt.setInt(2, storeId);
            stmt.execute();
            String products = stmt.getString(1);
            printDataFromDatabase(products);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the store ID is valid.");
        }
    }

/**
 * @param conn connection to the database
 */
    public static void viewPurchaseQuantityOfProduct(Connection conn)
    {
        try
        {
            int productId = readInt("\nProduct ID of the product (integer only): ");
            String sql = "{ ? = call CC_Package_Adv.Get_QuantityOrder_Of_ProductId(?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1, Types.INTEGER);
            stmt.setInt(2, productId);
            stmt.execute();
            int quantity = stmt.getInt(1);
            System.out.println("[RESULT] The product " + productId + " has been purchase " + quantity + " times.");
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the product ID is valid.");
        }
    }


//------------------- SECTION FOR PRODUCT-RELATED METHODS ----------------------

/**
 * Deletes a product from the database.
 * @param conn The Connection object to the database.
 */
    public static void deleteProduct(Connection conn) {
        try
        {
            int productId = readInt("\nProduct ID to be deleted (integer only): ");
            String sql = "{call CC_Package_Adv.Delete_Product(?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, productId);
            stmt.execute();
            System.out.println("[STATUS] Successfully deleted product " + productId);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the product ID is valid");
        }
    }

/**
 * Views the quantity of a product in the database.
 * @param conn The Connection object to the database.
 */
    public static void viewProductQuantity(Connection conn)
    {
        try
        {
            int productId = readInt("\nProduct ID of the product (integer only): ");
            String sql = "{ ? = call CC_Package.Get_Overall_ProductQuantity(?) }";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1, Types.INTEGER);
            stmt.setInt(2, productId);
            stmt.execute();
            int quantity = stmt.getInt(1);
            System.out.println("[RESULT] The quantity available for product ID " + productId + " is " + quantity + ".");
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the product ID is valid.");
        }
    }

/**
 * Views all reviews of a product from the database.
 * @param conn The Connection object to the database.
 */
    public static void viewReviewsOfAProduct(Connection conn)
    {
        try
        {
            int productId = readInt("\nProduct ID of the product (integer only): ");
            String sql = "{ ? = call CC_Package_Adv.Get_All_Reviews_For_ProductID(?) }";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1, Types.VARCHAR);
            stmt.setInt(2, productId);
            stmt.execute();
            String reviews = stmt.getString(1);
            printDataFromDatabase(reviews);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the product ID is valid.");
        }
}

/**
 * Changes the unit price of a product in the database.
 * @param conn The Connection object to the database.
 */
    public static void changeProductUnitPrice(Connection conn)
    {
        try
        {
            int productId = readInt("\nProduct ID of the product (integer only): ");
            double newUnitPrice = readDouble("New unit price for the product (double only): ");
            String sql = "{ call CC_Package_Adv.Change_Product_UnitPrice(?, ?) }";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, productId);
            stmt.setDouble(2, newUnitPrice);
            stmt.execute();
            System.out.println("[STATUS] The unit price has been successfully changed.");
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the product ID is valid and the new unit price is more than 0.");
        }
    }

/**
 * Views all products from the database.
 * @param conn The Connection object to the database.
 */
    public static void viewAllProducts(Connection conn)
    {
        System.out.println();
        fetchAndDisplayDataFromDatabase(conn, "CC_Package_Adv", "Get_All_Products", "Couldn't load information from the Products table");
    }

/**
 * Views the average score of a product from the database.
 * @param conn The Connection object to the database.
 */
    public static void viewAverageScoreOfProduct(Connection conn)
    {
        try
        {
            int productId = readInt("\nProduct ID of the product (integer only): ");
            String sql = "{ ? = call CC_Package_Adv.Get_AverageScore_For_ProductId(?) }";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1, Types.VARCHAR);
            stmt.setInt(2, productId);
            stmt.execute();
            String score = stmt.getString(1);
            printDataFromDatabase(score);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the product ID is valid.");
        }
    }

/**
 * Views all products of a specific category from the database.
 * @param conn The Connection object to the database.
 */
    public static void viewAllProductsOfCategory(Connection conn)
    {
        try
        {
            String category = readString("\nCategory of the products: ");
            String sql = "{ ? = call CC_Package_Adv.Get_All_Products_Of_Category(?) }";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1, Types.VARCHAR);
            stmt.setString(2, category);
            stmt.execute();
            String products = stmt.getString(1);
            printDataFromDatabase(products);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Couldn't load information from the Products table.");
        }
    }


//------------------- SECTION FOR REVIEW-RELATED METHODS ----------------------


/**
 * Flags a review in the database.
 * @param conn The Connection object to the database.
 */
    public static void flagReview(Connection conn){
        try{
            int reviewId = readInt("\nReview ID of the review to be flagged (integer only): ");
            String sql = "{call CC_Package_Adv.Flag_Review(?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, reviewId);
            stmt.execute();
            System.out.println("[STATUS] Successfully flagged review from review ID " + reviewId + ".");
        }
        catch (SQLException e){
            System.out.println("[ERROR] Make sure the review ID is valid.");
        }
    }

/**
 * Deletes a review from the database.
 * @param conn The Connection object to the database.
 */
    public static void deleteReview(Connection conn){
        try{
            int reviewId = readInt("\nReview ID of the review to be deleted (integer only): ");
            String sql = "{call CC_Package_Adv.Delete_Review(?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, reviewId);
            stmt.execute();
            System.out.println("[STATUS] Successfully deleted review from review ID " + reviewId + ".");
        }
        catch (SQLException e){
            System.out.println("[ERROR] Make sure the review ID is valid.");
        }
        
    }

/**
 * Retrieves and displays all reviews flagged as dangerous from the database.
 *
 * This method calls the corresponding stored procedure to fetch information about
 * reviews that have been flagged as dangerous and displays the retrieved data.
 *
 * @param conn The Connection object to the database.
 */
    public static void getAllDangerousReviews(Connection conn) {
        System.out.println();
        fetchAndDisplayDataFromDatabase(conn, "CC_Package_Adv", "Get_All_Dangerous_Reviews", "Couldn't load information from the Reviews tables");
    }

/**
 * Retrieves and displays all dangerous reviews from the database.
 * @param conn The Connection object to the database.
 */
    public static void modifyReview(Connection conn)
    {
        try
        {
            int reviewId = readInt("\nReview ID of the review to be modified (integer only): ");
            int score = readInt("New score of the review (integer only): ");
            String description = readLine("New description of the review: ");
            String sql = "{call CC_Package_Adv.Modify_Review(?, ?, ?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, reviewId);
            stmt.setInt(2, score);
            stmt.setString(3, description);
            stmt.execute();
            System.out.println("[STATUS] Successfully modified review " + reviewId + ".");
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the review ID is valid and the score is between 1 to 5.");
        }
    }


/**
 * Modifies the number of flags for a review in the database.
 *
 * This method calls the stored procedure "CC_Package_Adv.Modify_NumFlags"
 * with the provided review ID and the new number of flags, updating the
 * number of flags for the specified review in the database.
 *
 * @param conn The Connection object to the database.
 * @throws SQLException If a database access error occurs.
 */
    public static void modifyNumFlags(Connection conn)
    {
        try
        {
            int reviewId = readInt("\nReview ID of the review to be modified (integer only): ");
            int numflags = readInt("New number of flags for the review: ");
            String sql = "{call CC_Package_Adv.Modify_NumFlags(?, ?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, reviewId);
            stmt.setInt(2, numflags);
            stmt.execute();
            System.out.println("[STATUS] Successfully modifed the number of flags of review " + reviewId + ".");
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the review ID is valid and the number of flags is not negative.");
        }
    }


//------------------- SECTION FOR STORE-RELATED METHODS ----------------------


/**
 * Views all stores from the database.
 * @param conn The Connection object to the database.
 */
    public static void viewAllStores(Connection conn)
    {
        System.out.println();
        fetchAndDisplayDataFromDatabase(conn, "CC_Package_Adv", "Get_All_Stores", "Couldn't load information from the Stores table");
    }


//------------------- SECTION FOR WAREHOUSE-RELATED METHODS ----------------------


/**
 * Deletes a warehouse from the database.
 * @param conn The Connection object to the database.
 */
    public static void deleteWarehouse(Connection conn) {
        try
        {
            int warehouseId = readInt("\nWarehouse ID of the warehouse to be deleted (integer only): ");
            String sql = "{call CC_Package_Adv.Delete_Warehouse(?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, warehouseId);
            stmt.execute();
            System.out.println("[STATUS] Successfully deleted warehouse with ID: " + warehouseId);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the warehouse ID is valid.");
        }
    }

/**
 * Adds a quantity of a product to a warehouse in the database.
 * @param conn The Connection object to the database.
 */
    public static void addQuantityToWarehouse(Connection conn) {
        try
        {
            int warehouseId = readInt("\nWarehouse ID that is being restocked (integer only): ");
            int productId = readInt("Product ID of the restocked item (integer only): ");
            int quantity = readInt("Quantity to be added (integer only): ");
            String sql = "{call CC_Package_Adv.Add_Quantity_At_Warehouse(?, ?, ?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, warehouseId);
            stmt.setInt(2, productId);
            stmt.setInt(3, quantity);
            stmt.execute();
            System.out.println("[STATUS] Successfully added " + quantity + " of Product " + productId + " to Warehouse " + warehouseId);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the product ID and warehouse ID are valid and the quantity is more than 0.");
        }
    }

/**
 * Views all warehouses from the database.
 * @param conn The Connection object to the database.
 */
    public static void viewAllWarehouses(Connection conn)
    {
        System.out.println();
        fetchAndDisplayDataFromDatabase(conn, "CC_Package_Adv", "Get_All_Warehouses", "Couldn't load information from the Warehouses table");
    }



//--------- SECTION FOR GENERAL METHODS USED BY MORE THAN ONE CATEGORY ---------


/**
 * Helper method method that calls the functions that fetches the data from audit tables
 * @param conn The Connection object to the database.
 * @param category The category of the audit table.
 */
    public static void displayAudit(Connection conn, String category) {
        System.out.println();
        String subprogramName = "Display_" + category + "_Audit";
        fetchAndDisplayDataFromDatabase(conn, "CC_Package_Adv", subprogramName, "Couldn't load information from this audit table");
    }


/**
* Updates the address of either a customer or a warehouse in the database.
* @param conn The Connection object to the database.
* @param option The option specifying whether it's for a customer or a warehouse.
*/ 
    public static void changeAddress(Connection conn, String option){
        try{
            String newAddress;
            int cityIdChange;
            if (option.equals("customer")){
                int customerId = readInt("Customer ID: ");
                newAddress = readString("Customer's new address: ");
                cityIdChange = readInt("Did the city ID change? Answer 1 if yes, -1 for no: ");
                if(cityIdChange == 1){
                    changeCityId(conn, customerId, "customer");
                }
                updateCustomerAddressToDatabase(conn, customerId, newAddress);
            }

            else if (option.equals("warehouse")){
                int warehouseId = readInt("Warehouse ID: ");
                newAddress = readString("Warehouse's new address: ");
                cityIdChange = readInt("Did the city ID change? Answer 1 if yes, -1 for no: ");
                if(cityIdChange == 1){
                    changeCityId(conn, warehouseId, "warehouse");
                }
                updateWarehouseAddressToDatabase(conn, warehouseId, newAddress);
            }
            
        }
        catch (SQLException e){
            System.out.println("[ERROR] An error occured while attempting to change the address.");
        }
    }

/**
 * Updates the address of a customer in the database. This method calls the stored procedure 
 * "CC_Package_Adv.Update_Customer_Address" with the provided customer ID and new address, 
 * updating the customer's address in the database.
 *
 * @param conn The Connection object to the database.
 * @param customerId The ID of the customer whose address is to be updated.
 * @param newAddress The new address to be set for the customer.
 * @throws SQLException If a database access error occurs.
 */

    public static void updateCustomerAddressToDatabase(Connection conn, int customerId, String newAddress) throws SQLException{
        String sql = "{call CC_Package_Adv.Update_Customer_Address(?, ?)}";
        CallableStatement stmt = conn.prepareCall(sql);
        stmt.setInt(1, customerId);
        stmt.setString(2, newAddress);
        stmt.execute();
        System.out.println("[STATUS] Successfully updated the customer's address to: " + newAddress);
    }

/**
 * Updates the address of a warehouse in the database. This method calls the stored 
 * procedure "CC_Package_Adv.Update_Warehouse_Address" with the provided warehouse ID
 * and new address, updating the warehouse's address in the database.
 *
 * @param conn The Connection object to the database.
 * @param warehouseId The ID of the warehouse whose address is to be updated.
 * @param newAddress The new address to be set for the warehouse.
 * @throws SQLException If a database access error occurs.
 */

    public static void updateWarehouseAddressToDatabase(Connection conn, int warehouseId, String newAddress) throws SQLException{
        String sql = "{call CC_Package_Adv.Update_Warehouse_Address(?, ?)}";
        CallableStatement stmt = conn.prepareCall(sql);
        stmt.setInt(1, warehouseId);
        stmt.setString(2, newAddress);
        stmt.execute();
        System.out.println("[STATUS] Successfully updated the warehouse's address to: " + newAddress);
    }

/**
* Updates the City ID for either a customer or a warehouse in the database.
* @param conn The Connection object to the database.
* @param id The ID of the customer or warehouse.
* @param option The option specifying whether it's for a customer or a warehouse.
* @throws SQLException If a database access error occurs.
*/
    public static void changeCityId(Connection conn, int id, String option) throws SQLException{

        int newCityId = readInt("New city ID: ");

        if (option.equals("customer")){
            String sql = "{call CC_Package_Adv.Update_Customer_CityId(?, ?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, id);
            stmt.setInt(2, newCityId);
            stmt.execute();
        }

        else if (option.equals("warehouse")){
            String sql = "{call CC_Package_Adv.Update_Warehouse_CityId(?, ?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, id);
            stmt.setInt(2, newCityId);
            stmt.execute();
        }

        
        System.out.println("[STATUS] Successfully updated the city ID to: " + newCityId);
    }

/**
 * Helper method to read a user input of type integer only.
 * @param message The message to display before reading the input.
 * @return The integer input.
 */
    public static int readInt(String message)
    {
        while (true)
        {
            try
            {
                int i = Integer.parseInt(readLine(message));
                return i;
            }
            catch (NumberFormatException e)
            {
                System.out.println("An integer only input is required.");
            }
        }
    }


/**
* Helper method to read a user input of type double only.
* @param message The message to display before reading the input.
* @return The double input.
*/
    public static double readDouble(String message)
    {
        while (true)
        {
            try
            {
                double d = Double.parseDouble(readLine(message));
                return d;
            }
            catch (NumberFormatException e)
            {
                System.out.println("A double only input is required.");
            }
        }
    }

/**
* Helper method to read any user input as long as it's not null.
* @param message The message to display before reading the input.
* @return The string input.
*/
    public static String readString(String message)
    {
        while (true)
        {
            try
            {
                String s = readLine(message);
                if (s.length() <= 0)
                {
                    throw new IllegalArgumentException("An input of a at least one character is required.");
                }
                return s;
            }
            catch (IllegalArgumentException e)
            {
                System.out.println(e.getMessage());
            }
        }
    }

/**
* Helper method to read any user input.
* @param message The message to display before reading the input.
* @return The user input.
*/
    public static String readLine(String message)
    {
        return System.console().readLine(message);
    } 

/**
 * Helper method that calls a subprogram that doesn't take any parameter from the database and display the information to the console
 * @param conn The Connection object to the database.
 * @param packageName The name of the database package.
 * @param subprogramName The name of the subprogram to call.
 * @param errorMessage The error message to display if an SQL exception occurs.
 */
    public static void fetchAndDisplayDataFromDatabase(Connection conn, String packageName, String subprogramName, String errorMessage)
    {
        try
        {
            String sql = "{ ? = call " + packageName + "." + subprogramName + " }";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1, Types.VARCHAR);
            stmt.execute();
            String data = stmt.getString(1);
            printDataFromDatabase(data);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] " + errorMessage);
        }
    }

/**
 * Helper method to print table like information from the database.
 * @param data The data to print.
 */
    public static void printDataFromDatabase(String data)
    {
        String[] rows = data.split(";");
        for (String row : rows)
        {
            System.out.println(row);
        }
    }


    /**
     * Asks for and returns the City ID based on user input or adds a new city to the database.
     * @param conn The Connection object to the database.
     * @return The City ID.
     * @throws SQLException If a database access error occurs.
     */
    public static int askCityId(Connection conn) throws SQLException {
        int knowCityAnswer = readInt("Do you know the City ID of the address above? Answer 1 for yes or -1 to view a list of the cities: ");
        if (knowCityAnswer == -1){
            viewAllCities(conn);
            int noCityAnswer = readInt("If you can't see your city, you can add it! Do you wish to do so? Answer 1 for yes or -1 to skip: ");
            if (noCityAnswer == 1){
                addCity(conn);
            }
        }
        int cityId = readInt("City ID of the address (integer only): ");
        return cityId;
    }

/**
 * Views all cities from the database.
 * @param conn The Connection object to the database.
 */
    public static void viewAllCities(Connection conn)
    {
        System.out.println();
        fetchAndDisplayDataFromDatabase(conn, "CC_Package_Adv", "Get_All_Cities", "Couldn't load information from the Cities table");
    }


    /**
 * Adds a new city to the database.
 * @param conn The Connection object to the database.
 * @throws SQLException If a database access error occurs.
 */
    public static void addCity(Connection conn) throws SQLException{
        String cityName = readString("Name of the new city: ");
        int knowProvinceAnswer = readInt("Do you know the Province ID that the new city belongs in? Answer 1 for yes or -1 to view a list of the provinces: ");
        if (knowProvinceAnswer == -1){
            viewAllProvinces(conn);
            int noProvinceAnswer = readInt("If you can't see your province, you can add it! Do you wish to do so? Answer 1 for yes or -1 to skip: ");
            if (noProvinceAnswer == 1){
                addProvince(conn);
            }
        }
        
        int newCityId = addCityToDatabase(conn, cityName);
        System.out.println("[STATUS] Successfully added city: " + cityName + " with ID: " + newCityId);
    }


/**
 * Views all provinces from the database.
 * @param conn The Connection object to the database.
 */
    public static void viewAllProvinces(Connection conn)
    {
        System.out.println();
        fetchAndDisplayDataFromDatabase(conn, "CC_Package_Adv", "Get_All_Provinces", "Couldn't load information from the Provinces table");
    }

/**
 * Adds a new province to the database.
 * @param conn The Connection object to the database.
 * @throws SQLException If a database access error occurs.
 */
    public static void addProvince(Connection conn) throws SQLException{
        String provinceName = readString("Name of the new province: ");
        int knowCountryAnswer = readInt("Do you know the Country ID that the new province belongs in? Answer 1 for yes or -1 to view a list of the countries: ");
        if (knowCountryAnswer == -1){
            viewAllCountries(conn);
            int noCountryAnswer = readInt("If you can't see your country, you can add it! Do you wish to do so? Answer 1 for yes or -1 to skip: ");
            if (noCountryAnswer == 1){
                addCountry(conn);
            }
        }
        int newProvinceId = addProvinceToDatabase(conn, provinceName);
        System.out.println("[STATUS] Successfully added province: " + provinceName + " with ID: " + newProvinceId);
    }

/**
 * Adds a city to the database and returns the City ID.
 * @param conn The Connection object to the database.
 * @param cityName The name of the new city.
 * @return The City ID.
 * @throws SQLException If a database access error occurs.
 */
    public static int addCityToDatabase(Connection conn, String cityName) throws SQLException{
        try
        {
            int provinceId = readInt("Province ID of the city (integer only): ");
            String sql = "{call CC_Package.Add_City(?, ?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setString(1, cityName);
            stmt.setInt(2, provinceId);
            stmt.execute();
            int cityId = getCityId(conn, cityName, provinceId);
            return cityId;
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the city doesn't exist already and the province ID is valid.");
            throw e;
        }
    }

/**
 * Views all countries from the database.
 * @param conn The Connection object to the database.
 */
    public static void viewAllCountries(Connection conn)
    {
        System.out.println();
        fetchAndDisplayDataFromDatabase(conn, "CC_Package_Adv", "Get_All_Countries", "Couldn't load information from the Countries table");
    }


/**
 * Adds a new country to the database.
 * @param conn The Connection object to the database.
 * @throws SQLException If a database access error occurs.
 */
    public static void addCountry(Connection conn) throws SQLException{
        try
        {
            String countryName = readString("Name of the new country: ");
            String sql = "{call CC_Package.Add_Country(?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setString(1, countryName);
            stmt.execute();
            int countryId = getCountryId(conn, countryName);
            System.out.println("[STATUS] Successfully added country: " + countryName + " with ID: " +  countryId);
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the country doesn't exist already.");
            throw e;
        }
    }

/**
 * Adds a province to the database and returns the Province ID.
 * @param conn The Connection object to the database.
 * @param provinceName The name of the new province.
 * @return The Province ID.
 * @throws SQLException If a database access error occurs.
 */
    public static int addProvinceToDatabase(Connection conn, String provinceName) throws SQLException{
        try
        {
            int countryId = readInt("Country ID of the province (integer only): ");
            String sql = "{call CC_Package.Add_Province(?, ?)}";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setString(1, provinceName);
            stmt.setInt(2, countryId);
            stmt.execute();
            int provinceId = getProvinceId(conn, provinceName, countryId);
            return provinceId;
        }
        catch (SQLException e)
        {
            System.out.println("[ERROR] Make sure the province doesn't exist already and the country ID is valid.");
            throw e;
        }
    }


/**
 * Retrieves and returns the City ID from the database.
 * @param conn The Connection object to the database.
 * @param cityName The name of the city.
 * @param provinceId The Province ID of the city.
 * @return The City ID.
 * @throws SQLException If a database access error occurs.
 */
    public static int getCityId(Connection conn, String cityName, int provinceId) throws SQLException{
        String sql = "{ ? = call CC_Package.Get_CityId(?, ?) }";
        CallableStatement stmt = conn.prepareCall(sql);
        stmt.registerOutParameter(1, Types.INTEGER);
        stmt.setString(2, cityName);
        stmt.setInt(3, provinceId);
        stmt.execute();
        int cityId = stmt.getInt(1);
        return cityId;
    }

/**
 * Retrieves and returns the Country ID from the database.
 * @param conn The Connection object to the database.
 * @param countryName The name of the country.
 * @return The Country ID.
 * @throws SQLException If a database access error occurs.
 */
    public static int getCountryId(Connection conn, String countryName) throws SQLException{
        String sql = "{ ? = call CC_Package.Get_CountryId(?) }";
        CallableStatement stmt = conn.prepareCall(sql);
        stmt.registerOutParameter(1, Types.INTEGER);
        stmt.setString(2, countryName);
        stmt.execute();
        int countryId = stmt.getInt(1);
        return countryId;
    }

    /**
 * Retrieves and returns the Province ID from the database.
 * @param conn The Connection object to the database.
 * @param provinceName The name of the province.
 * @param countryId The Country ID of the province.
 * @return The Province ID.
 * @throws SQLException If a database access error occurs.
 */
    public static int getProvinceId(Connection conn, String provinceName, int countryId) throws SQLException{
        String sql = "{ ? = call CC_Package.Get_ProvinceId(?, ?) }";
        CallableStatement stmt = conn.prepareCall(sql);
        stmt.registerOutParameter(1, Types.INTEGER);
        stmt.setString(2, provinceName);
        stmt.setInt(3, countryId);
        stmt.execute();
        int provinceId = stmt.getInt(1);
        return provinceId;
    }

    
/**
 * Establishes a connection to the database using user credentials.
 * @return The Connection object to the database.
 * @throws IllegalArgumentException If too many login attempts occur.
 */
    public static Connection getConnection()
    {
        Connection conn;
        int attempt = 0;
        while (attempt < 3)
        {
            try
            {
                System.out.println("\n[LOG IN]");
                String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
                String user = readLine("Username: ");
                char[] passwordLetters = System.console().readPassword("Password: ");
                String password = "";
                for (int i = 0; i < passwordLetters.length; i++)
                {
                    password += passwordLetters[i];
                }
                conn = DriverManager.getConnection(url, user, password);
                System.out.println("\n[SUCCESSFUL LOG IN]");
                return conn;
            }
            catch (SQLException e)
            {
                attempt++;
                System.out.println("Error creating a new connection. Please try logging in again.");
            }
        }
        
        throw new IllegalArgumentException("Too many login attempts");
    }

/**
* Closes the database connection if it's not already closed.
* @param conn The Connection object to the database.
* @throws SQLException If a database access error occurs.
*/
    public static void closeConnection(Connection conn) throws SQLException
    {
        if(!conn.isClosed())
        {
            conn.close();
        }
    }
}
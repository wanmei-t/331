package dawson;
import java.sql.*;
import java.util.Map;

/**
 * @author Felicia Luu
 */
public class Store implements SQLData {
    public final String TYPENAME = "CC_STORE_TYPE";
    private int storeId;
    private String name;

    /**
     * Default constructor
     */
    public Store()
    {
        
    }
    
    /**
     * Constructor when addint a store to the database where storeId is inexistant
     *
     * @param name The name of the store.
     */
    public Store(String name){
        this.name = name;
    }

    /**
     * Normal constructor
     *
     * @param storeId The unique identifier for the store.
     * @param name The name of the store.
     */
    public Store(int storeId, String name){
        this.storeId = storeId;
        this.name = name;
    }


    /**
     * Adds the store to the database using a stored procedure.
     *
     * @param conn The database connection.
     * @throws SQLException If a database access error occurs.
     * @throws ClassNotFoundException If the class definition for the store type is not found.
     */
    public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException{
        Map<String, Class<?>> map = conn.getTypeMap();
        map.put(this.TYPENAME, Class.forName("dawson.Store"));
        conn.setTypeMap(map);

        Store newStore = new Store(this.storeId, this.name);
        String sql = "{call CC_Package_Adv.Add_Store_Type(?, ?)}";
        CallableStatement stmt = conn.prepareCall(sql);
        stmt.registerOutParameter(1, Types.INTEGER);
        stmt.setObject(2, newStore);        
        stmt.execute();
        int newStoreId = stmt.getInt(1);
        this.setStoreId(newStoreId);
    }


    /**
     * @return The unique identifier for the store.
     */
    public int getStoreId(){
        return this.storeId;
    }
    
    /**
     * @return The name of the store.
     */
    public String getName(){
        return this.name;
    }
    

    /**
     * @param storeId The unique identifier for the store.
     */
    public void setStoreId(int storeId){
        this.storeId = storeId;
    }
    
    /**
     * @param name The name of the store.
     */
    public void setName(String name){
        this.name = name;
    }


    /**
     * @return The SQL type name.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public String getSQLTypeName() throws SQLException{
        return this.TYPENAME;
    }

    /**
     * @param stream The SQL input stream.
     * @param typeName The SQL type name.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setStoreId(stream.readInt());
        setName(stream.readString());
    }

    /**
     * @param stream The SQL output stream.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        //Note order here matters, it must match the type
        stream.writeInt(getStoreId());
        stream.writeString(getName());
    }

    /**
     * @return A string representation of the store.
     */
    @Override
    public String toString(){
        return "[STORE] Store ID: " + this.storeId + " | Name : " + this.name;
    }
}


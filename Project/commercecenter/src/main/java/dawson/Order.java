package dawson;
import java.sql.*;
import java.util.Map;

/**
 * @author Wan Mei Tao
 */
public class Order implements SQLData {
    private final String TYPENAME = "CC_ORDER_TYPE";
    private int orderId;
    private int customerId;
    private int storeId;
    private int productId;
    private int quantity;
    private double price;
    private Date orderDate;

    /**
     * Default constructor
     */
    public Order() {

    }

    /**
     * Constructor when creating a Order to add to the database where orderId is nonexistent
     *
     * @param customerId The identifier of the customer associated with the order.
     * @param storeId    The identifier of the store associated with the order.
     * @param productId  The identifier of the product associated with the order.
     * @param quantity   The quantity of the product in the order.
     * @param price      The price of the order.
     * @param orderDate  The date when the order was placed.
     */
    public Order(int customerId, int storeId, int productId, int quantity, double price, Date orderDate) {
        this.customerId = customerId;
        this.storeId = storeId;
        this.productId = productId;
        this.quantity = quantity;
        this.price = price;
        this.orderDate = orderDate;
    }

    /**
     * Normal constructor
     *
     * @param orderId    The unique identifier for the order.
     * @param customerId The identifier of the customer associated with the order.
     * @param storeId    The identifier of the store associated with the order.
     * @param productId  The identifier of the product associated with the order.
     * @param quantity   The quantity of the product in the order.
     * @param price      The price of the order.
     * @param orderDate  The date when the order was placed.
     */
    public Order(int orderId, int customerId, int storeId, int productId, int quantity, double price, Date orderDate) {
        this.orderId = orderId;
        this.customerId = customerId;
        this.storeId = storeId;
        this.productId = productId;
        this.quantity = quantity;
        this.price = price;
        this.orderDate = orderDate;
    }


    /**
     * Adds the order to the database using a stored procedure.
     *
     * @param conn The database connection.
     * @throws SQLException If a database access error occurs.
     * @throws ClassNotFoundException If the class definition for the order type is not found.
     */
    public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        Map<String, Class<?>> map = conn.getTypeMap();
        map.put(this.TYPENAME, Class.forName("dawson.Order"));
        conn.setTypeMap(map);

        Order newOrder = new Order(getCustomerId(), getStoreId(), getProductId(), getQuantity(), getPrice(), getOrderDate());
        String sql = "{call CC_Package_Adv.Add_Order_Type(?, ?)}";
        CallableStatement stmt = conn.prepareCall(sql);
        stmt.registerOutParameter(1, Types.INTEGER);
        stmt.setObject(2, newOrder);        
        stmt.execute();
        int newOrderId = stmt.getInt(1);
        this.setOrderId(newOrderId);
    }


    /**
     * @return The unique identifier for the order.
     */
    public int getOrderId() {
        return this.orderId;
    }

    /**
     * @return The identifier of the customer associated with the order.
     */
    public int getCustomerId() {
        return this.customerId;
    }

    /**
     * @return The identifier of the store associated with the order.
     */
    public int getStoreId() {
        return this.storeId;
    }

     /**
     * @return The identifier of the product associated with the order.
     */
    public int getProductId() {
        return this.productId;
    }

    /**
     * @return The quantity of the product in the order.
     */
    public int getQuantity() {
        return this.quantity;
    }

    /**
     * @return The quantity of the product in the order.
     */
    public double getPrice() {
        return this.price;
    }

    /**
     * @return The date when the order was placed.
     */
    public Date getOrderDate() {
        return this.orderDate;
    }


    /**
     * @param orderId The unique identifier for the order.
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     * @param customerId The identifier of the customer associated with the order.
     */
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    /**
     * @param storeId The identifier of the store associated with the order.
     */
    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    /**
     * @param productId The identifier of the product associated with the order.
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * @param quantity The quantity of the product in the order.
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @param price The price of the order.
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @param orderDate The date when the order was placed.
     */
    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * @param stream The SQL output stream.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(this.getOrderId());
        stream.writeInt(this.getCustomerId());
        stream.writeInt(this.getStoreId());
        stream.writeInt(this.getProductId());
        stream.writeInt(this.getQuantity());
        stream.writeDouble(this.getPrice());
        stream.writeDate(this.getOrderDate());
    }

    /**
     * @param stream The SQL input stream.
     * @param typeName The SQL type name.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.setOrderId(stream.readInt());
        this.setCustomerId(stream.readInt());
        this.setStoreId(stream.readInt());
        this.setProductId(stream.readInt());
        this.setQuantity(stream.readInt());
        this.setPrice(stream.readDouble());
        this.setOrderDate(stream.readDate());
    }

    /**
     * @return The SQL type name.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public String getSQLTypeName() throws SQLException {
        return this.TYPENAME;
    }

    /**
     * @return A string representation of the order.
     */
    @Override
    public String toString() {
        return "[ORDER] Order ID: " + this.orderId + " | Customer ID: " + this.customerId + " | Store ID: " + this.storeId + " | Product ID: " + this.productId + " | Quantity: " + this.quantity + " | Price: " + this.price + " | Order Date: " + this.orderDate;
    }
}

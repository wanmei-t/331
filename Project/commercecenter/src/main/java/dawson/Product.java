package dawson;
import java.sql.*;
import java.util.Map;

/**
 * @author Wan Mei Tao
 */
public class Product implements SQLData {
    private final String TYPENAME = "CC_PRODUCT_TYPE";
    private int productId;
    private String name;
    private String category;
    private double unitPrice;

    /**
     * Default constructor
     */
    public Product() {

    }

    /**
     * Constructor for adding a product to the database where the productId is nonexistent
     *
     * @param name      The name of the product.
     * @param category  The category of the product.
     * @param unitPrice The unit price of the product.
     */
    public Product(String name, String category, double unitPrice)
    {
        this.name = name;
        this.category = category;
        this.unitPrice = unitPrice;
    }

    /**
     * Normal constructor
     * 
     * @param productId The unique identifier for the product.
     * @param name      The name of the product.
     * @param category  The category of the product.
     * @param unitPrice The unit price of the product.
     */
    public Product(int productId, String name, String category, double unitPrice) {
        this.productId = productId;
        this.name = name;
        this.category = category;
        this.unitPrice = unitPrice;
    }
    

    /**
     * Adds the product to the database using a stored procedure.
     *
     * @param conn The database connection.
     * @throws SQLException If a database access error occurs.
     * @throws ClassNotFoundException If the class definition for the product type is not found.
     */
    public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        Map<String, Class<?>> map = conn.getTypeMap();
        map.put(this.TYPENAME, Class.forName("dawson.Product"));
        conn.setTypeMap(map);

        Product newProduct = new Product(getName(), getCategory(), getUnitPrice());
        String sql = "{call CC_Package_Adv.Add_Product_Type(?, ?)}";
        CallableStatement stmt = conn.prepareCall(sql);
        stmt.registerOutParameter(1, Types.INTEGER);
        stmt.setObject(2, newProduct);        
        stmt.execute();
        int newProductId = stmt.getInt(1);
        this.setProductId(newProductId);
    }


    /**
     * @return The unique identifier for the product.
     */
    public int getProductId() {
        return this.productId;
    }

    /**
     * @return The name of the product.
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return The category of the product.
     */
    public String getCategory() {
        return this.category;
    }

    /**
     * @return The unit price of the product.
     */
    public double getUnitPrice() {
        return this.unitPrice;
    }


    /**
     * @param productId The unique identifier for the product.
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * @param name The name of the product.
     */
    public void setName(String name) {
        this.name = name;
    }

     /**
     * @param category The category of the product.
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @param unitPrice The unit price of the product.
     */
    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }


    /**
     * @param stream The SQL output stream.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(this.getProductId());
        stream.writeString(this.getName());
        stream.writeString(this.getCategory());
        stream.writeDouble(this.getUnitPrice());
    }

    /**
     * @param stream The SQL input stream.
     * @param typeName The SQL type name.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.setProductId(stream.readInt());
        this.setName(stream.readString());
        this.setCategory(stream.readString());
        this.setUnitPrice(stream.readDouble());
    }

    /**
     * @return The SQL type name.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public String getSQLTypeName() throws SQLException {
        return this.TYPENAME;
    }

    /**
     * @return A string representation of the product.
     */
    @Override
    public String toString()
    {
        return "[PRODUCT] Product ID: " + this.productId + " | Name: " + this.name + " | Category: " + this.category + " | Unit Price: " + this.unitPrice;
    }
    
}

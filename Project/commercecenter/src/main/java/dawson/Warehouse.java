package dawson;
import java.sql.*;
import java.util.Map;

/**
 * @author Wan Mei Tao
 */
public class Warehouse implements SQLData {
    private final String TYPENAME = "CC_WAREHOUSE_TYPE"; 
    private int warehouseId;
    private String name;
    private String address;
    private int cityId;

    /**
     * Default constructor
     */
    public Warehouse() {

    }

    /**
     * Constructor when adding a warehouse to the database where warehouseId is inexistant
     *
     * @param name The name of the warehouse.
     * @param address The address of the warehouse.
     * @param cityId The identifier of the city where the warehouse is located.
     */
    public Warehouse(String name, String address, int cityId) {
        this.name = name;
        this.address = address;
        this.cityId = cityId;
    }

    /**
     * Normal constructor
     * 
     * @param warehouseId The unique identifier for the warehouse.
     * @param name The name of the warehouse.
     * @param address The address of the warehouse.
     * @param cityId The identifier of the city where the warehouse is located.
     */
    public Warehouse(int warehouseId, String name, String address, int cityId) {
        this.warehouseId = warehouseId;
        this.name = name;
        this.address = address;
        this.cityId = cityId;
    }


    /**
     * Adds the warehouse to the database using a stored procedure.
     *
     * @param conn The database connection.
     * @throws SQLException If a database access error occurs.
     * @throws ClassNotFoundException If the class definition for the warehouse type is not found.
     */
    public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        Map<String, Class<?>> map = conn.getTypeMap();
        map.put(this.TYPENAME, Class.forName("dawson.Warehouse"));
        conn.setTypeMap(map);

        Warehouse newWarehouse = new Warehouse(getName(), getAddress(), getCityId());
        String sql = "{call CC_Package_Adv.Add_Warehouse_Type(?, ?)}";
        CallableStatement stmt = conn.prepareCall(sql);
        stmt.registerOutParameter(1, Types.INTEGER);
        stmt.setObject(2, newWarehouse);        
        stmt.execute();
        int newWarehouseId = stmt.getInt(1);
        this.setWarehouseId(newWarehouseId);
    }


    /**
     * @return The unique identifier for the warehouse.
     */
    public int getWarehouseId() {
        return this.warehouseId;
    }

    /**
     * @return The name of the warehouse.
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return The address of the warehouse.
     */
    public String getAddress() {
        return this.address;
    }

    /**
     * @return The identifier of the city where the warehouse is located.
     */
    public int getCityId() {
        return this.cityId;
    }


    /**
     * @param warehouseId The unique identifier for the warehouse.
     */
    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    /**
     * @param name The name of the warehouse.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param address The address of the warehouse.
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @param cityId The identifier of the city where the warehouse is located.
     */
    public void setCityId(int cityId) {
        this.cityId = cityId;
    }


    /**
     * @param stream The SQL output stream.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(this.getWarehouseId());
        stream.writeString(this.getName());
        stream.writeString(this.getAddress());
        stream.writeInt(this.getCityId());
    }

    /**
     * @param stream The SQL input stream.
     * @param typeName The SQL type name.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.setWarehouseId(stream.readInt());
        this.setName(stream.readString());
        this.setAddress(stream.readString());
        this.setCityId(stream.readInt());
    }

    /**
     * @return The SQL type name.
     * @throws SQLException If a database access error occurs.
     */
    @Override
    public String getSQLTypeName() throws SQLException {
        return this.TYPENAME;
    }

    /**
     * @return A string representation of the warehouse.
     */
    @Override
    public String toString()
    {
        return "[WAREHOUSE] Warehouse ID: " + this.warehouseId + " | Name: " + this.name + " | Address: " + this.address + " | City ID: " + this.cityId;
    }
}

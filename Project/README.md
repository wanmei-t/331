# Commerce Center

Link to repository: https://gitlab.com/wanmei-t/331_commercecenter.git

###  Authors

- Felicia Luu (2237149)  
- Wan Mei Tao (2235016)

### Setup

In SQL Developper, run the following scripts from the scripts folder in the following order:

1. setup.sql
2. setupBasic.sql
3. setupAdvanced.sql
4. baseData.sql

In VS Code, do the following steps:

1. Open the folder commercecenter
2. Open the file commercecenter\src\main\java\dawson\CommerceCenter.java
3. Click Run and Debug or press F5

### Removing Commerce Center

In SQL Developper, run remove.sql from the scripts folder

### Rules

- A customer can only purchase one product per order. The quantity is up to the customer as long as there is enough quantity in the warehouses.
- A customer cannot make more than one purchase of the same product per day.
- A customer can only leave one review per product. Purchasing the product is not necessary to leave a review. 
- A review requires a score, but a description is optional.
- A review is considered dangerous when its number of flags is more than 1.
- A warehouse cannot change its name, but it can change location.
- A customer cannot change their first name and last name, but it can change its email and address.
- A product cannot change name or category, but its unit price is modifiable.
- Sometimes, it happens that a review has been accidentally flagged. Therefore, the number of flags a review have can be manually modified.
CREATE OR REPLACE PACKAGE calculator_pkg AS
    FUNCTION adding (num1 IN NUMBER, num2 IN NUMBER)
        RETURN NUMBER;
    FUNCTION substract (num1 IN NUMBER, num2 IN NUMBER, num3 OUT NUMBER)
        RETURN NUMBER;
    FUNCTION multiply (num1 IN NUMBER, num2 IN NUMBER)
        RETURN NUMBER;
    FUNCTION divide (num1 IN NUMBER, num2 IN NUMBER)
        RETURN NUMBER;
END calculator_pkg;
/
CREATE OR REPLACE PACKAGE BODY calculator_pkg AS
    FUNCTION adding (num1 IN NUMBER, num2 IN NUMBER)
        RETURN NUMBER IS
        BEGIN
            RETURN num1 + num2;
        END;
        
    FUNCTION substract (num1 IN NUMBER, num2 IN NUMBER, num3 OUT NUMBER)
        RETURN NUMBER IS
        BEGIN
            num3 := num1 - num2;
            RETURN num3;
        END;
        
    FUNCTION multiply (num1 IN NUMBER, num2 IN NUMBER)
        RETURN NUMBER IS
        BEGIN
            RETURN num1 * num2;
        END;
    
    FUNCTION divide (num1 IN NUMBER, num2 IN NUMBER)
        RETURN NUMBER IS
        BEGIN
            RETURN num1 / num2;
        EXCEPTION
            WHEN ZERO_DIVIDE THEN
                DBMS_OUTPUT.PUT_LINE('A division by zero is not okay.');
                RETURN -1;
        END;
END calculator_pkg;
/
DECLARE
    result NUMBER;
BEGIN
    DBMS_OUTPUT.PUT_LINE(calculator_pkg.adding(5, 10));
    result := calculator_pkg.substract(20, 10, result);
    DBMS_OUTPUT.PUT_LINE(result);
    DBMS_OUTPUT.PUT_LINE(calculator_pkg.multiply(5, 10));
    DBMS_OUTPUT.PUT_LINE(calculator_pkg.divide(5, 0));
    DBMS_OUTPUT.PUT_LINE(calculator_pkg.divide(20, 10));
END;
package com.oracle.database.jdbc;
import java.sql.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        String user = System.console().readLine("Username: ");
        String password = System.console().readLine("Password: ");

        try 
        {
            Connection conn = DriverManager.getConnection(url, user, password);
            PreparedStatement statement = conn.prepareStatement("select * from books");
            ResultSet result = statement.executeQuery();
            
            while (result.next())
            {
                System.out.println(result.getString("title"));
                System.out.println(result.getDouble("cost"));
            }

            if(!statement.isClosed())
            {
                statement.close();
            }

            if(!conn.isClosed())
            {
                conn.close();
            }
        }
        catch (SQLException e)
        {
            System.out.println("ERROR!");
        }
    }
}

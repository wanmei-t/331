CREATE OR REPLACE PACKAGE HR_Package AS
    FUNCTION get_dept_id (Dept_Name VARCHAR2)
        RETURN NUMBER;
    TYPE dept_id_array_type IS VARRAY(100) OF NUMBER;
    FUNCTION print_department_names
        RETURN NUMBER;
    No_City EXCEPTION;
    FUNCTION get_city (Loc_id HR.Locations.Location_ID%TYPE)
        RETURN VARCHAR2;
END HR_Package;
/
CREATE OR REPLACE PACKAGE BODY HR_Package AS
    FUNCTION get_dept_id (Dept_Name VARCHAR2)
        RETURN NUMBER IS 
            dept_id NUMBER;
        BEGIN
            SELECT
                Department_Id INTO dept_id
            FROM
                HR.Departments
            WHERE
                Department_Name = Dept_Name;
            RETURN (dept_id);
        EXCEPTION
            WHEN no_data_found THEN
                dbms_output.put_line('Department ' || Dept_Name || ' does not exist.');
                RETURN -1;
        END;
        
    FUNCTION print_department_names
        RETURN NUMBER IS
        BEGIN
            FOR dept IN (   SELECT
                                Department_Name,
                                Department_Id
                            FROM
                                HR.Departments) 
            LOOP
                DBMS_OUTPUT.PUT_LINE(dept.Department_Name || ' ' || dept.Department_Id);
            END LOOP;
            RETURN 1;
        END;
        
    FUNCTION get_city (Loc_id HR.Locations.Location_ID%TYPE)
        RETURN VARCHAR2 IS
            city_name VARCHAR2(50);
        BEGIN
            SELECT
                City INTO city_name
            FROM
                HR.Locations
            WHERE
                Location_Id = loc_id;
            
            RETURN city_name;
        EXCEPTION
            WHEN no_data_found THEN
                dbms_output.put_line('Location_ID ' || Loc_ID || ' does not exist.');
                RAISE No_City;
                RETURN Loc_ID || ' does not exist.';
        END;
END HR_Package;
/
DECLARE
    result NUMBER(4,0);
BEGIN
    result := HR_Package.get_dept_id('Marketing');
    dbms_output.put_line(result);
END;
/
CREATE TABLE HRDepartments AS
SELECT
    *
FROM
    HR.Departments;
/
CREATE OR REPLACE PROCEDURE add_dept_manager (dept_name IN VARCHAR2, man_id IN NUMBER, dept_id OUT NUMBER) AS
BEGIN
    SELECT
        Department_Id INTO dept_id
    FROM
        HRDepartments
    WHERE
        Department_Name = dept_name;
        
    UPDATE HRDepartments SET  Manager_Id = man_id
    WHERE Department_ID = dept_id;
END;
/
DECLARE 
	dept_id NUMBER(4, 0);
BEGIN 
    add_dept_manager('Treasury', 200, dept_id);
    DBMS_OUTPUT.PUT_LINE(dept_id); 
END;
/
SELECT
    *
FROM
    HR.Employees
WHERE
    Department_Id = HR_Package.get_dept_id('Marketing');
/
DECLARE
    dept_id_array HR_Package.dept_id_array_type;
    num NUMBER;
BEGIN
    SELECT
        Department_Id BULK COLLECT INTO dept_id_array
    FROM
        HR.Departments;
    
    FOR i IN 1 .. dept_id_array.COUNT LOOP
        DBMS_OUTPUT.PUT_LINE(dept_id_array(i));
    END LOOP;
    
    num := HR_Package.print_department_names;
END;
/
DECLARE
    dept_id NUMBER;
BEGIN
    dept_id := HR_Package.get_dept_id('Security');
END;
/
DECLARE
    city_name VARCHAR2(50);
BEGIN
    city_name := HR_Package.get_city (9000);
    DBMS_OUTPUT.PUT_LINE(city_name);
EXCEPTION
    WHEN HR_Package.No_City THEN
        DBMS_OUTPUT.PUT_LINE('No city found');
END;
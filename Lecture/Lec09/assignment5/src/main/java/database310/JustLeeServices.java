package database310;
import java.sql.*;
import java.util.Map;

public class JustLeeServices {
    public static void main (String[] args) throws SQLException, ClassNotFoundException
    {
        Connection conn = getConnection();

        // Publisher publisher = new Publisher(10, "WAN MEI PUBLISHING", "WANMEITAO", "514-123-1234");
        // addPublisher(conn, publisher);

        Publisher publisher2 = getPublisher(conn, "PRINTING IS US");
        System.out.println(publisher2);


        Book book = new Book("12345678", "A New Book", Date.valueOf("2023-10-15"), 1, 23.67, 12.04, 2, "SELF HELP", getPublisher(conn, 4));
        addBook(conn, book);

        Book anotherBook = getBook(conn, book.getIsbn());
        System.out.println(anotherBook);

        if(!conn.isClosed())
        {
            conn.close();
        }
        
    }

    public static Publisher getPublisher(Connection conn, String name)
    {
        String sql = "{? = call get_publisher(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql))
        {
            stmt.registerOutParameter(1, Types.STRUCT, "PUBLISHER_TYPE");
            stmt.setString(2, name);
            stmt.execute();
            Publisher newPublisher = (Publisher)stmt.getObject(1);
            return newPublisher;
        }
        catch (SQLException e)
        {
            throw new IllegalArgumentException("Error getting publisher.\n" + e.getMessage());
        }
    }

    public static void addPublisher(Connection conn, Publisher publisher)
    {
        try
        {
            Map<String, Class<?>> map = conn.getTypeMap();
            conn.setTypeMap(map);
            map.put(publisher.getSQLTypeName(), Class.forName("database310.Publisher"));
            String sql = "{call Add_Publisher(?)}";
            try (CallableStatement stmt = conn.prepareCall(sql))
            {
                stmt.setObject(1, publisher);
                stmt.execute();
            }
        }
        catch (SQLException e)
        {
            throw new IllegalArgumentException("SQLException: Issue adding publisher" + "\n" + e.getMessage());
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("Exception: Issue adding publisher" + "\n" + e.getMessage());

        }

    }

    public static void addBook(Connection conn, Book book)
    {
        try
        {
            String insertStatement =    "INSERT INTO Books (isbn, title, pubDate, pubid, cost, retail, discount, category) " +
                                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = conn.prepareStatement(insertStatement);
            statement.setString(1, book.getIsbn());
            statement.setString(2, book.getTitle());
            statement.setDate(3, book.getPubDate());
            statement.setInt(4, book.getPubid());
            statement.setDouble(5, book.getCost());
            statement.setDouble(6, book.getRetail());
            statement.setDouble(7, book.getDiscount());
            statement.setString(8, book.getCategory());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) 
            {
                throw new IllegalArgumentException("No row was inserted.");
            }
        }
        catch (SQLException e)
        {
            throw new IllegalArgumentException("Error inserting the following book: " + book.getTitle() + "\n" + e.getMessage());
        }
    }

    public static Book getBook(Connection conn, String isbn)
    {   
        try 
        {
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM Books WHERE Isbn = ?");
            statement.setString(1, isbn);
            ResultSet result = statement.executeQuery();
            while (result.next())
            {            
                String title = result.getString("title");
                Date pubDate = result.getDate("pubdate");
                int pubid = result.getInt("pubid");
                double cost = result.getDouble("cost");
                double retail = result.getDouble("retail");
                double discount = result.getDouble("discount");
                String category = result.getString("category");                
                Book book = new Book(isbn, title, pubDate, pubid, cost, retail, discount, category, getPublisher(conn, pubid));

                if(!statement.isClosed())   
                {
                    statement.close();
                }

                return book;
            }
        }
        catch (SQLException e)
        {
            throw new IllegalArgumentException("Error getting book with isbn: " + isbn + "\n" + e.getMessage());
        }
        throw new IllegalArgumentException("This code should not be reached. One of the possible reason is inexistant isbn");
    }

    public static Publisher getPublisher(Connection conn, int pubid)
    {
        try 
        {
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM Publisher WHERE PubId = ?");
            statement.setInt(1, pubid);
            ResultSet result = statement.executeQuery();
            while (result.next())
            {            
                String name = result.getString("name");
                String contact = result.getString("contact");
                String phone = result.getString("phone");
                Publisher publisher = new Publisher(pubid, name, contact, phone);

                if(!statement.isClosed())
                {
                    statement.close();
                }

                return publisher;
            }
        }
        catch (SQLException e)
        {
            throw new IllegalArgumentException("Error getting publisher with pubid: " + pubid + "\n" + e.getMessage());
        }
        throw new IllegalArgumentException("This code should not be reached. One of the possible reason is inexistant pubid");
    }

    public static Connection getConnection()
    {
        Connection conn;
        try
        {
            String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
            String user = System.console().readLine("Username: ");
            char[] passwordLetters = System.console().readPassword("Password: ");
            String password = "";
            for (int i = 0; i < passwordLetters.length; i++)
            {
                password += passwordLetters[i];
            }
            conn = DriverManager.getConnection(url, user, password);
        }
        catch (SQLException e)
        {
            throw new IllegalArgumentException("Error creating a new connection " + "\n" + e.getMessage());
        }
        return conn;
    }
}

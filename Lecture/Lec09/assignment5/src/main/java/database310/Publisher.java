package database310;
import java.sql.*;

public class Publisher implements SQLData {
    private int pubid;
    private String name;
    private String contact;
    private String phone;
    private final String typeName = "PUBLISHER_TYPE";

    public Publisher()
    {

    }

    public Publisher(int pubid, String name, String contact, String phone)
    {
        this.pubid = pubid;
        this.name = name;
        this.contact = contact;
        this.phone = phone;
    }

    public String toString()
    {
        return "Pubid: " + this.pubid + "\nName: " + this.name + "\nContact: " + this.contact + "\nPhone: " + this.phone;
    }

    public int getPubid()
    {
        return this.pubid;
    }

    public String getName()
    {
        return this.name;
    }

    public String getContact()
    {
        return this.contact;
    }

    public String getPhone()
    {
        return this.phone;
    }

    public void setPubid(int pubid)
    {
        this.pubid = pubid;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setContact(String contact)
    {
        this.contact = contact;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    @Override
    public String getSQLTypeName() throws SQLException
    {
        return this.typeName;
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException
    {
        stream.writeInt(getPubid());
        stream.writeString(getName());
        stream.writeString(getContact());
        stream.writeString(getPhone());
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException
    {
        setPubid(stream.readInt());
        setName(stream.readString());
        setContact(stream.readString());
        setPhone(stream.readString());
    }
}

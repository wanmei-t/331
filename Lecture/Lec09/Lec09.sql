CREATE OR REPLACE TYPE Publisher_Type AS OBJECT
(
    PubId NUMBER(2),
    Name VARCHAR2(23),
    Contact VARCHAR2(15),
    Phone VARCHAR2(12)
);
/
CREATE OR REPLACE PROCEDURE Add_Publisher 
(
    VPublisher IN Publisher_Type
) IS
BEGIN
    INSERT INTO Publisher
    VALUES (VPublisher.PubId, VPublisher.Name, VPublisher.Contact, VPublisher.Phone);
END;
/
CREATE OR REPLACE FUNCTION Get_Publisher
(
    VName VARCHAR2
)
RETURN Publisher_Type IS
    VPublisher Publisher_Type;
BEGIN
    SELECT
        Pubid,
        Name,
        Contact,
        Phone
    INTO
        VPublisher.PubId,
        VPublisher.Name,
        VPublisher.Contact,
        VPublisher.Phone
    FROM
        Publisher
    WHERE
        Name = VName;
    
    RETURN VPublisher;
END;
/

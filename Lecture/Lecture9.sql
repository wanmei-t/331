CREATE TYPE Author_Type AS OBJECT (
    Author_Id VARCHAR(4),
    LName VARCHAR2(10),
    FName VARCHAR2(10)
);

CREATE OR REPLACE PROCEDURE Add_Author (
    VAuthor IN Author_Type
) IS
BEGIN
    INSERT INTO Author
    VALUES (VAuthor.Author_Id, VAuthor.LName, VAuthor.Fname);
END;
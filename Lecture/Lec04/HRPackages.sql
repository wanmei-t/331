CREATE TABLE HRDepartments AS
SELECT
    *
FROM
    HR.Departments;
/
CREATE OR REPLACE PROCEDURE add_dept_manager (dept_name IN VARCHAR2, man_id IN NUMBER, dept_id OUT NUMBER) AS
BEGIN
    SELECT
        Department_Id INTO dept_id
    FROM
        HRDepartments
    WHERE
        Department_Name = dept_name;
        
    UPDATE HRDepartments SET  Manager_Id = man_id
    WHERE Department_ID = dept_id;
END;
/
DECLARE 
	dept_id NUMBER(4, 0);
BEGIN 
    add_dept_manager('Treasury', 200, dept_id);
    DBMS_OUTPUT.PUT_LINE(dept_id); 
END;
/
SELECT
    *
FROM
    HR.Employees
WHERE
    Department_Id = HR_Package.get_dept_id('Marketing');
CREATE OR REPLACE PACKAGE HR_Package AS
    FUNCTION get_dept_id (Dept_Name VARCHAR2)
    RETURN NUMBER;
END HR_Package;
/
CREATE OR REPLACE PACKAGE BODY HR_Package AS
    FUNCTION get_dept_id (Dept_Name VARCHAR2)
        RETURN NUMBER IS 
            dept_id NUMBER;
        BEGIN
            SELECT
                Department_Id INTO dept_id
            FROM
                HR.Departments
            WHERE
                Department_Name = Dept_Name;
            RETURN (dept_id);
        END;
END HR_Package;
/
DECLARE
    result NUMBER(4,0);
BEGIN
    result := HR_Package.get_dept_id('Marketing');
    dbms_output.put_line(result);
END;


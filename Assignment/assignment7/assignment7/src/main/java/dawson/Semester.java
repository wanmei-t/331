package dawson;
import java.sql.*;
import java.util.Map;

public class Semester implements SQLData {
    private int semesterId;
    private String semesterName;
    private final String typeName = "SEMESTER_TYPE";

    public Semester(int semesterId, String semesterName) {
        this.semesterId = semesterId;
        this.semesterName = semesterName;
    }

    public void addToDatabse(Connection conn) throws SQLException, ClassNotFoundException
    {  
        Map<String, Class<?>> map = conn.getTypeMap();
        conn.setTypeMap(map);
        map.put(this.typeName, Class.forName("dawson.Semester"));
        String sql =  "{call add_semester(?)}";
        CallableStatement stmt = conn.prepareCall(sql);	
        Semester semester = new Semester(this.semesterId, this.semesterName);
        stmt.setObject(1, semester);
        stmt.execute();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException 
	{
        stream.writeInt(getSemesterId());
		stream.writeString(getSemesterName());
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException 
	{
        setSemesterId(stream.readInt());
        setSemesterName(stream.readString());
	}

    @Override
    public String getSQLTypeName() throws SQLException
	{
		return this.typeName;
	}

    public void setSemesterId(int semesterId)
    {
        this.semesterId = semesterId;
    }

    public void setSemesterName(String semesterName)
    {
        this.semesterName = semesterName;
    }

    public int getSemesterId() {
        return this.semesterId;
    }

    public String getSemesterName() {
        return this.semesterName;
    }

    @Override
    public String toString() {
        return "[SEMESTER] " +
               "\nSemester ID: " + this.semesterId +
               "\nSemester Name: " + this.semesterName;
    }
}

package dawson;
import java.sql.*;
import java.util.Map;

public class Term implements SQLData {
    private int termNumber;
    private int semesterId;
    private final String typeName = "TERM_TYPE";

    public Term(int termNumber, int semesterId) {
        this.termNumber = termNumber;
        this.semesterId = semesterId;
    }

    public void addToDatabse(Connection conn) throws SQLException, ClassNotFoundException
    {  
        Map<String, Class<?>> map = conn.getTypeMap();
        conn.setTypeMap(map);
        map.put(this.typeName, Class.forName("dawson.Term"));
        String sql =  "{call add_term(?)}";
        CallableStatement stmt = conn.prepareCall(sql);	
        Term term = new Term(this.termNumber, this.semesterId);
        stmt.setObject(1, term);
        stmt.execute();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException 
	{
        stream.writeInt(getTermNumber());
		stream.writeInt(getSemesterId());
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException 
	{
        setTermNumber(stream.readInt());
        setSemesterId(stream.readInt());
	}

    @Override
    public String getSQLTypeName() throws SQLException
	{
		return this.typeName;
	}

    public void setTermNumber(int termNumber)
    {
        this.termNumber = termNumber;
    }

    public void setSemesterId(int semesterId)
    {
        this.semesterId = semesterId;
    }
    
    public int getTermNumber() {
        return this.termNumber;
    }

    public int getSemesterId() {
        return this.semesterId;
    }

    @Override
    public String toString() {
        return "[TERM]" +
               "\nTerm Number: " + this.termNumber +
               "\nSemester ID: " + this.semesterId;
    }
}

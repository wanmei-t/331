package dawson;

import java.sql.SQLException;

public class App {
    public static void main(String[] args) throws SQLException, ClassNotFoundException
    {
        String user = System.console().readLine("Username: ");
        char[] passwordLetters = System.console().readPassword("Password: ");
        String password = "";
        for (int i = 0; i < passwordLetters.length; i++)
        {
            password += passwordLetters[i];
        }
        CourseListServices c = new CourseListServices(user, password);
        c.addCourse();
        c.close();
    }
}

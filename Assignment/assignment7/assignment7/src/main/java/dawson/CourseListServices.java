package dawson;
import java.sql.*;

public class CourseListServices {
    private Connection conn;

    public CourseListServices(String username, String password) throws SQLException
    {
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        this.conn = DriverManager.getConnection(url, username, password);    
    }

    public void close() throws SQLException
    {
        if (!this.conn.isClosed())
        {
            this.conn.close();
        }
    }

    public void addCourse() throws SQLException, ClassNotFoundException
    {
        String courseNumber = System.console().readLine("What is the course number: ");
        String courseName = System.console().readLine("What is the course name: ");
        int hoursClass = Integer.parseInt(System.console().readLine("How many hours will be dedicated for classes? Enter an integer: "));
        int hoursLab = Integer.parseInt(System.console().readLine("How many hours will be dedicated for labs? Enter an integer: "));
        int hoursHomework = Integer.parseInt(System.console().readLine("How many hours will be dedicated for homework? Enter an integer: "));
        int termNumber = Integer.parseInt(System.console().readLine("Which term is this course planned for? Enter an integer: "));
        String courseDescription = System.console().readLine("What is the course description? If it is not available, press ENTER: ");
        Course course = new Course(courseNumber, courseName, hoursClass, hoursLab, hoursHomework, termNumber, courseDescription);
        course.addToDatabse(this.conn);
    }
}

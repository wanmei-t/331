package dawson;
import java.sql.*;
import java.util.Map;

public class Course implements SQLData {
    private String courseNumber;
    private String courseName;
    private int hoursClass;
    private int hoursLab;
    private int hoursHomework;
    private int termNumber;
    private String courseDescription;
    private final String typeName = "COURSE_TYPE";

    public Course(String courseNumber, String courseName, int hoursClass, int hoursLab, int hoursHomework, int termNumber, String courseDescription) {
        this.courseNumber = courseNumber;
        this.courseName = courseName;
        this.hoursClass = hoursClass;
        this.hoursLab = hoursLab;
        this.hoursHomework = hoursHomework;
        this.termNumber = termNumber;
        this.courseDescription = courseDescription;
    }

    public void addToDatabse(Connection conn) throws SQLException, ClassNotFoundException
    {  
        Map<String, Class<?>> map = conn.getTypeMap();
        conn.setTypeMap(map);
        map.put(this.typeName, Class.forName("dawson.Course"));
        String sql =  "{call add_course(?)}";
        CallableStatement stmt = conn.prepareCall(sql);	
        Course course = new Course(this.courseNumber, this.courseName, this.hoursClass, this.hoursLab, this.hoursHomework, this.termNumber, this.courseDescription);
        stmt.setObject(1, course);
        stmt.execute();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException 
	{
        stream.writeString(getCourseNumber());
		stream.writeString(getCourseName());
        stream.writeInt(getHoursClass());
        stream.writeInt(getHoursLab());
        stream.writeInt(getHoursHomework());
        stream.writeInt(getTermNumber());
		stream.writeString(getCourseDescription());
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException 
	{
        setCourseNumber(stream.readString());
        setCourseName(stream.readString());
        setHoursClass(stream.readInt());
        setHoursLab(stream.readInt());
        setHoursHomework(stream.readInt());
        setTermNumber(stream.readInt());
        setCourseDescription(stream.readString());
	}

    @Override
    public String getSQLTypeName() throws SQLException
	{
		return this.typeName;
	}

    public void setCourseNumber(String courseNumber) {
        this.courseNumber = courseNumber;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public void setHoursClass(int hoursClass) {
        this.hoursClass = hoursClass;
    }

    public void setHoursLab(int hoursLab) {
        this.hoursLab = hoursLab;
    }

    public void setHoursHomework(int hoursHomework) {
        this.hoursHomework = hoursHomework;
    }

    public void setTermNumber(int termNumber) {
        this.termNumber = termNumber;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    public String getCourseNumber() {
        return this.courseNumber;
    }

    public String getCourseName() {
        return this.courseName;
    }

    public int getHoursClass() {
        return this.hoursClass;
    }

    public int getHoursLab() {
        return this.hoursLab;
    }

    public int getHoursHomework() {
        return this.hoursHomework;
    }

    public int getTermNumber() {
        return this.termNumber;
    }

    public String getCourseDescription() {
        return this.courseDescription;
    }

    @Override
    public String toString() {
        return "[COURSE]" +
               "\nCourse Number: " + this.courseNumber +
               "\nCourse Name: " + this.courseName +
               "\nHours (Class): " + this.hoursClass +
               "\nHours (Lecture): " + this.hoursLab +
               "\nHours (Homework): " + this.hoursHomework +
               "\nTerm Number: " + this.termNumber +
               "\nCourse Description: " + this.courseDescription;
    }
}

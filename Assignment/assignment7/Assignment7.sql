-- Drop tables
DROP TABLE W_Courses;
DROP TABLE W_Terms;
DROP TABLE W_Semesters;


-- Create tables
CREATE TABLE W_Semesters ( 
	SemesterId 	    NUMBER(1)       PRIMARY KEY,
	SemesterName    VARCHAR2(6) 	NOT NULL
);

CREATE TABLE W_Terms ( 
	TermNumber 	NUMBER(1)   PRIMARY KEY,
	SemesterId 	NUMBER(1)   NOT NULL REFERENCES W_Semesters(SemesterId)
);

CREATE TABLE W_Courses ( 
	CourseNumber 	    VARCHAR2(10)    PRIMARY KEY,
	CourseName 	        VARCHAR2(50) 	NOT NULL, 
	HoursClass 	        NUMBER(1) 	    NOT NULL,
    HoursLab            NUMBER(1)       NOT NULL,
    HoursHomework       NUMBER(1)       NOT NULL,
    -- I'm assuming term number represents the term this course is planified for the student to take
    TermNumber          NUMBER(1)       NOT NULL REFERENCES W_Terms(TermNumber),
    CourseDescription   VARCHAR2(2000)
);


-- Inserting basic minimal data
INSERT INTO W_Semesters
VALUES (1, 'FALL');
INSERT INTO W_Semesters
VALUES (2, 'WINTER');
INSERT INTO W_Semesters
VALUES (3, 'SUMMER');

INSERT INTO W_Terms
VALUES (1, 1);
INSERT INTO W_Terms
VALUES (2, 2);
INSERT INTO W_Terms
VALUES (3, 1);
INSERT INTO W_Terms
VALUES (4, 2);
INSERT INTO W_Terms
VALUES (5, 1);
INSERT INTO W_Terms
VALUES (6, 2);

-- Insert data for Programming I
INSERT INTO W_Courses
VALUES ('420-110-DW', 'Programming I', 3, 3, 3, 1, 'The course will introduce the student to the basic building blocks (sequential,
selection and repetitive control structures) and modules (methods and classes)
used to write a program. The student will use the Java programming language to
implement the algorithms studied. The array data structure is introduced, and
student will learn how to program with objects.');


-- Insert data for Programming II
INSERT INTO W_Courses
VALUES ('420-210-DW', 'Programming II', 3, 3, 3, 2, 'The course will introduce the student to basic object-oriented methodology in
order to design, implement, use and modify classes, to write programs in the
Java language that perform interactive processing, array and string processing,
and data validation. Object-oriented features such as encapsulation and
inheritance will be explored.');

/
-- Term
CREATE OR REPLACE TYPE Term_Type AS OBJECT (
    TermNumber NUMBER(1),  
    SemesterId NUMBER(1)
);
/
CREATE OR REPLACE PROCEDURE Add_Term (
    VTerm IN Term_Type
) IS
BEGIN
    INSERT INTO W_Terms
    VALUES (VTerm.TermNumber, VTerm.SemesterId);
END;
/
CREATE OR REPLACE FUNCTION Get_Term (
	VTermNumber IN NUMBER
) RETURN Term_Type
AS
    VTerm Term_Type;
BEGIN
	SELECT
        TermNumber,
		SemesterId
    INTO
        VTerm.TermNumber,
        VTerm.SemesterId
	FROM
		W_Terms
	WHERE
		TermNumber = VTermNumber;
        
    RETURN VTerm;
END;
/

-- Semester
CREATE OR REPLACE TYPE Semester_Type AS OBJECT (  
    SemesterId NUMBER(1),
    SemesterName VARCHAR2(6)
);
/
CREATE OR REPLACE PROCEDURE Add_Semester (
    VSemester IN Semester_Type
) IS
BEGIN
    INSERT INTO W_Semesters
    VALUES (VSemester.SemesterId, VSemester.SemesterName);
END;
/
CREATE OR REPLACE FUNCTION Get_Semester (
	VSemesterId IN NUMBER
) RETURN Semester_Type
AS
    VSemester Semester_Type;
BEGIN
	SELECT
		SemesterId,
        SemesterName
    INTO
        VSemester.SemesterId, 
        VSemester.SemesterName
	FROM
		W_Semesters
	WHERE
		SemesterId = VSemesterId;
        
    RETURN VSemester;
END;
/

-- Course
CREATE OR REPLACE TYPE Course_Type AS OBJECT (  
    CourseNumber VARCHAR2(10),
    CourseName VARCHAR2(50),
    HoursClass NUMBER(1),
    HoursLab NUMBER(1),
    HoursHomework NUMBER(1),
    TermNumber NUMBER(1),
    CourseDescription VARCHAR2(2000)
);
/
CREATE OR REPLACE PROCEDURE Add_Course (
    VCourse IN Course_Type
) IS
BEGIN
    INSERT INTO W_Courses
    VALUES (VCourse.CourseNumber, VCourse.CourseName, VCourse.HoursClass, VCourse.HoursLab, VCourse.HoursHomework, VCourse.TermNumber, VCourse.CourseDescription);
END;
/
CREATE OR REPLACE FUNCTION Get_Course (
	VCourseNumber IN VARCHAR2
) RETURN Course_Type
AS
    VCourse Course_Type;
BEGIN
	SELECT
		CourseNumber, 
        CourseName, 
        HoursClass, 
        HoursLab, 
        HoursHomework, 
        TermNumber, 
        CourseDescription 
    INTO
        VCourse.CourseNumber, 
        VCourse.CourseName, 
        VCourse.HoursClass, 
        VCourse.HoursLab, 
        VCourse.HoursHomework, 
        VCourse.TermNumber, 
        VCourse.CourseDescription
	FROM
		W_Courses
	WHERE
		CourseNumber = VCourseNumber;
        
    RETURN VCourse;
END;
/
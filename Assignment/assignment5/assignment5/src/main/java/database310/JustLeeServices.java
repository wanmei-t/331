package database310;
import java.sql.*;

public class JustLeeServices {
    public static void main (String[] args)
    {
        try
        {
            Connection conn = getConnection();

            Book book = new Book("12345678", "A New Book", Date.valueOf("2023-10-15"), 1, 23.67, 12.04, 2, "SELF HELP", getPublisher(conn, 4));
            addBook(conn, book);

            Book anotherBook = getBook(conn, book.getIsbn());
            System.out.println(anotherBook);

            if(!conn.isClosed())
            {
                conn.close();
            }
        }
        catch (SQLException e)
        {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public static void addBook(Connection conn, Book book)
    {
        try
        {
            String insertStatement =    "INSERT INTO Books (isbn, title, pubDate, pubid, cost, retail, discount, category) " +
                                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = conn.prepareStatement(insertStatement);
            statement.setString(1, book.getIsbn());
            statement.setString(2, book.getTitle());
            statement.setDate(3, book.getPubDate());
            statement.setInt(4, book.getPubid());
            statement.setDouble(5, book.getCost());
            statement.setDouble(6, book.getRetail());
            statement.setDouble(7, book.getDiscount());
            statement.setString(8, book.getCategory());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) 
            {
                throw new IllegalArgumentException("No row was inserted.");
            }
        }
        catch (SQLException e)
        {
            throw new IllegalArgumentException("Error inserting the following book: " + book.getTitle() + "\n" + e.getMessage());
        }
    }

    public static Book getBook(Connection conn, String isbn)
    {   
        try 
        {
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM Books WHERE Isbn = ?");
            statement.setString(1, isbn);
            ResultSet result = statement.executeQuery();
            while (result.next())
            {            
                String title = result.getString("title");
                Date pubDate = result.getDate("pubdate");
                int pubid = result.getInt("pubid");
                double cost = result.getDouble("cost");
                double retail = result.getDouble("retail");
                double discount = result.getDouble("discount");
                String category = result.getString("category");                
                Book book = new Book(isbn, title, pubDate, pubid, cost, retail, discount, category, getPublisher(conn, pubid));

                if(!statement.isClosed())   
                {
                    statement.close();
                }

                return book;
            }
        }
        catch (SQLException e)
        {
            throw new IllegalArgumentException("Error getting book with isbn: " + isbn + "\n" + e.getMessage());
        }
        throw new IllegalArgumentException("This code should not be reached. One of the possible reason is inexistant isbn");
    }

    public static Publisher getPublisher(Connection conn, int pubid)
    {
        try 
        {
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM Publisher WHERE PubId = ?");
            statement.setInt(1, pubid);
            ResultSet result = statement.executeQuery();
            while (result.next())
            {            
                String name = result.getString("name");
                String contact = result.getString("contact");
                String phone = result.getString("phone");
                Publisher publisher = new Publisher(pubid, name, contact, phone);

                if(!statement.isClosed())
                {
                    statement.close();
                }

                return publisher;
            }
        }
        catch (SQLException e)
        {
            throw new IllegalArgumentException("Error getting publisher with pubid: " + pubid + "\n" + e.getMessage());
        }
        throw new IllegalArgumentException("This code should not be reached. One of the possible reason is inexistant pubid");
    }

    public static Connection getConnection()
    {
        Connection conn;
        try
        {
            String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
            String user = System.console().readLine("Username: ");
            char[] passwordLetters = System.console().readPassword("Password: ");
            String password = "";
            for (int i = 0; i < passwordLetters.length; i++)
            {
                password += passwordLetters[i];
            }
            conn = DriverManager.getConnection(url, user, password);
        }
        catch (SQLException e)
        {
            throw new IllegalArgumentException("Error creating a new connection " + "\n" + e.getMessage());
        }
        return conn;
    }
}

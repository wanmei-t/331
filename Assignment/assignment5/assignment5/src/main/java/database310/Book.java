package database310;
import java.sql.Date;

public class Book {
    private String isbn;
    private String title;
    private Date pubDate;
    private int pubid;
    private double cost;
    private double retail;
    private double discount;
    private String category;
    private Publisher publisher;

    public Book(String isbn, String title, Date pubDate, int pubid, double cost, double retail, double discount, String category, Publisher publisher)
    {
        this.isbn = isbn;
        this.title = title;
        this.pubDate = pubDate;
        this.pubid = pubid;
        this.cost = cost;
        this.retail = retail;
        this.discount = discount;
        this.category = category;
        this.publisher = publisher;
    }

    public String toString()
    {
        return "ISBN: " + this.isbn + "\nTitle: " + this.title + "\nDate: " + this.pubDate + "\nPubID: " + this.pubid + "\nCost: " + this.cost + "\nRetail: " + this.retail + "\nDiscount: " + this.discount + "\nCategory: " + this.category + "\n" + this.publisher;
    }

    public String getIsbn() {
        return this.isbn;
    }

    public String getTitle() {
        return this.title;
    }

    public Date getPubDate() {
        return this.pubDate;
    }

    public int getPubid() {
        return this.pubid;
    }

    public double getCost() {
        return this.cost;
    }

    public double getRetail() {
        return this.retail;
    }

    public double getDiscount() {
        return this.discount;
    }

    public String getCategory() {
        return this.category;
    }

    public Publisher getPublisher()
    {
        return this.publisher;
    }

}

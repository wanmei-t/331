CREATE OR REPLACE PACKAGE book_store AS
    FUNCTION get_price_after_tax (book_isbn VARCHAR2)
        RETURN NUMBER;
    TYPE customer_id_array_type IS VARRAY(100) OF Customers.Customer#%TYPE;
    PROCEDURE show_purchases;
    PROCEDURE rename_category (category_name Books.Category%TYPE, new_category_name Books.Category%TYPE);
    Category_Not_Found EXCEPTION;
    Category_Name_Too_Long EXCEPTION;
    PROCEDURE add_publisher (pub_name Publisher.Name%TYPE, pub_contact Publisher.Contact%TYPE, pub_phone Publisher.Phone%TYPE);
    Publisher_Exists_Already EXCEPTION;
END book_store;
/
CREATE OR REPLACE PACKAGE BODY book_store AS        
    FUNCTION price_after_discount (book_isbn VARCHAR2)
        RETURN NUMBER IS 
            discount_price NUMBER;
            book_retail NUMBER;
            book_discount NUMBER;
        BEGIN
            SELECT
                Retail INTO book_retail
            FROM
                Books
            WHERE
                ISBN = book_isbn;
            
            SELECT
                NVL(Discount, 0) INTO book_discount
            FROM
                Books
            WHERE
                ISBN = book_isbn;
            
            discount_price := (book_retail - book_discount);
            
            RETURN discount_price;            
        END;
        
    FUNCTION get_price_after_tax (book_isbn VARCHAR2)
        RETURN NUMBER IS 
            book_final_price NUMBER;
            book_discounted_price NUMBER;
        BEGIN
            book_discounted_price := price_after_discount(book_isbn);
            book_final_price := (book_discounted_price*1.15);
            RETURN book_final_price;
        END;    
        
    FUNCTION book_purchasers (targetISBN Books.ISBN%TYPE)
        RETURN customer_id_array_type IS
            customer_id_array customer_id_array_type;
        BEGIN
            SELECT DISTINCT
                Customer# BULK COLLECT INTO customer_id_array
            FROM
                Orders INNER JOIN ORDERITEMS 
                USING(Order#)
            WHERE 
                ISBN = targetISBN;
            
            RETURN customer_id_array;
        END;
        
    PROCEDURE show_purchases AS
        book_name Books.Title%TYPE;
        customer_id_array customer_id_array_type;
        customer_name VARCHAR2(30);
        all_customers_names VARCHAR2(200) := '';
    BEGIN
        FOR row IN (  SELECT
                        ISBN,
                        Title
                    FROM
                        Books) 
        LOOP
            customer_id_array := book_purchasers(row.ISBN);
            FOR i IN 1 .. customer_id_array.COUNT 
            LOOP
                SELECT
                    FirstName || ' ' || LastName INTO customer_name
                FROM
                    Customers
                WHERE
                    Customer# = customer_id_array(i);
                    
                all_customers_names := all_customers_names || ' ' || customer_name || ',';
            END LOOP;
            DBMS_OUTPUT.PUT_LINE(row.ISBN || ' : ' || row.Title || ' : ' || all_customers_names);
            all_customers_names := '';
        END LOOP;
    END;
    
    PROCEDURE rename_category (category_name IN Books.Category%TYPE, new_category_name IN Books.Category%TYPE) AS
        count_category NUMBER;
    BEGIN    
        DBMS_OUTPUT.PUT_LINE('Trying to rename category ' || UPPER(category_name) || ' to ' || UPPER(new_category_name));
        
        IF (LENGTH(UPPER(new_category_name))>12) THEN
            RAISE Category_Name_Too_Long;
        END IF;
        
        SELECT
            COUNT(*) INTO count_category
        FROM
            Books
        WHERE 
            Category = UPPER(category_name);
        
        IF (count_category < 1) THEN
            RAISE Category_Not_Found;
        END IF;        
        
        UPDATE Books SET Category = UPPER(new_category_name)
        WHERE Category = UPPER(category_name);
        
        DBMS_OUTPUT.PUT_LINE('Successfully renamed ' || UPPER(category_name) || ' to ' || UPPER(new_category_name));
    END;
    
    PROCEDURE add_publisher (pub_name IN Publisher.Name%TYPE, pub_contact IN Publisher.Contact%TYPE, pub_phone IN Publisher.Phone%TYPE) AS
        count_publisher NUMBER;
        last_pubid Publisher.Pubid%TYPE;
    BEGIN
        DBMS_OUTPUT.PUT_LINE('Trying to add publisher ' || UPPER(pub_name));
        
        SELECT
            COUNT(*) INTO count_publisher
        FROM
            Publisher
        WHERE
            Name = UPPER(pub_name) AND
            Contact = UPPER(pub_contact) AND
            Phone = pub_phone;
        
        IF (count_publisher >= 1) THEN
            RAISE Publisher_Exists_Already;
        END IF;
        
        SELECT
            MAX(PubId) INTO last_pubid
        FROM
            Publisher;
        
        INSERT INTO Publisher (Pubid, Name, Contact, Phone)
        VALUES (last_pubid+1, UPPER(pub_name), UPPER(pub_contact), pub_phone);
        
        DBMS_OUTPUT.PUT_LINE('Successfully added ' || UPPER(pub_name));
    END;
END book_store;
/
BEGIN
    book_store.rename_category('Computer', 'Computer Science');
EXCEPTION
    WHEN book_store.Category_Not_Found THEN
        DBMS_OUTPUT.PUT_LINE('Category was not found.');
        ROLLBACK;
    WHEN book_store.Category_Name_Too_Long THEN
        DBMS_OUTPUT.PUT_LINE('Category name is too long.');
        ROLLBACK;
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('An unknown error occured.');
        ROLLBACK;
END;
/
BEGIN
    book_store.rename_category('Teaching', 'Education');
EXCEPTION
    WHEN book_store.Category_Not_Found THEN
        DBMS_OUTPUT.PUT_LINE('Category was not found.');
        ROLLBACK;
    WHEN book_store.Category_Name_Too_Long THEN
        DBMS_OUTPUT.PUT_LINE('Category name is too long.');
        ROLLBACK;
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('An unknown error occured.');
        ROLLBACK;
END;
/
BEGIN
    book_store.add_publisher('Dawson Printing', 'John Smith', '111-555-2233');
EXCEPTION
    WHEN book_store.Publisher_Exists_Already THEN
        DBMS_OUTPUT.PUT_LINE('Publisher exists already.');
        ROLLBACK;
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('An unknown error occured.');
        ROLLBACK;
END;
/
BEGIN
    book_store.add_publisher('Publish Our Way', 'Jane Tomlin', '010-410-0010');
EXCEPTION
    WHEN book_store.Publisher_Exists_Already THEN
        DBMS_OUTPUT.PUT_LINE('Publisher exists already.');
        ROLLBACK;
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('An unknown error occured.');
        ROLLBACK;
END;
/

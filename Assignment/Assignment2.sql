CREATE OR REPLACE PACKAGE book_store AS
    FUNCTION get_price_after_tax (book_isbn VARCHAR2)
        RETURN NUMBER;
END book_store;
/
CREATE OR REPLACE PACKAGE BODY book_store AS        
    FUNCTION price_after_discount (book_isbn VARCHAR2)
        RETURN NUMBER IS 
            discount_price NUMBER;
            book_retail NUMBER;
            book_discount NUMBER;
        BEGIN
            SELECT
                Retail INTO book_retail
            FROM
                Books
            WHERE
                ISBN = book_isbn;
            
            SELECT
                NVL(Discount, 0) INTO book_discount
            FROM
                Books
            WHERE
                ISBN = book_isbn;
            
            discount_price := (book_retail - book_discount);
            
            RETURN discount_price;            
        END;
        
    FUNCTION get_price_after_tax (book_isbn VARCHAR2)
        RETURN NUMBER IS 
            book_final_price NUMBER;
            book_discounted_price NUMBER;
        BEGIN
            book_discounted_price := price_after_discount(book_isbn);
            book_final_price := (book_discounted_price*1.15);
            RETURN book_final_price;
        END;
END book_store;
/
DECLARE
    car_book_ISBN VARCHAR2(10);
    holy_grail_book_ISBN VARCHAR2(10);
    car_book_price NUMBER(5, 2);
    holy_grail_book_price NUMBER(5, 2);
BEGIN
    SELECT
        ISBN INTO car_book_ISBN
    FROM
        Books
    WHERE
        Title = 'BUILDING A CAR WITH TOOTHPICKS';
    
    SELECT
        ISBN INTO holy_grail_book_ISBN
    FROM
        Books
    WHERE
        Title = 'HOLY GRAIL OF ORACLE';
        
    car_book_price := book_store.get_price_after_tax(car_book_ISBN);
    holy_grail_book_price := book_store.get_price_after_tax(holy_grail_book_ISBN);
        
    DBMS_OUTPUT.PUT_LINE('The final price of BUILDING A CAR WITH TOOTHPICKS is: $' || car_book_price);
    DBMS_OUTPUT.PUT_LINE('The final price of HOLY GRAIL OF ORACLE is: $' || holy_grail_book_price);        
END;
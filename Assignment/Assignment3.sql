CREATE OR REPLACE PACKAGE book_store AS
    FUNCTION get_price_after_tax (book_isbn VARCHAR2)
        RETURN NUMBER;
    TYPE customer_id_array_type IS VARRAY(100) OF Customers.Customer#%TYPE;
    PROCEDURE show_purchases;
END book_store;
/
CREATE OR REPLACE PACKAGE BODY book_store AS        
    FUNCTION price_after_discount (book_isbn VARCHAR2)
        RETURN NUMBER IS 
            discount_price NUMBER;
            book_retail NUMBER;
            book_discount NUMBER;
        BEGIN
            SELECT
                Retail INTO book_retail
            FROM
                Books
            WHERE
                ISBN = book_isbn;
            
            SELECT
                NVL(Discount, 0) INTO book_discount
            FROM
                Books
            WHERE
                ISBN = book_isbn;
            
            discount_price := (book_retail - book_discount);
            
            RETURN discount_price;            
        END;
        
    FUNCTION get_price_after_tax (book_isbn VARCHAR2)
        RETURN NUMBER IS 
            book_final_price NUMBER;
            book_discounted_price NUMBER;
        BEGIN
            book_discounted_price := price_after_discount(book_isbn);
            book_final_price := (book_discounted_price*1.15);
            RETURN book_final_price;
        END;    
        
    FUNCTION book_purchasers (targetISBN Books.ISBN%TYPE)
        RETURN customer_id_array_type IS
            customer_id_array customer_id_array_type;
        BEGIN
            SELECT DISTINCT
                Customer# BULK COLLECT INTO customer_id_array
            FROM
                Orders INNER JOIN ORDERITEMS 
                USING(Order#)
            WHERE 
                ISBN = targetISBN;
            
            RETURN customer_id_array;
        END;
        
    PROCEDURE show_purchases AS
        book_name Books.Title%TYPE;
        customer_id_array customer_id_array_type;
        customer_name VARCHAR2(30);
        all_customers_names VARCHAR2(200) := '';
    BEGIN
        FOR row IN (  SELECT
                        ISBN,
                        Title
                    FROM
                        Books) 
        LOOP
            customer_id_array := book_purchasers(row.ISBN);
            FOR i IN 1 .. customer_id_array.COUNT 
            LOOP
                SELECT
                    FirstName || ' ' || LastName INTO customer_name
                FROM
                    Customers
                WHERE
                    Customer# = customer_id_array(i);
                    
                all_customers_names := all_customers_names || ' ' || customer_name || ',';
            END LOOP;
            DBMS_OUTPUT.PUT_LINE(row.ISBN || ' : ' || row.Title || ' : ' || all_customers_names);
            all_customers_names := '';
        END LOOP;
    END;
END book_store;
/
EXECUTE book_store.show_purchases;
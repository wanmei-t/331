DROP TABLE JLUSERS;
DROP TYPE JLUSER_TYPE;
DROP PROCEDURE Add_JLUser;
DROP FUNCTION Get_JLUser;
DROP PROCEDURE Update_JLUser;
/
CREATE TABLE JLUSERS (
    UserId              VARCHAR2(50)    PRIMARY KEY,
    Salt                RAW(16)         NOT NULL,
    Hash                RAW(64)         NOT NULL,
    FailedLoginCount    NUMBER(1)       DEFAULT(0)
);

CREATE TYPE JLUSER_TYPE AS OBJECT (
    UserId              VARCHAR2(50),
    Salt                RAW(16),
    Hash                RAW(64),
    FailedLoginCount    NUMBER(1)
);
/
CREATE OR REPLACE PROCEDURE Add_JLUser (
    vJLUser IN JLUSER_TYPE
) 
AS
BEGIN
    INSERT INTO JLUSERS
    VALUES (vJLUser.UserId, vJLUser.Salt, vJLUser.Hash, vJLUser.FailedLoginCount);
END;
/
CREATE OR REPLACE FUNCTION Get_JLUser (
    vUsername IN JLUSERS.UserId%TYPE
) 
RETURN JLUSER_TYPE 
AS
    vJLUser JLUSER_TYPE;
BEGIN
    SELECT 
        JLUSER_TYPE (UserId, Salt, Hash, FailedLoginCount)
    INTO 
        vJLUser
    FROM 
        JLUSERS
    WHERE 
        UserId = vUsername;

    RETURN vJLUser;
END;
/
CREATE OR REPLACE PROCEDURE Update_JLUser (
    vJLUser IN JLUSER_TYPE
)
AS
BEGIN
    UPDATE 
        JLUSERS
    SET 
        UserId = vJLUser.UserId,
        Salt = vJLUser.Salt,
        Hash = vJLUser.Hash,
        FailedLoginCount = vJLUser.FailedLoginCount
    WHERE
        UserId = vJLUser.UserId;

    IF SQL%NOTFOUND THEN
        Add_JLUser(vJLUser);
    END IF;
END;
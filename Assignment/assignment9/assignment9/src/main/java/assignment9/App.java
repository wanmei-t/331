package assignment9;

import java.sql.*;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println("Welcome!");
        try
        {
            Connection conn = getConnection();
            IJLSecurity jls = new JLSecurity(conn);

            while (true)
            {
                displayActions(jls);

                //ask user if they want to make another action or quit
                System.out.println("\nWould you like to make another action?");
                System.out.println("1. Yes\n2. Quit");
                int answer = readInt("Answer with an integer only: ", 1, 2);
                if (answer == 2) break;
            }

            closeConnection(conn);
        }
        catch (IllegalArgumentException e)
        {
            System.out.println("\n" + e.getMessage());
        }
        System.out.println("\nGoodbye!");
    }

    public static void displayActions(IJLSecurity jls)
    {
        System.out.println("\nWhat action do you want to make?");
        System.out.println("1. Create an account");
        System.out.println("2. Login");
        int answer = readInt("Answer with an integer only: ", 1, 2);
        if (answer == 1)
        {
            createUser(jls);
        }
        else
        {
            loginUser(jls);
        }
    }

    public static void createUser(IJLSecurity jls)
    {
        try
        {
            System.out.println("\n[USER CREATE ACCOUNT]");
            String user = readLine("Username: ");
            String password = readLine("Password: ");
            jls.CreateUser(user, password);
            System.out.println("\n" + user + ", your account was successfully created.");
        }
        catch(IllegalArgumentException e)
        {
            System.out.println("\nFailed to create an account.\n" + e.getMessage());
        }
    }

    public static void loginUser(IJLSecurity jls)
    {
        while (true) 
        {
            try
            {
                System.out.println("\n[USER LOGIN]");
                String user = readLine("Username: ");
                String password = readLine("Password: ");
                boolean loggedIn = jls.Login(user, password);
                if (loggedIn)
                {
                    System.out.println("\nSuccessfully logged in.");
                    return;
                }
                System.out.println("\nInvalid password.");
            }
            catch (IllegalArgumentException e)
            {
                System.out.println("\n" + e.getMessage());
            }
            System.out.println("\nWould you like to try again?");
            System.out.println("1. Yes\n2. Forget about logging in");
            int answer = readInt("Answer with an integer only: ", 1, 2);
            if (answer == 2) return;
        }
    }

    public static Connection getConnection()
    {
        int attempt = 0;
        while (attempt < 3)
        {
            try
            {
                System.out.println("\n[ADMIN LOG IN TO ORACLE]");
                String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
                String user = readLine("Username: ");
                char[] passwordLetters = System.console().readPassword("Password: ");
                String password = String.valueOf(passwordLetters);
                Connection conn = DriverManager.getConnection(url, user, password);
                System.out.println("\n[SUCCESSFUL LOG IN]");
                return conn;
            }
            catch (SQLException e)
            {
                System.out.println("\nError creating a new connection. Please try logging in again.");
            }
        }
        throw new IllegalArgumentException("Too many login attempts");
    }

    public static void closeConnection(Connection conn)
    {
        try
        {
            if(!conn.isClosed())
            {
                conn.close();
            }
        }
        catch (SQLException e)
        {
            throw new IllegalArgumentException("Failed to close the connection to the database.");
        }
    }

    public static String readLine(String message)
    {
        while (true)
        {
            String answer = System.console().readLine(message);
            if (!answer.isEmpty()) return answer;
            System.out.println("Your answer requires at least 1 character.");
        }
    } 

    public static int readInt(String message, int min, int max)
    {
        while (true)
        {
            try
            {
                int i = Integer.parseInt(readLine(message));
                if (i > max || i < min)
                {
                    throw new IllegalArgumentException();
                }
                return i;
            }
            catch (NumberFormatException e)
            {
                System.out.println("An integer only input is required.");
            }
            catch (IllegalArgumentException e)
            {
                System.out.println("Your answer should be between " + min + " and " + max + " inclusive.");
            }
        }
    }
}

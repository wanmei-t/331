package assignment9;

import java.sql.*;

public class User implements IUser, SQLData {
    private byte[] salt;
    private String username;
    private byte[] hash;
    private long failedLoginCount;
    private final String TYPENAME = "JLUSER_TYPE";

    public User()
    {

    }

    public User(byte[] salt, String username, byte[] hash, long failedLoginCount)
    {
        this.salt = salt;
        this.username = username;
        this.hash = hash;
        this.failedLoginCount = failedLoginCount;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException 
    {
        setUsername(stream.readString());
        setSalt(stream.readBytes());
        setHash(stream.readBytes());
        setFailedLoginCount(stream.readLong());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException 
    {
        //Note order here matters, it must match the type
        stream.writeString(getUsername());
        stream.writeBytes(getSalt());
        stream.writeBytes(getHash());
        stream.writeLong(getFailedLoginCount());
    }

    @Override
    public String getSQLTypeName() throws SQLException 
    {
        return this.TYPENAME;
    }


    public byte[] getSalt()
    {
        return this.salt;
    }

    public void setSalt(byte[] salt)
    {
        this.salt = salt;
    }
    
    public String getUsername()
    {
        return this.username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }
    
    public byte[] getHash()
    {
        return this.hash;
    }

    public void setHash(byte[] hash)
    {
        this.hash = hash;
    }

    public long getFailedLoginCount()
    {
        return this.failedLoginCount;
    }

    public void setFailedLoginCount(long count)
    {
        this.failedLoginCount = count;
    }
}

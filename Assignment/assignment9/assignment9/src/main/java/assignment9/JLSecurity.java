package assignment9;

import java.util.Arrays;
import java.sql.*;
import java.math.BigDecimal;
import java.security.*;
import java.security.spec.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.SecretKeyFactory;

public class JLSecurity implements IJLSecurity {
    private Connection conn;

    public JLSecurity(Connection conn)
    {
        this.conn = conn;
    }

    public void CreateUser(String username, String password)
    {
        byte[] salt = createSalt();
        byte[] hash = getHashedPassword(password, salt);

        //adding new user to database
        //if the username exists already, its info will be reset to 0 failed login attemps and to a new salt
        IUser user = new User(salt, username, hash, 0);
        UpdateDB(user);
    }

    public boolean Login(String username, String password)
    {
        IUser user = GetUser(username);
        if (user.getFailedLoginCount() >= 5)
        {
            throw new IllegalArgumentException("You can't login, because your account has been blocked due to too many failed login attempts.");
        }
        byte[] hashedPassword = getHashedPassword(password, user.getSalt());
        if (Arrays.equals(hashedPassword, user.getHash())) return true;
        else
        {
            long failedLoginCount = user.getFailedLoginCount();
            failedLoginCount++;
            user.setFailedLoginCount(failedLoginCount);
            UpdateDB(user);
            return false;
        }
    }

    private IUser GetUser(String username)
    {
        try
        {
            String sql = "{? = call Get_JLUser(?)}";
            CallableStatement stmt = this.conn.prepareCall(sql);
            stmt.registerOutParameter(1, Types.STRUCT, "JLUSER_TYPE");
            stmt.setString(2, username);
            stmt.execute();
            IUser user = mapStructToUser((Struct)stmt.getObject(1));
            return user;
        }
        catch (SQLException e)
        {
            throw new IllegalArgumentException("It seems like there is no account linked to the username " + username + ".");
        }
    }

    private IUser mapStructToUser(Struct struct) throws SQLException {
        Object[] attributes = struct.getAttributes();
        long failedLoginCount = ((BigDecimal)attributes[3]).longValue();
        IUser user = new User((byte[])attributes[1], (String)attributes[0], (byte[])attributes[2], failedLoginCount);
        return user;
    }

    private void UpdateDB(IUser user)
    {
        try
        {
            String sql = "{call Update_JLUser(?)}";
            CallableStatement stmt = this.conn.prepareCall(sql);
            stmt.setObject(1, user);        
            stmt.execute();
        }
        catch (SQLException e)
        {
            throw new IllegalArgumentException("There was an issue updating the database.");
        }
    }

    private byte[] createSalt()
    {
        try
        {
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            byte[] salt = new byte[16];
            sr.nextBytes(salt);
            return salt;
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new IllegalArgumentException("No such salt algorithm.");
        }
    }

    private byte[] getHashedPassword(String password, byte[] salt)
    {
        try
        {
            char[] passwordBytes = password.toCharArray();
            int keyLength = 64 /*bytes*/ * 8 /*bits*/; //512 bits
            PBEKeySpec spec = new PBEKeySpec(passwordBytes, salt, 10, keyLength);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = skf.generateSecret(spec).getEncoded();
            return hash;
        }
        catch (InvalidKeySpecException e)
        {
            throw new IllegalArgumentException("Invalid key specifiction.");
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new IllegalArgumentException("No such hash algorithm.");
        }
    }    
}

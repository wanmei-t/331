-- Dropping tables
DROP TABLE W_BooksAuthors;
DROP TABLE W_Books;
DROP TABLE W_Authors;
DROP TABLE W_Locations;

-- Creating tables
CREATE TABLE W_Locations ( 
	LocationId 	VARCHAR2(6)     PRIMARY KEY,
	City 		VARCHAR2(30) 	NOT NULL, 
	Country 	VARCHAR2(30) 	NOT NULL
);

CREATE TABLE W_Authors ( 
	AuthorId 	VARCHAR2(6)     PRIMARY KEY,
	AuthorName 	VARCHAR2(30) 	NOT NULL, 
	LocationId 	VARCHAR2(6) 	NOT NULL REFERENCES W_Locations(LocationId)
);

CREATE TABLE W_Books ( 
	ISBN        VARCHAR2(6)     PRIMARY KEY,
	Title 	    VARCHAR2(50) 	NOT NULL, 
	Category 	VARCHAR2(30) 	NOT NULL,
	Price 	    NUMBER(4, 2) 	NOT NULL
);

CREATE TABLE W_BooksAuthors ( 
	ISBN        VARCHAR2(6)     NOT NULL REFERENCES W_Books(ISBN),
	AuthorId 	VARCHAR2(6)     NOT NULL REFERENCES W_Authors(AuthorId),
    
    CONSTRAINT BooksAuthors_ISBN_AuthorId_U
	UNIQUE (ISBN, AuthorId)
);

--Inserting data
INSERT INTO W_Locations
VALUES ('L00001', 'Montreal', 'Canada');
INSERT INTO W_Locations
VALUES ('L00002', 'Birmingham', 'England');
INSERT INTO W_Locations
VALUES ('L00003', 'Toronto', 'Canada');

INSERT INTO W_Authors
VALUES ('A00001', 'Reginald Authorson', 'L00001');
INSERT INTO W_Authors
VALUES ('A00002', 'XYZ Tolkeen', 'L00002');
INSERT INTO W_Authors
VALUES ('A00003', 'B Ronalds', 'L00002');
INSERT INTO W_Authors
VALUES ('A00004', 'Janeet Paulton', 'L00003');
INSERT INTO W_Authors
VALUES ('A00005', 'Vanti Raulson', 'L00003');

INSERT INTO W_Books
VALUES ('123ABC', 'How to book', 'Business', 20.23);
INSERT INTO W_Books
VALUES ('AJKAJA', 'Wow hobbits!', 'Fantasy', 10.2);
INSERT INTO W_Books
VALUES ('AJSHAK', 'Science Adventure', 'Sci-fi', 13.12);
INSERT INTO W_Books
VALUES ('OAISJD', 'It''s cooking!', 'Cooking', 35.36);
INSERT INTO W_Books
VALUES ('ASDOA', 'The times and ideas of Ronald McRonald', 'Biography', 23);
INSERT INTO W_Books
VALUES ('JKLXCD', 'Can you really ever book?', 'Business', 30.12);
INSERT INTO W_Books
VALUES ('ASDSAK', 'Geez its hobbits.', 'Fantasy', 4.5);

INSERT INTO W_BooksAuthors
VALUES ('123ABC', 'A00001');
INSERT INTO W_BooksAuthors
VALUES ('AJKAJA', 'A00002');
INSERT INTO W_BooksAuthors
VALUES ('AJSHAK', 'A00003');
INSERT INTO W_BooksAuthors
VALUES ('OAISJD', 'A00004');
INSERT INTO W_BooksAuthors
VALUES ('ASDOA', 'A00005');
INSERT INTO W_BooksAuthors
VALUES ('JKLXCD', 'A00001');
INSERT INTO W_BooksAuthors
VALUES ('ASDSAK', 'A00002');
INSERT INTO W_BooksAuthors
VALUES ('ASDOA', 'A00004');

-- Query 1
SELECT
    COUNT(*)
FROM
    W_Books
WHERE
    Price > 15;

-- Query 2
SELECT
    Title,
    AuthorName
FROM
    W_Books INNER JOIN W_BooksAuthors
    USING (ISBN)
    INNER JOIN W_Authors
    USING (AuthorId)
WHERE
    Category = 'Fantasy';

-- Query 3
SELECT DISTINCT
    Title
FROM
    W_Books INNER JOIN W_BooksAuthors
    USING (ISBN)
    INNER JOIN W_Authors
    USING (AuthorId)
    INNER JOIN W_Locations
    USING (LocationId)
WHERE
    Country = 'Canada';
    
-- Creating a view
CREATE OR REPLACE VIEW BusinessBooks AS
SELECT
	Title,
    AuthorName,
    Price,
    City
FROM
	W_Books INNER JOIN W_BooksAuthors
    USING (ISBN)
    INNER JOIN W_Authors
    USING (AuthorId)
    INNER JOIN W_Locations
    USING (LocationId)
WHERE
    Category = 'Business';
   
-- Dropping index in case it exists already
DROP INDEX W_BooksCategoryIndex;
    
-- Before index
SELECT
    *
FROM
    BusinessBooks;

-- Creating the index
CREATE INDEX W_BooksCategoryIndex ON W_Books (Category);

-- After index
SELECT
    *
FROM
    BusinessBooks;
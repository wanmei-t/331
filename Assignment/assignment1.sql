-- PART  1
CREATE TABLE Home ( 
	hid     CHAR(4)         PRIMARY KEY, 
	address VARCHAR2(200)   NOT NULL
);

CREATE TABLE Occupation ( 
	oid     CHAR(4)         PRIMARY KEY, 
	type    VARCHAR2(20)    NOT NULL,
    salary  NUMBER(10, 2)   NOT NULL
);

CREATE TABLE Person ( 
	pid         CHAR(4)         PRIMARY KEY, 
	firstname   VARCHAR2(20)    NOT NULL,
	lastname    VARCHAR2(20)    NOT NULL,
    father_id   CHAR(4)         REFERENCES Person(pid),
    mother_id   CHAR(4)         REFERENCES Person(pid),
    hid         CHAR(4)         REFERENCES Home(hid) NOT NULL,
    oid         CHAR(4)         REFERENCES Occupation(oid) NOT NULL
);

--PART 2
INSERT INTO  Home
VALUES ('H001', '123 Easy St.');

INSERT INTO  Home
VALUES ('H002', '56 Fake Ln.');

INSERT INTO  Occupation
VALUES ('O000', 'N/A', 0);

INSERT INTO  Occupation
VALUES ('O001', 'Student', 0);

INSERT INTO  Occupation
VALUES ('O002', 'Doctor', 100000);

INSERT INTO  Occupation
VALUES ('O003', 'Professor', 80000);

INSERT INTO  Person (pid, firstname, lastname, hid, oid)
VALUES ('P001', 'Zachary', 'Aberny', 'H002', 'O000');

INSERT INTO  Person (pid, firstname, lastname, hid, oid)
VALUES ('P002', 'Yanni', 'Aberny', 'H002', 'O000');

INSERT INTO  Person
VALUES ('P003', 'Alice', 'Aberny', 'P001', 'P002', 'H001', 'O002');

INSERT INTO  Person (pid, firstname, lastname, hid, oid)
VALUES ('P004', 'Bob', 'Bortelson', 'H001', 'O003');

INSERT INTO  Person
VALUES ('P005', 'Carl', 'Aberny-Bortelson', 'P004', 'P003', 'H001', 'O001');

INSERT INTO  Person
VALUES ('P006', 'Denise', 'Aberny-Bortelson', 'P004', 'P003', 'H001', 'O001');

-- PART 2 TEST
SELECT 
    gp.firstname 
FROM 
    person gp JOIN person p 
    ON gp.pid = p.mother_id OR gp.pid = p.father_id 
    JOIN person c ON p.pid = c.mother_id OR p.pid = c.father_id 
WHERE 
    c.firstname = 'Denise';
    
-- PART 3
-- Query 1
SELECT
    Address,
    COUNT (PId)
FROM
    Person INNER JOIN Home
    USING (HId)
GROUP BY 
    Address;

    
-- Query 2
SELECT DISTINCT
    GP.FirstName,
    GP.LastName
FROM
    Person C INNER JOIN Person P
    ON (C.Father_Id = P.PId OR C.Mother_Id = P.Pid)
    INNER JOIN Person GP
    ON P.Father_Id = GP.PId;

-- Query 3
SELECT DISTINCT
    S.FirstName,
    S.LastName
FROM
    Person S INNER JOIN Person P
    ON (S.Father_Id = P.PId OR S.Mother_Id = P.Pid) 
WHERE
    S.OId = 'O001' AND S.HId = P.HId;

-- Query 4
SELECT
    SUM(Salary)
FROM
    Person P INNER JOIN Occupation O
    USING (OId)
    INNER JOIN Home H
    USING (HId)
GROUP BY
    HId
ORDER BY
    SUM(Salary) DESC
FETCH
    FIRST ROW ONLY;
    
    




